/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.exdion.writer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.AreaReference;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTable;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumn;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableColumns;
import org.openxmlformats.schemas.spreadsheetml.x2006.main.CTTableStyleInfo;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.exdion.exceptions.OutputDocsException;
import com.exdion.exceptions.XlsxException;
import com.exdion.helper.ConfigLoader;
import com.exdion.helper.S3Handler;

/**
 * Class that generates the excel files based on comparison data and uploads to
 * cloud tenant bucket
 * 
 * @author Sudip
 *
 */
/**
 * @author Sudip Das
 *
 */
public class XlsxWriter {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(XlsxWriter.class.getName());
	/**
	 * Name of Summary File for non consolidated discrepancies
	 */
	private final String SUMMARY_FILE_PREFIX = "Summary of Policy Discrepancies";

	/**
	 * File name post fix for discrepancies. Currently used
	 */
	private final String DISCREPANCY_FILE_POSTFIX = "_discrepancy.xlsx";
	/**
	 * File name post fix for extracts. The sheet name changes t failed or
	 * successful based on the scenario. Currently used
	 */
	private final String EXTRACT_DETAILS_FILE_POSTFIX = "_details.xlsx";
	/**
	 * File name post fix for failed discrepancies. Currently not used
	 */
	private final String FAILED_EXTRACTS_FILE_POSTFIX = "_extracts.xlsx";
	/**
	 * Name of Summary Tab for consolidated discrepancies
	 */
	private final String SUMMARY_SHEET = "Summary";

	/**
	 * The column from where all tables get populated
	 */
	private final String summaryColStartPrefix = "B";

	private final int summaryTablesStartColIdx = 1;
	/**
	 * Name of Discrepancy Tab for consolidated discrepancies
	 */
	private final String DISCREPANCY_SHEET = "Policy Discrepancies";
	/**
	 * Name of Failed Extracts Tab for consolidated discrepancies
	 */
	private final String FAILED_EXTRACTS_SHEET = "Failed Packs Extracts";
	/**
	 * Name of Successful extracts Tab for consolidated discrepancies
	 */
	private final String SUCCESSFUL_EXTRACTS_SHEET = "Success Packs Extracts";

	/**
	 * Table identifiers-id attribute against table as long value. Currently 3
	 * tables
	 */
	/*
	 * IMPORTANT: Different id for different table. id attribute against table as
	 * long value
	 */
	private final Long[] tableIds = { new Long(1), new Long(2), new Long(3), new Long(4) };

	/**
	 * Summary table display names
	 */
	private final String[] tableNameIdentifiers = { "FIRST_SUMMARY_TABLE", "SECOND_SUMMARY_TABLE",
			"THIRD_SUMMARY_TABLE", "FOURTH_SUMMARY_TABLE" };

	/**
	 * Column headers for table 1 in summary tab
	 */
	/* Define Header Information for the Table */
	private final String[] table1ColHeaders = { "Summary", "# of Docs" };

	/**
	 * Column headers for table 2 in summary tab
	 */
	/*
	 * CCR 7 Define Header Information for the Table and related changes Change:
	 * Date 18 Apr 2019 Summary Table 2 additional columns - total pages, scanned
	 * pages etc
	 */
	private final String[] table2ColHeaders = { "Portal ID", "Doc. Name", "Doc. Type", "Doc. Status", "Reason",
			"Total Pages", "Native Pages", "Scanned Pages", "Blank Pages", "Image Count" };

	/**
	 * Column headers for table 3 in summary tab
	 */
	/* CCR 7 Define Header Information for the Table and related changes */
	private final String[] table3ColHeaders = { "Discrepancy not generated for Policies having below files" };

	/**
	 * Column headers for table 4 in summary tab
	 */
	/* CCR 7 Define Header Information for the Table and related changes */
	private final String[] table4ColHeaders = { "Files list with wrong file names" };

	/**
	 * Column headers for discrepancies
	 */
	// CHANGE DUE TO GROUP LOBs, Extracts LOB(s) introduced
	private final String[] DISCREPANCY_DOC_HEADERS = { "Insured Name", "Ins Carrier Name", "Portal Job ID", "Policy No",
			"Policy LOB", "Policy Term", "File Name", "Policy Loaded Date", "Policy Type", "Checklist Questions",
			"Observation", "Current Term Value", "Reference Value", "Page No.", "Match Score", "Action Taken",
			"CSR/AM Comments", "CSR/AM Notes", "Current Term Extracts LOB", "Reference Term Extracts LOB" };
	// CCR7
	/**
	 * Column headers for extracts
	 */
	private final String[] EXTRACTS_DOC_HEADERS = { "File Location", "LOB", "Checklist Questions",
			"Extract Description", "Extract Value", "Page No." };

	/**
	 * Excel row from where the first table starts
	 */
	private final int startingRowNumTable1 = 3;

	/**
	 * Subsequent rows left for next tables
	 */
	/*
	 * row starts with zero count so 1 + 1 header + 1 blank =3 the data will be
	 * populated in row cells after this interval
	 */
	private final int dataPopulationRowInterval = 3;

	/**
	 * Total number of Files uploaded for processing
	 */
	private int totalFilesLoaded = 0;

	/**
	 * Total number of files processed
	 */
	private int totalFilesProcessed = 0;

	/**
	 * Total number of files not processed while determining discrepancies
	 */
	private int totalFilesUnchecked = 0;

	/**
	 * Total number of files that had discrepancies
	 */
	private int totalProcessedPoliciesWithDiscrepancies = 0;

	/**
	 * Total number of files without discrepancies
	 */
	private int totalProcessedPoliciesWithoutDiscrepancies = 0;

	/**
	 * Provides the later part of the file name .xlsx
	 */
	private String summaryFilePostFix = "";

//	private ArrayList<String> generatedFiles = new ArrayList<String>();

	public int getTotalFilesLoaded() {
		return totalFilesLoaded;
	}

	public void setTotalFilesLoaded(int totalFiles) {
		this.totalFilesLoaded = totalFiles;
	}

	public int getTotalFilesProcessed() {
		return totalFilesProcessed;
	}

	public void setTotalProcessed(int totalProcessed) {
		this.totalFilesProcessed = totalProcessed;
	}

	public int getTotalFilesUnchecked() {
		return totalFilesUnchecked;
	}

	public void setTotalFilesUnchecked(int totalFilesUnchecked) {
		this.totalFilesUnchecked = totalFilesUnchecked;
	}

	public int getTotalWithDiscrepancies() {
		return totalProcessedPoliciesWithDiscrepancies;
	}

	public void setTotalWithDiscrepancies(int totalWithDiscrepancies) {
		this.totalProcessedPoliciesWithDiscrepancies = totalWithDiscrepancies;
	}

	public int getTotalWithoutDiscrepancies() {
		return totalProcessedPoliciesWithoutDiscrepancies;
	}

	public void setTotalWithoutDiscrepancies(int totalWithoutDiscrepancies) {
		this.totalProcessedPoliciesWithoutDiscrepancies = totalWithoutDiscrepancies;
	}

	public String getSummaryFileName() {
		return SUMMARY_FILE_PREFIX + " " + summaryFilePostFix;
	}

	public void setSummaryFileName(String rootFolderName) {
		summaryFilePostFix = rootFolderName + ".xlsx";
	}

	/**
	 * Method to get Header Style for the discrepancy reports
	 * 
	 * @param wb Current workbook
	 * @return Style Format of header cells
	 */
	private XSSFCellStyle getHeaderStyle(XSSFWorkbook wb) {

		XSSFCellStyle headerStyle = wb.createCellStyle();
		XSSFFont headerFont = wb.createFont();
		headerFont.setFontHeightInPoints((short) 12);
		headerFont.setFontName("Calibri (Body)");
		headerFont.setBold(true);

		headerStyle.setFont(headerFont);
		headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		headerStyle.setAlignment(HorizontalAlignment.CENTER);
		headerStyle.setBorderBottom(BorderStyle.THIN);
		headerStyle.setBorderLeft(BorderStyle.THIN);
		headerStyle.setBorderRight(BorderStyle.THIN);
		headerStyle.setBorderTop(BorderStyle.THIN);
		headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerStyle.setFillForegroundColor(IndexedColors.PALE_BLUE.getIndex());
		return headerStyle;
	}

	/**
	 * Method to get non Header (Body) Style for the discrepancy reports
	 * 
	 * @param wb Current workbook
	 * @return Style Format of cell of non-header cells
	 */
	private XSSFCellStyle getDefaultStyle(XSSFWorkbook wb) {
		XSSFCellStyle defaultStyle = wb.createCellStyle();
		XSSFFont defaultFont = wb.createFont();
		defaultFont.setFontHeightInPoints((short) 11);
		defaultFont.setFontName("Calibri (Body)");
		defaultStyle.setFont(defaultFont);
		defaultStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		defaultStyle.setWrapText(true);
		defaultStyle.setBorderBottom(BorderStyle.THIN);
		defaultStyle.setBorderLeft(BorderStyle.THIN);
		defaultStyle.setBorderRight(BorderStyle.THIN);
		defaultStyle.setBorderTop(BorderStyle.THIN);
		return defaultStyle;
	}

	/**
	 * Create the required table for the sheet, designed, formatted and with header
	 * values
	 * 
	 * @param sheet      The sheet which will have the table
	 * @param cell_range Cell range covered by the table
	 * @param tableNum   Table number in the sheet
	 */
	private void createTableWithHeaders(XSSFSheet sheet, AreaReference cell_range, int tableNum) {
		String[] tableColHeaders = null;
		switch (tableNum) {
		case 1:
			tableColHeaders = table1ColHeaders;
			break;
		case 2:
			tableColHeaders = table2ColHeaders;
			break;
		case 3:
			tableColHeaders = table3ColHeaders;
			break;
		case 4:
			tableColHeaders = table4ColHeaders;
			break;
		}

		/*
		 * Array index: 1 less than the table number: table1=0, table2=1, table3=2
		 */
		int arrayIdx = tableNum - 1;

		// Designing the table and formatting
		XSSFTable table = sheet.createTable();
		CTTable ctTable = table.getCTTable();
		CTTableStyleInfo tableStyle = ctTable.addNewTableStyleInfo();
		tableStyle.setName("TableStyleLight13");
		tableStyle.setShowColumnStripes(false); // showColumnStripes=0
		tableStyle.setShowRowStripes(true); // showRowStripes=1

		ctTable.setRef(cell_range.formatAsString());
		ctTable.setDisplayName(tableNameIdentifiers[arrayIdx]); /* this is the display name of the table */
		ctTable.setName(tableNameIdentifiers[arrayIdx]); /* This maps to "displayName" attribute in <table>, OOXML */
		ctTable.setId(tableIds[arrayIdx]); // id attribute against table as long value, id is different for each table

//		CTTable ctTable=getDesignedTable(sheet,cell_range,tableNum);

		CTTableColumns tableColumns = ctTable.addNewTableColumns();
		tableColumns.setCount(new Long(tableColHeaders.length)); // define number of columns, here 2
		/* Define Header Information for the Table */
		for (int i = 0; i < tableColHeaders.length; i++) {// Two columns
			CTTableColumn eachTableColHeader = tableColumns.addNewTableColumn();
			eachTableColHeader.setName(tableColHeaders[i]);
			eachTableColHeader.setId(i + 1);
		}
	}

	/**
	 * Method to populate first table of Summary having files uploaded, processed
	 * etc.
	 * 
	 * @param summary_Sheet    Summary Sheet
	 * @param summaryTableNums Data for table columns
	 */
	private void createSummaryTable1(XSSFSheet summary_Sheet, int[] summaryTableNums) {
		// Table number
		int summaryTableNum = 1;

		// Defining the table area
		int tableRowStart = startingRowNumTable1;// From B3 (in this case)
		int tableRowEnd = tableRowStart + summaryTableNums.length;// Currently there are 5 items so this becomes C8 (in
																	// this case)
		String cellRangeStart = summaryColStartPrefix + Integer.valueOf(tableRowStart);
		String cellRangeEnd = "C" + Integer.valueOf(tableRowEnd);
		AreaReference cell_range = new AreaReference(cellRangeStart + ":" + cellRangeEnd);// B3:C8 in this case

		createTableWithHeaders(summary_Sheet, cell_range, summaryTableNum);

		String[] table1Col1Data = { "Summary",
				"Total # of files loaded (Current term, Prior term, Proposal & Endoresments)",
				"Total # of files processed", "Total # of files not processed",
				"Total # of policies processed with discrepancies",
				"Total # of policies processed without discrepancies" };

		/*
		 * Add remaining Table Data Populate 5 rows starting at cell B3 [0,1,2], so i
		 * starts with 2. Create a Row.Starts at B3 i.e. i=2 First row cell value to be
		 * filled B3, C3 as headers. This being the 3rd row, we need to have headers for
		 * row-->i=2 so the cells that gets filled as headers are (row=2,first col=1)
		 * and (row=2,second col=2) The rest of the cells from row =3 but column 1 are
		 * populated from firstColEntries
		 */
		int startRowIdx = 2;// Starting first table from row 3 (0,1,2 is the sequence for row count)
		int endRowIdx = startRowIdx + summaryTableNums.length;
		int startColIdx = summaryTablesStartColIdx;// Starting from column 1(0,1 is the sequence)
		int endColIdx = table1ColHeaders.length;// Two columns required
		// Total Two columns in each row

		for (int i = startRowIdx; i <= endRowIdx; i++) {
			/*  */
			XSSFRow eachTable1Row = summary_Sheet.createRow(i);
			for (int j = startColIdx; j <= endColIdx; j++) // Two columns in each row
			{
				XSSFCell table1Cell = eachTable1Row.createCell(j);

				// Setting the column headers
				if (i == startRowIdx) {
					table1Cell.setCellValue(table1ColHeaders[j - 1]);
				} else if ((i == startRowIdx) && (j == endColIdx)) {
					table1Cell.setCellValue(table1ColHeaders[j - 1]);
				} else {
					if (j == 1) {
						table1Cell.setCellValue(table1Col1Data[i - 2]);
					} else {
						if (i - 2 <= summaryTableNums.length) {
							/*
							 * Header Row starts at 2 Data rows start at 3 so to get summaryNums[0] subtract
							 * 3 etc
							 */
							table1Cell.setCellValue(summaryTableNums[i - 3]);
						}
					}
				}
				summary_Sheet.autoSizeColumn(j);
			}
		}
	}

	/**
	 * Method to populate second table of Summary having portal id, doc name, doc
	 * type, status and reason
	 * 
	 * @param summary_Sheet          Summary Sheet
	 * @param summaryTabFilesList    List of files and the data for each column
	 *                               associated with each File
	 * @param previousTablesRowCount First Table Row Count
	 */
	private void createSummaryTable2(XSSFSheet summary_Sheet, ArrayList<HashMap<String, String>> summaryTabFilesList,
			int previousTablesRowCount) {
		// Table number
		int summaryTableNum = 2;

		// Defining the table area
		int tableRowStart = previousTablesRowCount + dataPopulationRowInterval;
		// First row is header so one extra row to be added
		int tableRowEnd = tableRowStart + summaryTabFilesList.size() + 1;
		String cellRangeStart = summaryColStartPrefix + Integer.valueOf(tableRowStart);
		/*
		 * 5 columns, B,C,D,E,F - SOW CCR7 Changed from "C" to "F". [B(x):F(y)]
		 * B,C,D,E,F. Date 18 Apr 2019 Summary Table 2- Change from 5 to 10 columns
		 * [B(x):K(y) in this case)] B,C,D,E,F,G,H,I,J,K
		 * 
		 */
		String cellRangeEnd = "K" + Integer.valueOf(tableRowEnd);

		AreaReference cell_range = new AreaReference(cellRangeStart + ":" + cellRangeEnd);

		createTableWithHeaders(summary_Sheet, cell_range, summaryTableNum);

		/*
		 * First column will have filenames so count will decide the number of rows
		 * (i.e."startRowIdx")
		 */
		int startRowIdx = tableRowStart - 1;// Row count starts from 0 - (0,1,2,3 for 4 rows)
		int endRowIdx = startRowIdx + summaryTabFilesList.size();

		/*
		 * End Col Idx: SOW CCR7 Changed to 5 columns from 2 columns End Col Idx: Date
		 * 18 Apr 2019 Summary Table 2 additional columns Changed to 10
		 */
		int startColIdx = summaryTablesStartColIdx;// Starting from COl B which is index 1(A=0,B=1, C=2 etc.)
		int endColInx = table2ColHeaders.length; // Number of columns to populate

		String[] colHashKeys = { "portalID", "docName", "docType", "docStatus", "reason", "totalPages", "nativePages",
				"scannedPages", "blankPages", "imageCount" };

		for (int i = startRowIdx; i <= endRowIdx; i++) {

			XSSFRow eachTable2Row = summary_Sheet.createRow(i);
			for (int j = startColIdx; j <= endColInx; j++) // Five columns in each row
			{
				XSSFCell table2Cell = eachTable2Row.createCell(j);

				// Setting the column headers
				if (i == startRowIdx) {
					table2Cell.setCellValue(table2ColHeaders[j - 1]);
				}

				else {
					if (summaryTabFilesList.size() > 0) {
						/*
						 * Row index i starts at row 9 so initial i=9. But i=9 is the header row. So for
						 * data the row index will effectively start from the 10th row. Number of rows
						 * to populate is given by endRowIdx. As an example let us consider that there
						 * are 2 records in the summaryTabFilesList then endRowIdx=9+2. Hence to get the
						 * element (cell values) in summaryTabFilesList we use the following logic
						 * zeroth element = i-10 = 10-10 = 0 = summaryTabFilesList.get(0); - 1st element
						 * next element = i-10 = 11-10 = 1 = summaryTabFilesList.get(1); - 2nd element
						 * next element = i-10 = 12-10 = 2 = summaryTabFilesList.get(2); ... so on
						 */

						/*
						 * Change Leela Mail dated: Summary Tab change Monday, 29 October 2018 at 12:28
						 * PM. Change: Date 18 Apr 2019 Summary Table 2 change additional columns added
						 * total pages, ocr pages, etc colHashKeys[j-1] will provide the key of HashMap
						 * to get the value. Some values are strings and some integers so uniform
						 * String.valueof() used
						 */
						table2Cell
								.setCellValue(String.valueOf(summaryTabFilesList.get(i - 10).get(colHashKeys[j - 1])));

					} else {
						// Sets empty cell for all columns
						table2Cell.setCellValue("");
					}
				}
				summary_Sheet.autoSizeColumn(j);
			}
		}
	}

	/**
	 * Method to populate list of files for which discrepancy has not been generated
	 * (reasons in table 2)
	 * 
	 * @param summary_Sheet          Summary Sheet
	 * @param packsExcludedFiles     List of files
	 * @param previousTablesRowCount Second Table Row Count
	 */
	private void createSummaryTable3(XSSFSheet summary_Sheet, ArrayList<String> packsExcludedFiles,
			int previousTablesRowCount) {
		// Table number
		int summaryTableNum = 3;

		// Defining the table area
		int tableRowStart = previousTablesRowCount + dataPopulationRowInterval;
		// First row is header so one extra row to be added
		int tableRowEnd = tableRowStart + packsExcludedFiles.size() + 1;
		String cellRangeStart = summaryColStartPrefix + Integer.valueOf(tableRowStart);
		/*
		 * 1 column only but number of rows dependent on size of summaryTabFilesList so
		 * B
		 */
		String cellRangeEnd = summaryColStartPrefix + Integer.valueOf(tableRowEnd);

		AreaReference cell_range = new AreaReference(cellRangeStart + ":" + cellRangeEnd);// B(x):B(y) in this case

		createTableWithHeaders(summary_Sheet, cell_range, summaryTableNum);

		/*
		 * First column will have filenames so count will decide the number of rows
		 * (i.e."startRowIdx")
		 */

		int startRowIdx = tableRowStart - 1;// Row count starts from 0 - (0,1,2,3 for 4 rows)
		int endRowIdx = startRowIdx + packsExcludedFiles.size();

		int startColIdx = summaryTablesStartColIdx;
		int endColInx = table3ColHeaders.length; // Number of columns to populate, here 1

		for (int i = startRowIdx; i <= endRowIdx; i++) {

			XSSFRow eachTable3Row = summary_Sheet.createRow(i);
			for (int j = startColIdx; j <= endColInx; j++) // One column in each row
			{
				XSSFCell table3Cell = eachTable3Row.createCell(j);

				// Setting the column headers
				if (i == startRowIdx) {
					table3Cell.setCellValue(table3ColHeaders[j - 1]);
				}

				else {
					if (packsExcludedFiles.size() > 0) {
						/*
						 * Row index i starts at row tableRowStart-1. But first one is the header row.
						 * So for data the row index will effectively start from the tableRowStart row.
						 * Number of rows to populate is given by endRowIdx. As an example let us
						 * consider that there are 2 records in the summaryTabFilesList then
						 * endRowIdx=9+2. Check createSummaryTable2() for a detailed explanation
						 */
						if (j == startColIdx) {// Column 1 File Name
							table3Cell.setCellValue(packsExcludedFiles.get(i - tableRowStart));

						}

					} else {
						// Sets empty cell for all columns
						table3Cell.setCellValue("");
					}
				}
				summary_Sheet.autoSizeColumn(j);
			}
		}
	}

	/**
	 * Method to populate list of files with wrong naming convention
	 * 
	 * @param summary_Sheet          Summary Sheet
	 * @param wrongFileNamesList     List of files
	 * @param previousTablesRowCount Third Table Row Count
	 */
	private void createSummaryTable4(XSSFSheet summary_Sheet, ArrayList<String> wrongFileNamesList,
			int previousTablesRowCount) {
		// Table number
		int summaryTableNum = 4;

		// Defining the table area
		int tableRowStart = previousTablesRowCount + dataPopulationRowInterval;
		// First row is header so one extra row to be added
		int tableRowEnd = tableRowStart + wrongFileNamesList.size() + 1;
		String cellRangeStart = summaryColStartPrefix + Integer.valueOf(tableRowStart);
		/*
		 * 1 column only but number of rows dependent on size of summaryTabFilesList so
		 * B
		 */
		String cellRangeEnd = summaryColStartPrefix + Integer.valueOf(tableRowEnd);

		AreaReference cell_range = new AreaReference(cellRangeStart + ":" + cellRangeEnd);// B(x):B(y) in this case

		createTableWithHeaders(summary_Sheet, cell_range, summaryTableNum);

		/*
		 * First column will have filenames so count will decide the number of rows
		 * (i.e."startRowIdx")
		 */

		int startRowIdx = tableRowStart - 1;// Row count starts from 0 - (0,1,2,3 for 4 rows)
		int endRowIdx = startRowIdx + wrongFileNamesList.size();

		int startColIdx = summaryTablesStartColIdx;
		int endColInx = table4ColHeaders.length; // Number of columns to populate, here 1

		for (int i = startRowIdx; i <= endRowIdx; i++) {

			XSSFRow eachTable4Row = summary_Sheet.createRow(i);
			for (int j = startColIdx; j <= endColInx; j++) // One column in each row
			{
				XSSFCell table4Cell = eachTable4Row.createCell(j);

				// Setting the column headers
				if (i == startRowIdx) {
					table4Cell.setCellValue(table4ColHeaders[j - 1]);
				}

				else {
					if (wrongFileNamesList.size() > 0) {
						/*
						 * Row index i starts at row tableRowStart-1. But first one is the header row.
						 * So for data the row index will effectively start from the tableRowStart row.
						 * Number of rows to populate is given by endRowIdx. As an example let us
						 * consider that there are 2 records in the summaryTabFilesList then
						 * endRowIdx=9+2. Check createSummaryTable2() for a detailed explanation
						 */
						if (j == startColIdx) {// Column 1 File Name
							table4Cell.setCellValue(wrongFileNamesList.get(i - tableRowStart));

						}

					} else {
						// Sets empty cell for all columns
						table4Cell.setCellValue("");
					}
				}
				summary_Sheet.autoSizeColumn(j);
			}
		}
	}

	/*
	 * Change Leela Mail dated: Summary Tab change Monday, 29 October 2018 at 12:28
	 * PM
	 */
	/**
	 * Method to populate Summary Details
	 * 
	 * @param workbook            Current workbook
	 * @param summaryTabFilesList Files List with data for columns required for
	 *                            table 2
	 * @param packsExcludedFiles  Files excluded from comparison for table 3
	 * @param filesWithWrongNames
	 */
	private void populateSummaryTab(XSSFWorkbook workbook, ArrayList<HashMap<String, String>> summaryTabFilesList,
			ArrayList<String> packsExcludedFiles, ArrayList<String> filesWithWrongNames) {

		int[] summaryTableNums = { getTotalFilesLoaded(), getTotalFilesProcessed(), getTotalFilesUnchecked(),
				getTotalWithDiscrepancies(), getTotalWithoutDiscrepancies() };

		XSSFSheet summary_Sheet = workbook.createSheet(SUMMARY_SHEET);

		createSummaryTable1(summary_Sheet, summaryTableNums);
//		
		/*
		 * NUM ROWS TO LEAVE WILL ALWAYS TAKE THE HEADER ROW NUM OF THE HEADER assuming
		 * that there is NO DATA for subsequent rows of the table. In case there are
		 * rows ther number of rows will be given by the second parameter
		 * summaryTableNums.length/summaryTabFilesList.size()+packsExcludedFiles.size().
		 * Since we need to keep in mind the HEADER ROW without data First table starts
		 * at B2, so numRowsToLeave=2+. Second table starts at B10, so
		 * numRowsToLeave=10+. Third table starts at B17, so numRowsToLeave=17+. The new
		 * table will also take reference of the previous table header.
		 */

		/*
		 * The first table starts at row B2, with header at row B2. So
		 * previousBHeaderRow=2
		 */
		int previousHeaderRowBLocation = 2;
		int numRowsToLeave = previousHeaderRowBLocation + summaryTableNums.length;
		createSummaryTable2(summary_Sheet, summaryTabFilesList, numRowsToLeave);

		int gapBetweenTables = 3;// (1 header+1blank row of table)+1 empty excel row

		numRowsToLeave = numRowsToLeave + summaryTabFilesList.size() + gapBetweenTables;
		createSummaryTable3(summary_Sheet, packsExcludedFiles, numRowsToLeave);

		numRowsToLeave = numRowsToLeave + packsExcludedFiles.size() + gapBetweenTables;
		createSummaryTable4(summary_Sheet, filesWithWrongNames, numRowsToLeave);
	}

	/*
	 * Change Leela Mail dated: Summary Tab change Monday, 29 October 2018 at 12:28
	 * PM
	 */
	/**
	 * Method to populate Discrepancy
	 * 
	 * @param workbook            Current workbook
	 * @param totalRecordsDataMap Processed records to populate the data
	 */
	private void populateDiscrepancyTab(XSSFWorkbook workbook,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> totalRecordsDataMap) {

		XSSFSheet sheet = workbook.createSheet(DISCREPANCY_SHEET);

		// CHANGE DUE To GROUP LOB

		sheet.setColumnWidth(0, 10000);
		sheet.setColumnWidth(1, 7000);
		sheet.setColumnWidth(2, 7000);
		sheet.setColumnWidth(3, 6000);
		sheet.setColumnWidth(4, 4000);
		sheet.setColumnWidth(5, 5500);
		sheet.setColumnWidth(6, 10000);
		sheet.setColumnWidth(7, 5500);
		sheet.setColumnWidth(8, 5500);
		sheet.setColumnWidth(9, 10000);
		sheet.setColumnWidth(10, 15000);
		sheet.setColumnWidth(11, 10000);
		sheet.setColumnWidth(12, 10000);
		sheet.setColumnWidth(13, 10000);
		sheet.setColumnWidth(14, 4000);
		sheet.setColumnWidth(15, 10000);
		sheet.setColumnWidth(16, 10000);
		sheet.setColumnWidth(17, 10000);
		sheet.setColumnWidth(18, 7000);
		sheet.setColumnWidth(19, 7000);

		XSSFCellStyle headerStyle = getHeaderStyle(workbook);
		XSSFCellStyle defaultStyle = getDefaultStyle(workbook);

		Set<String> allDataMapKeys = totalRecordsDataMap.keySet();

		// Get the headers in row 0
		XSSFRow newRow = sheet.createRow(0);
		XSSFCell newCell = null;
		for (int c = 0; c < DISCREPANCY_DOC_HEADERS.length; c++) {
			newCell = newRow.createCell(c);
			newCell.setCellStyle(headerStyle);
			newCell.setCellValue(DISCREPANCY_DOC_HEADERS[c]);
			// discrepancy_Sheet.autoSizeColumn(c);// Auto size for headers row only
		}

		int recordCount = 0;
		int nonHeaderRowIdx = 1;// Starts from row 1, header in row 0

		for (String eachdataMapKey : allDataMapKeys) {
			ArrayList<LinkedHashMap<String, String>> allDataMaps = totalRecordsDataMap.get(eachdataMapKey);

			for (int mapCount = 0; mapCount < allDataMaps.size(); mapCount++) {
				HashMap<String, String> rowCellData = allDataMaps.get(mapCount);

				// CHANGE DUE TO GROUP LOB
				String[] thisRowCell = { rowCellData.get("Insured Name"), rowCellData.get("Ins Carrier Name"),
						rowCellData.get("Portal Job ID"), rowCellData.get("Policy No"), rowCellData.get("Policy LOB"),
						rowCellData.get("Policy Term"), rowCellData.get("File Name"),
						rowCellData.get("Policy Loaded Date"), rowCellData.get("Policy Type"),
						rowCellData.get("Checklist Questions"), rowCellData.get("Observation"),
						rowCellData.get("Current Term Value"), rowCellData.get("Reference Value"),
						rowCellData.get("Page No."), rowCellData.get("Match Score"), rowCellData.get("Action Taken"),
						rowCellData.get("CSR/AM Comments"), rowCellData.get("CSR/AM Notes"),
						rowCellData.get("Current Term Extracts LOB"), rowCellData.get("Reference Term Extracts LOB") };

				// Subsequent rows
				newRow = sheet.createRow(nonHeaderRowIdx);
				recordCount++;
				for (int c = 0; c < DISCREPANCY_DOC_HEADERS.length; c++) {
					newCell = newRow.createCell(c);
					newCell.setCellStyle(defaultStyle);
					newCell.setCellValue(thisRowCell[c]);
					// discrepancy_Sheet.autoSizeColumn(c);// Auto size for headers row only
				}
				nonHeaderRowIdx++;
			}
		}

		logger.debug("Discrepancy Record Count: " + recordCount);
	}

	// CC8 Successful extracts along with failed extracts
	/**
	 * Method to populate the extracts data
	 * 
	 * @param workbook            Current workbook
	 * @param totalRecordsDataMap Records to populate the data
	 * @param isFailedExtracts    failed extracts or successful extracts
	 */
	private void populateExtractsFiles(XSSFWorkbook workbook,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> totalRecordsDataMap,
			boolean isFailedExtracts) {

		// CC8 to determine successful or failed extracts
		String sheetName = isFailedExtracts ? FAILED_EXTRACTS_SHEET : SUCCESSFUL_EXTRACTS_SHEET;

		XSSFSheet sheet = workbook.createSheet(sheetName);
		sheet.setDefaultColumnWidth(10);
		sheet.setColumnWidth(0, 12000);
		sheet.setColumnWidth(1, 5000);
		sheet.setColumnWidth(2, 5000);
		sheet.setColumnWidth(3, 10000);
		sheet.setColumnWidth(4, 10000);

		XSSFCellStyle headerStyle = getHeaderStyle(workbook);
		XSSFCellStyle defaultStyle = getDefaultStyle(workbook);

		Set<String> allDataMapKeys = totalRecordsDataMap.keySet();

		// Get the headers in row 0
		XSSFRow newRow = sheet.createRow(0);
		XSSFCell newCell = null;
		for (int c = 0; c < EXTRACTS_DOC_HEADERS.length; c++) {
			newCell = newRow.createCell(c);
			newCell.setCellStyle(headerStyle);
			newCell.setCellValue(EXTRACTS_DOC_HEADERS[c]);
			// discrepancy_Sheet.autoSizeColumn(c);// Auto size for headers row only
		}

		int recordCount = 0;
		int nonHeaderRowIdx = 1;// Starts from row 1, header in row 0

		for (String eachdataMapKey : allDataMapKeys) {
			ArrayList<LinkedHashMap<String, String>> allDataMaps = totalRecordsDataMap.get(eachdataMapKey);

			for (int mapCount = 0; mapCount < allDataMaps.size(); mapCount++) {
				HashMap<String, String> rowCellData = allDataMaps.get(mapCount);

				String[] thisRowCell = { rowCellData.get("file_path"), rowCellData.get("extr_lob"),
						rowCellData.get("qs_key"), rowCellData.get("qs_desc"), rowCellData.get("qs_value"),
						rowCellData.get("page_num") };

				// Subsequent rows
				newRow = sheet.createRow(nonHeaderRowIdx);
				recordCount++;
				for (int c = 0; c < EXTRACTS_DOC_HEADERS.length; c++) {
					newCell = newRow.createCell(c);
					newCell.setCellStyle(defaultStyle);
					newCell.setCellValue(thisRowCell[c]);
					// discrepancy_Sheet.autoSizeColumn(c);// Auto size for headers row only
				}
				nonHeaderRowIdx++;
			}
		}
		// CCR 8
		if (isFailedExtracts) {
			logger.debug("Failed Pack Extracted Record Count: " + recordCount);
		} else {
			logger.debug("Successful Pack Extracted Record Count: " + recordCount);
		}
	}

	/**
	 * Method to create the consolidated file and upload to Could Tenant
	 * 
	 * @param ouputFilePath         Output File Path
	 * @param summaryTabFilesList   Summary Tab List of files
	 * @param discrepancySheetData  Discrepancy Sheet Data
	 * @param excludedPacksExtracts Failed Packs Extracts
	 * @param includedPacksExtracts Successful Packs Extracts
	 * @param jobID                 Job ID
	 * @param S3Path                S3 Path
	 * @param packIdPackKeyMap      Keys from PackIds map
	 * @param packsExcludedFiles    Files in excluded Packs
	 * @param filesWithWrongNames   Files with wrong names
	 * @throws XlsxException       Xlsx Exception
	 * @throws OutputDocsException
	 */
	/*
	 * Change Leela Mail dated: Summary Tab change Monday, 29 October 2018 at 12:28
	 * PM. Change: Date 18 Apr 2019 Summary Table 2 change
	 */
	public void writeConsolidatedXLSXFile(String ouputFilePath, ArrayList<HashMap<String, String>> summaryTabFilesList,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discrepancySheetData,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> excludedPacksExtracts,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> includedPacksExtracts, String jobID,
			String S3Path, LinkedHashMap<String, String> packIdPackKeyMap, ArrayList<String> packsExcludedFiles,
			ArrayList<String> filesWithWrongNames) throws XlsxException, OutputDocsException {

		String XLSXfilename = getSummaryFileName();

		logger.debug("Writing consolidated records to file " + XLSXfilename);

		XSSFWorkbook wbook = new XSSFWorkbook();
		populateSummaryTab(wbook, summaryTabFilesList, packsExcludedFiles, filesWithWrongNames);
		populateDiscrepancyTab(wbook, discrepancySheetData);
		populateExtractsFiles(wbook, excludedPacksExtracts, true);
		populateExtractsFiles(wbook, includedPacksExtracts, false);

		writeFilesToEC2Docs(ouputFilePath + XLSXfilename, wbook);

		// Validate file count and file names before uploading

		// ******** 20210331: 5.x no upload to S3 only exception thrown******
//		if (validateFilesToUpload(true)) {
//			// Writing to AWS S3, filename will be based on summary file name
//			uploadToS3(jobID, S3Path);
//		} else {
//			throw new OutputDocsException("All files for S3 could not be generated or file names were incorrect.");
//		}

		if (!validateFilesToUpload(true)) {
			throw new OutputDocsException("All files for S3 could not be generated or file names were incorrect.");
		}
	}

	// SOW CCR7
	// CC8 successful extracts
	/**
	 * Method to create the non consolidated files and upload to Could Tenant
	 * 
	 * @param ouputFilePath         Output File Path
	 * @param summaryTabFilesList   Summary Tab List of files
	 * @param allDiscrepRecords     Discrepancy Sheet Data
	 * @param excludedPacksExtracts Failed Packs Extracts
	 * @param includedPacksExtracts Successful Packs Extracts
	 * @param jobID                 Job ID
	 * @param S3Path                S3 Path
	 * @param packIdPackKeyMap      Keys from PackIds map
	 * @param packsExcludedFiles    Files in excluded Packs
	 * @param filesWithWrongNames   Files with wrong names
	 * @throws XlsxException       Xlsx Exception
	 * @throws OutputDocsException
	 */

	/*
	 * Change: SOW CCR7. Change: CC8 successful extracts. Change: Date 18 Apr 2019
	 * Summary Table 2 change
	 */
	public void writeIndividualXLSXFiles(String ouputFilePath, ArrayList<HashMap<String, String>> summaryTabFilesList,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> allDiscrepRecords,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> excludedPacksExtracts,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> includedPacksExtracts, String jobID,
			String S3Path, LinkedHashMap<String, String> packIdPackKeyMap, ArrayList<String> packsExcludedFiles,
			ArrayList<String> filesWithWrongNames) throws XlsxException, OutputDocsException {

		// Writing summary File
		String summaryFilename = getSummaryFileName();
		XSSFWorkbook summaryWbook = new XSSFWorkbook();
		populateSummaryTab(summaryWbook, summaryTabFilesList, packsExcludedFiles, filesWithWrongNames);

		logger.debug("Writing summary data to file " + summaryFilename);
		writeFilesToEC2Docs(ouputFilePath + summaryFilename, summaryWbook);

		// Writing discrepancy files
		for (String packIdkey : allDiscrepRecords.keySet()) {
			ArrayList<LinkedHashMap<String, String>> eachDiscpRecord = allDiscrepRecords.get(packIdkey);
			/*
			 * Instantiate a new HashMap with ArrayList containing a map of qs variances. To
			 * maintain earlier uniformity Use packid as key
			 */
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discpDataXLSX = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>();
			discpDataXLSX.put(packIdkey, eachDiscpRecord);

//			String discrepancyFilename = packIdPackKeyMap.get(packIdkey) + "_discrepancy.xlsx";
			String discrepancyFilename = packIdPackKeyMap.get(packIdkey) + DISCREPANCY_FILE_POSTFIX;
			XSSFWorkbook discrepancyWbook = new XSSFWorkbook();
			populateDiscrepancyTab(discrepancyWbook, discpDataXLSX);

			logger.debug("Writing individual discrepancy records to file \"" + discrepancyFilename);
			writeFilesToEC2Docs(ouputFilePath + discrepancyFilename, discrepancyWbook);

			// Adding _discrepancy file to generated files List
		}

		// Writing failed extracts files

		for (String packIdkey : excludedPacksExtracts.keySet()) {
			ArrayList<LinkedHashMap<String, String>> eachDiscpRecord = excludedPacksExtracts.get(packIdkey);
			/*
			 * Instantiate a new HashMap with ArrayList containing a map of qs variances. To
			 * maintain earlier uniformity Use packid as key
			 */
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discpDataXLSX = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>();
			discpDataXLSX.put(packIdkey, eachDiscpRecord);

//			String failedExtractsFilename = packIdPackKeyMap.get(packIdkey) + "_extracts.xlsx";
			String failedExtractsFilename = packIdPackKeyMap.get(packIdkey) + FAILED_EXTRACTS_FILE_POSTFIX;
			XSSFWorkbook failedExtractsWbook = new XSSFWorkbook();
			populateExtractsFiles(failedExtractsWbook, discpDataXLSX, true);

			logger.debug("Writing individual failed extracts to file \"" + failedExtractsFilename);
			writeFilesToEC2Docs(ouputFilePath + failedExtractsFilename, failedExtractsWbook);

			// Adding _extracts file to generated files List
		}

		// Writing success extracts files
		// CCR8 Successful extracts
		for (String packIdkey : includedPacksExtracts.keySet()) {
			ArrayList<LinkedHashMap<String, String>> eachDiscpRecord = includedPacksExtracts.get(packIdkey);
			/*
			 * Instantiate a new HashMap with ArrayList containing a map of qs variances. To
			 * maintain earlier uniformity Use packid as key
			 */
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discpDataXLSX = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>();
			discpDataXLSX.put(packIdkey, eachDiscpRecord);

//			String successExtractsFilename = packIdPackKeyMap.get(packIdkey) + "_details.xlsx";
			String successExtractsFilename = packIdPackKeyMap.get(packIdkey) + EXTRACT_DETAILS_FILE_POSTFIX;
			XSSFWorkbook successExtractsWbook = new XSSFWorkbook();
			populateExtractsFiles(successExtractsWbook, discpDataXLSX, false);

			logger.debug("Writing individual success extracts to file \"" + successExtractsFilename);
			writeFilesToEC2Docs(ouputFilePath + successExtractsFilename, successExtractsWbook);
		}
		// Validate file count and file names before uploading

		// ******** 20210331: 5.x no upload to S3 only exception thrown******

//		if (validateFilesToUpload(false)) {
//			// Writing all files to AWS S3, filename will be based on multiple file names
//			uploadToS3(jobID, S3Path);
//		} else {
//			throw new OutputDocsException("All files for S3 could not be generated or file names were incorrect.");
//		}

		if (!validateFilesToUpload(false)) {
			throw new OutputDocsException("All files for S3 could not be generated or file names were incorrect.");
		}

	}

	/*
	 * CCR7
	 */
	/**
	 * Writing intermediate files for Cloud Tenant
	 * 
	 * @param filePath File Path
	 * @param workbook Current workbook
	 * @throws XlsxException Xlsx Exception
	 */
	private void writeFilesToEC2Docs(String filePath, XSSFWorkbook workbook) throws XlsxException {

		try {
			FileOutputStream fileOut = new FileOutputStream(filePath);
			workbook.write(fileOut);

			fileOut.flush();
			fileOut.close();
		} catch (FileNotFoundException e) {
			logger.error("ERROR: FileNotFoundException while writing " + filePath);
			throw new XlsxException("In writeFilesEC2Docs()", e);
		} catch (IOException e) {
			logger.error("ERROR: IOException while writing " + filePath);
			throw new XlsxException("In writeFilesEC2Docs()", e);
		}
	}

	/*
	 * CCR7
	 */
	/**
	 * Methods to initiate cloud tenant transfer
	 * 
	 * @param jobID  Job ID
	 * @param S3Path S3 Path
	 * @throws InterruptedException
	 * @throws AmazonClientException
	 * @throws AmazonServiceException
	 */
	@SuppressWarnings("unused")
	private void uploadToS3(String jobID, String S3Path) {

		logger.info("Establishing AWS connection");
		S3Handler s3Writer = new S3Handler();
		logger.info("Publishing Reports in S3");

		// Delete from docs folder in EC2
		File tempDir = new File(ConfigLoader.TEMP_EXCEL_DIR);
		File[] tempFiles = tempDir.listFiles();
		for (File eachFile : tempFiles) {
			// Don't upload hidden files
			if (!eachFile.isHidden()) {
//				s3Writer.writeExcelToS3(ConfigLoader.TEMP_EXCEL_DIR + "/" + eachFile.getName(), jobID, S3Path,
//						eachFile.getName());
				try {
					s3Writer.uploadToS3(eachFile, S3Path);
				} catch (AmazonServiceException e) {
					logger.error("AmazonServiceException while uploading " + eachFile);
					logger.debug(e.getMessage());
				} catch (AmazonClientException e) {
					logger.error("AmazonClientException while uploading " + eachFile);
					logger.debug(e.getMessage());
				} catch (InterruptedException e) {
					logger.error("Amazon InterruptedException while uploading " + eachFile);
					logger.debug(e.getMessage());
				}
			}
			eachFile.delete();
		}
		tempDir.delete();
	}

	private boolean validateFilesToUpload(boolean isConsolidatedFile) {
		boolean isValid = true;
		List<String> fileNamesPresent = new ArrayList<String>();

		File tempDir = new File(ConfigLoader.TEMP_EXCEL_DIR);
		File[] tempFiles = tempDir.listFiles();
		for (File eachFile : tempFiles) {
			fileNamesPresent.add(eachFile.getName());
		}
		if (isConsolidatedFile && fileNamesPresent.size() == 1) {
			// Only one element so get(0)
			isValid = StringUtils.containsIgnoreCase(fileNamesPresent.get(0), SUMMARY_FILE_PREFIX);
		} else {
			if (fileNamesPresent.size() >= 3) {
				boolean isValidSummary = false;
				boolean isValidDiscrepancy = false;
				boolean isValidDetails = false;
				for (String eachFileName : fileNamesPresent) {
					if (StringUtils.containsIgnoreCase(eachFileName, SUMMARY_FILE_PREFIX)) {
						isValidSummary = true;
					} else if (StringUtils.containsIgnoreCase(eachFileName, DISCREPANCY_FILE_POSTFIX)) {
						isValidDiscrepancy = true;
					} else if (StringUtils.containsIgnoreCase(eachFileName, EXTRACT_DETAILS_FILE_POSTFIX)) {
						isValidDetails = true;
					}
				}
				if (isValidSummary && isValidDiscrepancy && isValidDetails)
					isValid = true;
				else
					isValid = false;
			} else {
				isValid = false;
			}
		}

		// Delete existing files in EC2 if the return value is false, spill over files
		// in other jobs issue

		// 20210331 5.x commenting delete files from Ec2

//		if (!isValid)
//			deleteEc2FilesDirectory();

		logger.debug("Validity of files to be uploaded:" + fileNamesPresent + " is " + isValid);
		return isValid;
	}

	/**
	 * Written to remove all temp files and take care of earlier files. This method
	 * was intriduced after
	 */
	@SuppressWarnings("unused")
	private void deleteEc2FilesDirectory() {

		logger.debug("Deleting directory and files from " + ConfigLoader.TEMP_EXCEL_DIR);

		// Delete from docs folder in EC2
		File tempDir = new File(ConfigLoader.TEMP_EXCEL_DIR);
		File[] tempFiles = tempDir.listFiles();
		for (File eachFile : tempFiles) {
			eachFile.delete();
		}
		tempDir.delete();
	}
}
