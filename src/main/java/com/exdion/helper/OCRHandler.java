/**
 * 
 */
package com.exdion.helper;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.CharMatcher;

/**
 * @author Sudip Das
 *
 */
public class OCRHandler {
	public static final int NUMERIC = 0;
	public static final int ALPHABETS = 1;
	public static final int ALPHANUMERIC = 2;
	public static final int SPACE = 4;

	public static ArrayList<LinkedHashMap<String, String>> sanitizeRecords(
			ArrayList<LinkedHashMap<String, String>> recordsList, int sanitizeType) {

		for (LinkedHashMap<String, String> eachMap : recordsList) {
			eachMap.put("qs_value_std", sanitizeOcrString(eachMap.get("qs_value_std"), sanitizeType));
		}
		return recordsList;
	}

	public static String sanitizeOcrString(String orgStr, int sanitizeType) {

		switch (sanitizeType) {
		case NUMERIC:
			orgStr = CharMatcher.whitespace().trimFrom(orgStr);
			return CharMatcher.inRange('0', '9').retainFrom(orgStr);
		case ALPHABETS:
			orgStr = CharMatcher.whitespace().trimFrom(orgStr);
			return CharMatcher.inRange('a', 'z').or(CharMatcher.inRange('A', 'Z')).retainFrom(orgStr);
		case ALPHANUMERIC:
			orgStr = CharMatcher.whitespace().trimFrom(orgStr);
			return CharMatcher.inRange('0', '9').or(CharMatcher.inRange('a', 'z')).or(CharMatcher.inRange('A', 'Z'))
					.retainFrom(orgStr);
		case SPACE:
			return CharMatcher.whitespace().trimFrom(orgStr);
		default:
			return orgStr;
		}

	}

	public static int getLongestCommonString(String str1, String str2) {
		char[] array1 = str1.toCharArray();
		int array1Length = str1.length();
		char[] array2 = str2.toCharArray();
		int array2Length = str2.length();

		int result = 0;

		int lngComn[][] = new int[array1Length + 1][array2Length + 1];

		for (int i = 0; i <= array1Length; i++) {
			for (int j = 0; j <= array2Length; j++) {
				if (i == 0 || j == 0)
					lngComn[i][j] = 0;
				else if (array1[i - 1] == array2[j - 1]) {
					lngComn[i][j] = lngComn[i - 1][j - 1] + 1;
					result = Integer.max(result, lngComn[i][j]);
				} else
					lngComn[i][j] = 0;
			}
		}
		return result;
	}

	public static double getOcrStringMatch(String str1, String str2) {
		// Length taken after santization and common string is on sanitized string
//		str1=replaceOcrChars(str1);
		str1 = sanitizeOcrString(str1, ALPHANUMERIC);
		int len1 = str1.length();

//		str2=replaceOcrChars(str2);
		str2 = sanitizeOcrString(str2, ALPHANUMERIC);
		int len2 = str2.length();

		int longComLen = getLongestCommonString(str1, str2);

		double matchLen1 = 1.0 * longComLen / len1;
		double matchLen2 = 1.0 * longComLen / len2;

//		System.err.println("Strings:"+str1+" : "+str2 );	
//		System.err.println("Commonality:"+matchLen1+" : "+matchLen2 );	

		double minMatch = Math.min(matchLen1, matchLen2);

		if (minMatch < 0.75) {
			return minMatch * 100.0;
		} else {
			return 100.0;
		}
	}

	public static String replaceOcrChars(String str) {
		str = StringUtils.replaceAll(str, "Z", "2");
		str = StringUtils.replaceAll(str, "l", "1");
		str = StringUtils.replaceAll(str, "L", "1");
		str = StringUtils.replaceAll(str, "S", "5");
		str = StringUtils.replaceAll(str, "B", "8");
		str = StringUtils.replaceAll(str, "C", "0");
		str = StringUtils.replaceAll(str, "6", "0");
		return str;
	}

	public static void main(String[] args) {
//		2=Z
//		0=o,O
//		6=C,(
//		5=s, $
//		8=B
//		1=l

		String str1 = "8-4) , 2) 45 $";
		String str2 = "8-4) / - 2) 46";
		getOcrStringMatch(str1, str2);

	}
}
