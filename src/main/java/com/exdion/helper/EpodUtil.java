/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.exdion.helper;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import com.sdsl.utils.CollectionsUtil;
import com.sdsl.utils.JsonUtil;
import com.sdsl.utils.StringUtil;

/**
 * @author Sudip Das
 *
 */
public class EpodUtil {

	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(EpodUtil.class.getName());

	/**
	 * Standard messages for data not found in policy documents
	 */
	public static final String NOT_FOUND = "NOT FOUND";
	/**
	 * Standard messages for tag not found in policy documents
	 */
	public static final String NO_TAGS_FOUND = "NO TAGS FOUND";
	/**
	 * Standard messages for keys not found in policy documents
	 */
	public static final String NO_KEY_FOUND = "NO KEYS";
	/**
	 * Standard messages for no policy endorsements internal or external
	 */
	public static final String NO_ED_PAGES = "NO ED PAGES";
	/**
	 * Standard messages for no relevant records within policy documents
	 */
	public static final String NO_RECORDS = "NO RECORDS";
	/**
	 * Standard messages for content error in policy documents
	 */
	public static final String IS_DOC_CONTENT_ERROR = "DOCUMENT CONTENT ERROR";
	/**
	 * Standard messages for document format error in policy documents
	 */
	public static final String IS_DOC_FORMAT_ERROR = "DOCUMENT FORMAT ERROR";
	/**
	 * Standard messages for signifying error in policy documents
	 */
	public static final String IS_ERROR = "ERROR";

	/**
	 * Method to return the score of two Strings comparison score.
	 * 
	 * @param str1 First String
	 * @param str2 Second String
	 * @return score
	 */
	public static double getStringMatch(String str1, String str2) {
		ArrayList<String> strsToIgnore = new ArrayList<String>();
		strsToIgnore.add(NO_RECORDS);
		strsToIgnore.add(NOT_FOUND);
		strsToIgnore.add(NO_KEY_FOUND);
		strsToIgnore.add(NO_TAGS_FOUND);
		strsToIgnore.add(IS_ERROR);
		strsToIgnore.add(IS_DOC_CONTENT_ERROR);
		strsToIgnore.add(IS_DOC_FORMAT_ERROR);
		return StringUtil.getStringMatchScore(str1, str2, strsToIgnore);
	}

	/**
	 * Remove duplicates from List.
	 * 
	 * @param recordsList   List of records from which items have to be removed
	 * @param removeIdxList List of indices used for removal of items
	 * @return List with records removed
	 */
	public static ArrayList<LinkedHashMap<String, String>> getDedupedRecords(
			ArrayList<LinkedHashMap<String, String>> recordsList, ArrayList<Integer> removeIdxList) {

		return CollectionsUtil.removeDuplicatesFromList(recordsList, removeIdxList);
	}

	/**
	 * Method to check if the string value is proper and not part of strings to be
	 * ignored
	 * 
	 * @param inputStr
	 * @return boolean
	 */
	public static boolean isProperValueStr(String inputStr) {

		ArrayList<String> strsToIgnore = new ArrayList<String>();
		strsToIgnore.add(NO_RECORDS);
		strsToIgnore.add(NOT_FOUND);
		strsToIgnore.add(NO_KEY_FOUND);
		strsToIgnore.add(NO_TAGS_FOUND);
		strsToIgnore.add(IS_ERROR);
		strsToIgnore.add(IS_DOC_CONTENT_ERROR);
		strsToIgnore.add(IS_DOC_FORMAT_ERROR);
		strsToIgnore.add(NO_ED_PAGES);

		return StringUtil.containsStringIgnoreCase(inputStr, strsToIgnore) ? false : true;
	}

	public static boolean isProper3LJson(ArrayList<String> templateQsList, String referenceJsonStr, String testJsonStr)
			throws ParseException {
		ArrayList<String> ref1LQsKeys = JsonUtil.get1LevelKeysNestedJson(referenceJsonStr);
		System.out.println("From template:" + ref1LQsKeys);
		ArrayList<String> test1LQsKeys = JsonUtil.get1LevelKeysNestedJson(testJsonStr);
		System.err.println("From test Json:" + test1LQsKeys);
		if (test1LQsKeys.size() == 0 || test1LQsKeys.size() > 1) {
			return false;
		} else {
			// Only one element, get the first level key
			String test1LKey = test1LQsKeys.get(0);
			// Check if the first level key from test is present in
			// reference first level
			ArrayList<String> ref2LKeys = JsonUtil.get2LevelKeysNestedJson(referenceJsonStr, test1LKey);
			// Check if the key from the testJson is also there in template
			if (ref2LKeys == null || ref2LKeys.size() == 0) {
				logger.debug("JSON Data for Qs Key " + test1LKey + " does not exist in template JSON.");
				return false;
			}
			ArrayList<String> test2LKeys = JsonUtil.get2LevelKeysNestedJson(testJsonStr, test1LKey);
			System.out.println(ref2LKeys);
			System.out.println(test2LKeys);
			for (String eachtest2LKeys : test2LKeys) {
				ArrayList<String> ref3LKeys = JsonUtil.get3LevelKeysNestedJson(referenceJsonStr, test1LKey,
						eachtest2LKeys);

				if (ref3LKeys == null || ref3LKeys.size() == 0) {
					logger.debug("JSON Data for Qs Key " + test1LKey + "and sub-node " + eachtest2LKeys
							+ " does not exist in template JSON.");
					return false;
				}
				ArrayList<String> test3LKeys = JsonUtil.get3LevelKeysNestedJson(testJsonStr, test1LKey, eachtest2LKeys);
				System.err.println(ref3LKeys);
				System.err.println(test3LKeys);
				if (ref3LKeys.containsAll(test3LKeys)) {
					System.out.println("TRUE");
					return true;
				} else {
					System.out.println("FALSE");
					return false;
				}
			}
			return true;
		}

	}

	public static boolean isProper2LJson(String referenceJsonStr, String testJsonStr, String qsKey)
			throws ParseException {
		boolean isProperJson = true;

		// Get all the second level nodes from the reference checklist JSON
		ArrayList<String> keys2LRef = JsonUtil.get2LevelKeysNestedJson(referenceJsonStr, qsKey);

		/*
		 * Check if configuration exists for the question. If configuration is not found
		 * in reference JSON, it will return null. Also if no third level keys are found
		 * even if configuration exists in reference JSON, it will return zero size
		 * List. Return false in this case and come out of the function.
		 */
		if (keys2LRef == null || keys2LRef.size() == 0) {
			logger.debug("Reference JSON has no configuration for " + qsKey + ".");
			isProperJson = false;
			return false;
		}
		/*
		 * Get all the third level keys from both JSON related to second level keys.
		 * Each 2nd level Key from reference JSON is passed to get the 3rd level keys in
		 * both. Since test JSON is double nested, call is made to 2levelKeys function,
		 * not 3levelKeys. 3levelKeys is called for reference JSON, 2levelKeys is called
		 * for test JSON
		 */
		for (String each2LRefKey : keys2LRef) {
			ArrayList<String> keys3LRef = JsonUtil.get3LevelKeysNestedJson(referenceJsonStr, qsKey, each2LRefKey);
			ArrayList<String> keys3LTest = JsonUtil.get2LevelKeysNestedJson(testJsonStr, each2LRefKey);
			/*
			 * If any key from reference JSON is not found in test JSON, it will return
			 * null. Also if no third level keys are found it will return zero size List. .
			 * Return false in this case after breaking from loop
			 */

			if (keys3LRef == null || keys3LTest == null || keys3LRef.size() == 0 || keys3LTest.size() == 0) {
				logger.debug("Node in DB JSON is present in Reference but not in DB data.");
				isProperJson = false;
				break;
			} else if (!keys3LRef.containsAll(keys3LTest)) {
				// If the returned keys do not match return false after breaking from loop
				logger.debug("Data in DB JSON does not contain all the keys.");
				isProperJson = false;
				break;
			}
		}
		return isProperJson;
	}

	/**
	 * Check if JSON object null. If yes return "" else convert JSON object to
	 * string, trim and return
	 * 
	 * @param jsonObj
	 * @return
	 */
	public static String dealWithNull(Object value) {
		if (value == null) {
			logger.debug(value + " " + "is NULL, changing to empty string");
			return "";
		} else {
			return value.toString().trim();
		}
	}

	/**
	 * Checks for existence of all keys in the JSON object
	 * 
	 * @param jsonObj
	 * @param jsonObjKeys
	 * @return
	 */
	public static boolean checkKeysExist(JSONObject jsonObj, String[] jsonObjKeys) {
		for (int i = 0; i < jsonObjKeys.length; i++) {
			if (!jsonObj.containsKey(jsonObjKeys[i]))
				return false;
		}
		return true;
	}
}
