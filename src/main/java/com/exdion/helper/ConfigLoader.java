/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.exdion.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.exdion.exceptions.ComparatorException;
import com.google.gson.Gson;
import com.sdsl.slml.ComparingPOJO;
import com.sdsl.slml.MetaPOJO;
import com.sdsl.slml.ScoringPOJO;
import com.sdsl.utils.JsonUtil;

/**
 * @author Sudip Das
 *
 */
public class ConfigLoader {
	/**
	 * Default logger
	 */
	static Logger logger = null;
	/**
	 * All Properties
	 */
	public static Properties awsProps = null;
	public static Properties dbProps = null;
	public static Properties slMlRcdQsProps = new Properties();
	public static Properties binRcdQsProps = new Properties();

	public static Properties lobQsMappingProps = new Properties();
	public static Properties finalCompareProps = new Properties();
	public static Properties mnemonicsProps = new Properties();
	public static Properties binaryMatchProps = new Properties();
	public static Properties lobDependencyProps = new Properties();
	public static Properties descMatchProps = new Properties();
	public static Properties descThresholdsProps = new Properties();
	public static Properties lobDictProps = new Properties();
	public static Properties statusMsgsProps = new Properties();
	public static Properties descWtProps = new Properties();
	public static Properties lobWtProps = new Properties();
	public static Properties stdValWtProps = new Properties();
	public static Properties finalScoringThresholdProps = new Properties();

	/**
	 * Location of local excel files
	 */
	public static String TEMP_EXCEL_DIR = "./comparator_docs";
	/**
	 * Location of local log files
	 */
	public static String TEMP_LOG_DIR = "./comparator_logs";

	/**
	 * AWS Region
	 */
	public static String CURRENT_REGION = "";

	public static String SINGLE_RECORD = "singlerecord";
	public static String MULTI_RECORD = "multirecord";
	public static String BINNING_RECORD = "binningrecord";

	/**
	 * Configuration maps
	 */
	private static HashMap<String, HashMap<String, String>> contentsMap;

	/**
	 * All resource files
	 */
	// 6.x change
//	private static String CHKLIST_JSON = "exdionChecklist.json";
//	private static String SCORING_JSON = "exdionScoringModel.json";
	private static String CHKLIST_JSON = "checklistConfig.json";
	private static String INFRA_JSON = "exdionInfra.json";
	private static String CONTENT_JSON = "exdionContent.json";
	private static String LOBDICT_PROP_FILE = "lobDictionary.properties";

	/**
	 * Other variables
	 */
	private static String chkLstJsonStr = null;

//	public static LinkedHashMap<String, ArrayList<String>> testAllQs = new LinkedHashMap<String, ArrayList<String>>();

	/**
	 * 6x changes. All POJOS for configuration limited to SL and ML, binning has
	 * separate treatment
	 */
	public static LinkedHashMap<String, Map<String, String>> slMlMetaConfigMaps = new LinkedHashMap<String, Map<String, String>>();
	public static LinkedHashMap<String, List<Map<String, String>>> slMlScoreConfigMaps = new LinkedHashMap<String, List<Map<String, String>>>();
	public static LinkedHashMap<String, List<Map<String, String>>> slMlCompareConfigMaps = new LinkedHashMap<String, List<Map<String, String>>>();

	/**
	 * Initialization: Properties, log files, documents
	 * 
	 * @param jobId      Job ID
	 * @param loggingTS  Logging Time stamp
	 * @param currentEnv Current environment
	 * @throws ComparatorException Comparator Exception
	 */
	public static void init(String jobId, Timestamp loggingTS, String currentEnv, String currentReg)
			throws ComparatorException {

		String logFileName = "./comparator_logs/" + jobId + "_ExdionPOD_comparator.log";
		System.setProperty("logfile.name", logFileName);
		logger = Logger.getLogger(ConfigLoader.class.getName());
		CURRENT_REGION = currentReg;

		try {
			ClassLoader classloader = Thread.currentThread().getContextClassLoader();

			// Checklist related properties
			chkLstJsonStr = JsonUtil.getJSonToStr(classloader, CHKLIST_JSON);

			getPropsFromChecklist(chkLstJsonStr, SINGLE_RECORD);
			getPropsFromChecklist(chkLstJsonStr, MULTI_RECORD);
			getPropsFromChecklist(chkLstJsonStr, BINNING_RECORD);

//			System.err.println(slMlMetaConfigMaps);
//			System.err.println(slMlScoreConfigMaps);
//			System.err.println(slMlCompareConfigMaps);

			// Infrastructure(AWS/DB) related properties
			String jsonStr = JsonUtil.getJSonToStr(classloader, INFRA_JSON);
			// New Infra JSON

			awsProps = getInfraProps(jsonStr, "aws");
			dbProps = getInfraProps(jsonStr, currentEnv.toLowerCase());

			if (awsProps == null) {
				throw new ComparatorException("Cloud Configuration Parameters not available, contact Support.");
			}
			if (dbProps == null) {
				throw new ComparatorException("Wrong Environment Parameters provided: \"" + currentEnv + "\"");
			}

			// Content properties like prefix strings for observation, page number etc
			jsonStr = JsonUtil.getJSonToStr(classloader, CONTENT_JSON);
			contentsMap = getExdionContentJsonData(jsonStr);

			String homeDir = Paths.get(".").toAbsolutePath().normalize().toString();

			// 20210331 5.x appending jobid to docs dir

			TEMP_EXCEL_DIR = homeDir + "/comparator_docs/" + jobId;
			File directory = new File(TEMP_EXCEL_DIR);
			if (!directory.exists()) {
//				directory.mkdir();
				directory.mkdirs();
			}

			directory = new File(homeDir + "/comparator_logs");
			if (!directory.exists()) {
				directory.mkdir();
			}

			// LOB dictionary and status messages properties
			InputStream is = classloader.getResourceAsStream(LOBDICT_PROP_FILE);
			lobDictProps.load(is);

//			Commenting this out as status messages will come from DB
//			is = classloader.getResourceAsStream(STATUSMSG_PROP_FILE);
//			statusMsgsProps.load(is);// statusMsgsProps

		} catch (FileNotFoundException e) {
			logger.error("ERROR: FileNotFoundException while fetching configuration files.");
			throw new ComparatorException("In init()", e);
		} catch (IOException e) {
			logger.error("ERROR: IOException while converting JSON to String or loading Input Stream.");
			throw new ComparatorException("In init()", e);
		} catch (ParseException e) {
			logger.error("ERROR: ParseException while converting Nested JSON to HashMap of HashMaps / Bin Maps.");
			throw new ComparatorException("In init()", e);
		} catch (ComparatorException e) {
			logger.error("ERROR:" + e.getMessage());
			throw new ComparatorException("ERROR:" + e.getMessage());
		} catch (Exception e) {
			logger.error("ERROR: Exception in configuration loader initialization.");
			throw new ComparatorException("In init()", e);
		}
	}

	/**
	 * Method to get the Exdion Checklist JSON as JSON String
	 * 
	 * @return String of ExdionCheckList JSON contents
	 */
	public static String getChkLstJsonStr() {
		return chkLstJsonStr;
	}

	/**
	 * Method to get Content Label and Page Label from exdionContent.json
	 * 
	 * @param jsonStr Data as String from a JSON file
	 * @return Map of content maps
	 * @throws ParseException
	 */
	private static HashMap<String, HashMap<String, String>> getExdionContentJsonData(String jsonStr)
			throws ParseException {
		return JsonUtil.getDoubleNestedMapsFromDoubleNestedJson(jsonStr);
	}

	/**
	 * @param jsonStr Data as String from a JSON file
	 * @param awsOrDb AWS Prop or Db of curret environment
	 * @return Relevant properties
	 * @throws ParseException
	 * @throws ComparatorException
	 */
	private static Properties getInfraProps(String jsonStr, String awsOrDb) throws ParseException, ComparatorException {
		Properties props = new Properties();
		String relevantInfraJson = null;
		try {
			/*
			 * If it is aws, take from the node directly and set relevantInfraJson to get
			 * properties. If it is db then one extra step is involved. One intermediate
			 * object is created from node "envs" and then relevant relevantInfraJson is got
			 * from the key.getValue(); The key while getting getValue() is matched with
			 * what is passed as environment in awsOrDb. This value forms the json snippet
			 * after which setting the property is logic is same.
			 */

			if (awsOrDb.equalsIgnoreCase("aws")) {
				HashMap<String, HashMap<String, String>> allInfraMapsFromJson = JsonUtil
						.getDoubleNestedMapsFromTripleNestedJsonPerNode(jsonStr, "regions");
				relevantInfraJson = allInfraMapsFromJson.get(CURRENT_REGION).get("aws");// AWS
			} else {
				HashMap<String, String> allInfraMapsFromJson = JsonUtil
						.getDoubleNestedMapsFromTripleNestedJsonPerNode(jsonStr, "regions").get(CURRENT_REGION);
				relevantInfraJson = allInfraMapsFromJson.get("envs");// ENVS
				Object intermediateJSON = new JSONParser().parse(relevantInfraJson);
				JSONObject intermediateJsonOb = (JSONObject) intermediateJSON;
				Iterator<?> intermediateKeys = intermediateJsonOb.entrySet().iterator();
				relevantInfraJson = null;// Setting to null afresh
				while (intermediateKeys.hasNext()) {
					Map.Entry<?, ?> intermediateNodesMap = (Map.Entry<?, ?>) intermediateKeys.next();
					if (intermediateNodesMap.getKey().toString().equalsIgnoreCase(awsOrDb)) {
						relevantInfraJson = intermediateNodesMap.getValue().toString();
					}
				}
			}

			/*
			 * Common part once relevantInfraJson snippet is set.
			 */
			Object parsedJSON = new JSONParser().parse(relevantInfraJson);
			JSONObject jsonObj = (JSONObject) parsedJSON;
			Iterator<?> keys = jsonObj.entrySet().iterator();
			while (keys.hasNext()) {
				Map.Entry<?, ?> firstLevelNodesMap = (Map.Entry<?, ?>) keys.next();
				props.setProperty(firstLevelNodesMap.getKey().toString(), firstLevelNodesMap.getValue().toString());
			}

		} catch (ParseException e) {
			logger.error("Parsing Exception: Infra json:" + e.getMessage());
			throw e;
		} catch (Exception e) {
			logger.error("Null pointer Exception: Infra json:" + e.getMessage());
			throw new ComparatorException(e.getMessage());
		}
		return props;
	}

	/**
	 * Setting the status message properties file from DB eliminating the properties
	 * file.
	 * 
	 * @param statusMsgsProps
	 */
	public static void setStatusMsgsProps(Properties statusMsgsProps) {
		ConfigLoader.statusMsgsProps = statusMsgsProps;
	}

	/**
	 * Method to initialize all properties, with special treatment for some
	 * non-billing related properties
	 * 
	 * @param jsonStr
	 * @param recType
	 * @throws ParseException
	 */
	private static void getPropsFromChecklist(String jsonStr, String recType) throws ParseException {
//		slRcdQsMap
		ArrayList<String> chkListKeys = new ArrayList<String>();
		JSONObject fullChkLstJsonObj = (JSONObject) new JSONParser().parse(jsonStr);

		JSONObject recObj = (JSONObject) fullChkLstJsonObj.get(recType);
		Iterator<?> recsIterator = recObj.entrySet().iterator();
		while (recsIterator.hasNext()) {
			Map.Entry<?, ?> chkLstKeys = (Map.Entry<?, ?>) recsIterator.next();
			chkListKeys.add(chkLstKeys.getKey().toString());
		}

		for (String eachKey : chkListKeys) {
			JSONObject keyObj = (JSONObject) recObj.get(eachKey);
			JSONObject metaObj = (JSONObject) keyObj.get("metaDataParams");

			lobQsMappingProps.setProperty(eachKey, metaObj.get("lob").toString());
			finalCompareProps.setProperty(eachKey, metaObj.get("finalCompareThreshold").toString());
			finalScoringThresholdProps.setProperty(eachKey, metaObj.get("finalScoringThreshold").toString());
			mnemonicsProps.setProperty(eachKey, metaObj.get("mnemonic").toString());

			/**
			 * Currently for single and multi line. The binning JSON follows a different
			 * structure
			 */

			// ********* START OF SPECIAL TREATMENT *****************************
			if (!recType.equalsIgnoreCase(BINNING_RECORD)) {

				Gson gson = new Gson();
				String qsJsonStr = gson.toJson(keyObj);
				MetaPOJO metaParamsPOJO = gson.fromJson(qsJsonStr, MetaPOJO.class);
//				System.err.println(metaParamsPOJO.getMetaDataParams());

				ScoringPOJO scoringParamsPOJO = gson.fromJson(qsJsonStr, ScoringPOJO.class);
//				System.err.println(scoringParamsPOJO.getScoringParams());

				ComparingPOJO comparingParamsPOJO = gson.fromJson(qsJsonStr, ComparingPOJO.class);
//				System.err.println(comparingParamsPOJO.getComparisonParams());

				slMlMetaConfigMaps.put(eachKey, metaParamsPOJO.getMetaDataParams());
				slMlScoreConfigMaps.put(eachKey, scoringParamsPOJO.getScoringParams());
				slMlCompareConfigMaps.put(eachKey, comparingParamsPOJO.getComparisonParams());

				binaryMatchProps.setProperty(eachKey, metaObj.get("binarymatch").toString());

				/**
				 * LOB dependency. Get array of scoring params, get id="lob" from each array
				 * element. If dependency is true, add.
				 */
				JSONArray scoringArray = (JSONArray) keyObj.get("scoringParams");
				for (int i = 0; i < scoringArray.size(); i++) {
					JSONObject scoreObj = (JSONObject) scoringArray.get(i);
					if (scoreObj.get("id").toString().equalsIgnoreCase("extr_lob")) {
						if (Boolean.parseBoolean(scoreObj.get("dependency").toString())) {
							lobDependencyProps.setProperty(eachKey, scoreObj.get("dependency").toString());
						}						
						lobWtProps.setProperty(eachKey, scoreObj.get("weight").toString());
					} else if (scoreObj.get("id").toString().equalsIgnoreCase("qs_desc_std")) {
						if (Boolean.parseBoolean(scoreObj.get("dependency").toString())) {
							descMatchProps.setProperty(eachKey, scoreObj.get("dependency").toString());
						}
						descWtProps.setProperty(eachKey, scoreObj.get("weight").toString());
					} else if (scoreObj.get("id").toString().equalsIgnoreCase("qs_value_std")) {
						stdValWtProps.setProperty(eachKey, scoreObj.get("weight").toString());
					}
				}
				descThresholdsProps.setProperty(eachKey, "0.0");
				JSONArray comparingArray = (JSONArray) keyObj.get("comparisonParams");
				for (int i = 0; i < comparingArray.size(); i++) {
					JSONObject compareObj = (JSONObject) comparingArray.get(i);
					if (compareObj.get("id").toString().equalsIgnoreCase("qs_desc_std")) {
						descThresholdsProps.setProperty(eachKey, compareObj.get("compareThreshold").toString());
					}
				}

			}
			// ********* END OF SPECIAL TREATMENT *****************************

		}
		if (recType.equalsIgnoreCase(SINGLE_RECORD)) {
			slMlRcdQsProps.setProperty("SINGLERECORD", String.join(",", chkListKeys));
		} else if (recType.equalsIgnoreCase(MULTI_RECORD)) {
			slMlRcdQsProps.setProperty("MULTIRECORD", String.join(",", chkListKeys));
			// binRcdQsProps
		} else if (recType.equalsIgnoreCase(BINNING_RECORD)) {
			binRcdQsProps.setProperty("BINNINGRECORD", String.join(",", chkListKeys));
		}
	}

	/**
	 * Getter method to get Contents Map
	 * 
	 * @return Contents Map derived from processing contents JSON file
	 */
	public static HashMap<String, HashMap<String, String>> getContentsMap() {
		return contentsMap;
	}

	public static void main(String args[]) throws IOException, ParseException, ComparatorException {
//		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
//		String jsonStr = JsonUtil.getJSonToStr(classloader, getExdChkLstJsonFile());
//		GsonUtils.getMapOfAllBinParamMapsFromJSON(jsonStr, "binningrecord");

//		String jsonStr = JsonUtil.getJSonToStr(classloader, INFRA_JSON);
//		System.out.println(getInfraProps(jsonStr, "aws"));
//		System.out.println(getInfraProps(jsonStr, "dev"));
//		// System.out.println(Regions.fromName("us-west-1"));

	}

}
