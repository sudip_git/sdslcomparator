/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.exdion.helper;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.sdsl.writers.AwsS3Writer;

/**
 * @author Sudip Das
 *
 */
public class S3Handler {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(S3Handler.class.getName());
	private TransferManager tm = null;

	public S3Handler() {
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(
				ConfigLoader.awsProps.getProperty("AWSAccessKeyId").toString(),
				ConfigLoader.awsProps.getProperty("AWSSecretKey").toString());
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(Regions.fromName(ConfigLoader.CURRENT_REGION))
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		tm = TransferManagerBuilder.standard().withS3Client(s3Client)
				.withMultipartUploadThreshold((long) (5 * 1024 * 1025)).build();
	}

	/**
	 * Method to write excel file in S3 bucket
	 * 
	 * @param fileToUploadWithPath File To Upload With Path (Input)
	 * @param jobID                Job ID
	 * @param s3Path               S3 Bucket Path
	 * @param ouptFileName         File Name in S3 (Output)
	 */
	public void writeExcelToS3(String fileToUploadWithPath, String jobID, String s3Path, String ouptFileName) {

		AwsS3Writer.writeToS3(ConfigLoader.awsProps.getProperty("AWSAccessKeyId").toString(),
				ConfigLoader.awsProps.getProperty("AWSSecretKey").toString(), Regions.US_EAST_2,
				"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8", fileToUploadWithPath,
				jobID, s3Path, ouptFileName);
	}

	/**
	 * Method to write log files as text files in S3 bucket
	 * 
	 * @param fileToUploadWithPath File To Upload With Path (Input)
	 * @param jobID                Job ID
	 * @param s3Path               S3 Bucket Path
	 * @param ouptFileName         File Name in S3 (Output)
	 */
	public void writeLogToS3(String fileToUploadWithPath, String jobID, String s3Path, String ouptFileName) {
		// Writing job log file to s3

		AwsS3Writer.writeToS3(ConfigLoader.awsProps.getProperty("AWSAccessKeyId").toString(),
				ConfigLoader.awsProps.getProperty("AWSSecretKey").toString(), Regions.US_EAST_2,
				"text/plain;charset=UTF-8", fileToUploadWithPath, jobID, s3Path, ouptFileName);
	}

	/**
	 * Writing file to S3
	 * 
	 * @param s3Path
	 * @param region
	 * @param contentType
	 * @param contentString
	 * @param uploadFileName
	 * @throws IOException
	 */
	public static void writeFileToS3(String s3Path, String region, String contentType, String contentString,
			String uploadFileName) throws IOException {

		BasicAWSCredentials awsCreds = new BasicAWSCredentials(
				ConfigLoader.awsProps.getProperty("AWSAccessKeyId").toString(),
				ConfigLoader.awsProps.getProperty("AWSSecretKey").toString());

		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(region)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();

		// Get the bytes from file content string to set length
		InputStream inputStream = new ByteArrayInputStream(contentString.getBytes());
		byte[] bytes = IOUtils.toByteArray(inputStream);
		ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);

		ObjectMetadata objectMetaData = new ObjectMetadata();
		objectMetaData.setContentType(contentType);
		objectMetaData.setContentLength(bytes.length);

		PutObjectRequest request = new PutObjectRequest(s3Path, uploadFileName, byteArrayInputStream, objectMetaData);
		request.setMetadata(objectMetaData);
		s3Client.putObject(request);
	}

	/**
	 * Upload file to S3
	 * 
	 * @param fileToUploadWithPath
	 * @param s3Path
	 * @throws AmazonServiceException
	 * @throws AmazonClientException
	 * @throws InterruptedException
	 */
	public void uploadToS3(File fileToUploadWithPath, String s3Path)
			throws AmazonServiceException, AmazonClientException, InterruptedException {
		Upload upload = tm.upload(s3Path, fileToUploadWithPath.getName(), fileToUploadWithPath);
		upload.waitForCompletion();
	}

	/**
	 * Getting Json String from json file in S3
	 * 
	 * @param s3Path
	 * @param region
	 * @param dwFileName
	 * @return
	 * @throws IOException
	 * @throws AmazonS3Exception
	 */
	public static String downloadS3FileAsString(String s3Path, String region, String dwFileName)
			throws IOException, AmazonS3Exception {

		BasicAWSCredentials awsCreds = new BasicAWSCredentials(
				ConfigLoader.awsProps.getProperty("AWSAccessKeyId").toString(),
				ConfigLoader.awsProps.getProperty("AWSSecretKey").toString());
		AmazonS3 s3Client = AmazonS3ClientBuilder.standard().withRegion(region)
				.withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
		S3Object s3object = s3Client.getObject(s3Path, dwFileName);
		S3ObjectInputStream inputStream = s3object.getObjectContent();
		return IOUtils.toString(inputStream, "UTF-8");
	}
}
