/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */
package com.exdion.comparator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import com.exdion.exceptions.ExtractHelperException;
import com.exdion.helper.ConfigLoader;
import com.exdion.helper.EpodUtil;
import com.exdion.helper.OCRHandler;
import com.sdsl.binning.ComparisonParams;
import com.sdsl.binning.GsonUtils;
import com.sdsl.binning.MetaDataParams;
import com.sdsl.binning.ScoringParams;
import com.sdsl.utils.BinningUtil;
import com.sdsl.utils.CollectionsUtil;
import com.sdsl.utils.JsonUtil;
import com.sdsl.utils.ScoringUtil;
import com.sdsl.utils.StringUtil;

/**
 * This class serves as a helper class for ExdionPod Comparator. Match scores
 * for checklist questions are calculated and returned to the calling
 * Comparator. The helper class also serves to normalize values of current term
 * and prior term extracted values and standardize messages for discrepancy
 * report. Sequencing of current term and prior term values for question value
 * override is determined in this class.
 * 
 * @author Sudip Das
 *
 */
public class ExtractHelper {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(ExtractHelper.class.getName());

	/*
	 * Policy Type introduced for PP1
	 */
	static boolean isMarketedPolicy = true;

	/*
	 * Special treatment areas
	 */
	static final String[] formKeys = { "CA1", "CL1", "PT1" };

	/**
	 * validates if CT00 is present in the files list retrieved from DB
	 * 
	 * @param fileTypesList List of file types
	 * @return true if CT exists
	 */
	public static boolean checkCtExists(ArrayList<String> fileTypesList) {
		boolean hasCT = false;
		for (String fileType : fileTypesList) {
			if (fileType.equalsIgnoreCase("CT00")) {
				hasCT = true;
				break;
			}
		}
		if (hasCT == false) {
			logger.debug("No source (CT) array (multirecords) found in file types");
		}
		return hasCT;
	}

	/**
	 * Method to get matching score for single records
	 * 
	 * @param ctRecordsList   List of current term records
	 * @param ptPrRecordsList List of prior term records
	 * @param qs_Key          Checklist question
	 * @return List of Maps with comparison, will have only one Map as this is
	 *         Single Line
	 * @throws ExtractHelperException Extract Helper Exception
	 */

	/*
	 * CR Leela Mail Call - NFP dated Tuesday, 27 November 2018 at 4:09 AM:
	 * Observation & Page Number changes
	 */
	public static ArrayList<LinkedHashMap<String, String>> getSlRecordMatch(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key, boolean isMarketed)
			throws ExtractHelperException {

		// 6.x change
		ArrayList<LinkedHashMap<String, String>> returnList = new ArrayList<LinkedHashMap<String, String>>();
		boolean isBinaryMatch = Boolean.parseBoolean(ConfigLoader.slMlMetaConfigMaps.get(qs_Key).get("binarymatch"));

		isMarketedPolicy = isMarketed;
		// PP needs a different comparison
		boolean isDate = qs_Key.equalsIgnoreCase("PP1") ? true : false;

		// List of comparison params
		List<Map<String, String>> compareParamsList = ConfigLoader.slMlCompareConfigMaps.get(qs_Key);

		String finalCTString = EpodUtil.NO_RECORDS;
		String finalPtPrString = EpodUtil.NO_RECORDS;

		for (int i = 0; i < ctRecordsList.size(); i++) {
			String qs_val_std = ctRecordsList.get(i).get("qs_value_std");
			if (!EpodUtil.isProperValueStr(qs_val_std)) {
				continue;
			} else {
				finalCTString = qs_val_std;
			}
		}
		for (int i = 0; i < ptPrRecordsList.size(); i++) {
			String qs_val_std = ptPrRecordsList.get(i).get("qs_value_std");
			if (!EpodUtil.isProperValueStr(qs_val_std)) {
				continue;
			} else {
				finalPtPrString = qs_val_std;
			}
		}

		// Existing code maintained
		/*
		 * Currently only PP needs binary match so if isBinaryMatch=true is being
		 * ignored
		 */
		double score = 0.0;

		if (!EpodUtil.isProperValueStr(finalCTString)) {
			score = 0.0;
		} else if (!EpodUtil.isProperValueStr(finalPtPrString)) {
			score = 0.0;
		} else if (isDate) {
			score = getMatch(finalCTString, finalPtPrString, true);

		} else if (!isBinaryMatch) {
			score = getMatch(finalCTString, finalPtPrString, false);
		}

		LinkedHashMap<String, String> returnMapForList = new LinkedHashMap<String, String>();
		for (LinkedHashMap<String, String> eachCtMap : ctRecordsList) {

			if (eachCtMap.get("qs_value_std").equals(finalCTString)
					|| finalCTString.equalsIgnoreCase(EpodUtil.NO_RECORDS)) {
				returnMapForList.put("ct_qs_desc", eachCtMap.get("qs_desc"));
				returnMapForList.put("ct_qs_desc_std", eachCtMap.get("qs_desc_std"));
				returnMapForList.put("ct_qs_value", eachCtMap.get("qs_value"));
				returnMapForList.put("ct_qs_valstd", eachCtMap.get("qs_value_std"));
				returnMapForList.put("ct_qs_valtype", eachCtMap.get("val_type"));
				returnMapForList.put("ct_file_path", eachCtMap.get("file_path"));
				returnMapForList.put("ct_page_num", eachCtMap.get("page_num"));
				returnMapForList.put("ct_line_num", eachCtMap.get("line_num"));
//				returnMapForList.put("ct_file_lob", eachCtMap.get("file_lob"));
				returnMapForList.put("ct_file_type", eachCtMap.get("file_type"));
				returnMapForList.put("ct_file_id", eachCtMap.get("file_id"));
				returnMapForList.put("ct_extr_id", eachCtMap.get("extr_id"));
				returnMapForList.put("ct_extr_lob", eachCtMap.get("extr_lob"));
			}
		}
		for (LinkedHashMap<String, String> eachPtPrMap : ptPrRecordsList) {
			if (eachPtPrMap.get("qs_value_std").equals(finalPtPrString)
					|| finalPtPrString.equalsIgnoreCase(EpodUtil.NO_RECORDS)) {
				returnMapForList.put("ptPr_qs_desc", eachPtPrMap.get("qs_desc"));
				returnMapForList.put("ptPr_qs_desc_std", eachPtPrMap.get("qs_desc_std"));
				returnMapForList.put("ptPr_qs_value", eachPtPrMap.get("qs_value"));
				returnMapForList.put("ptPr_qs_valstd", eachPtPrMap.get("qs_value_std"));
				returnMapForList.put("ptPr_qs_valtype", eachPtPrMap.get("val_type"));
				returnMapForList.put("ptPr_file_path", eachPtPrMap.get("file_path"));
				returnMapForList.put("ptPr_page_num", eachPtPrMap.get("page_num"));
				returnMapForList.put("ptPr_line_num", eachPtPrMap.get("line_num"));
//				returnMapForList.put("ptPr_file_lob", eachPtPrMap.get("file_lob"));
				returnMapForList.put("ptPr_file_type", eachPtPrMap.get("file_type"));
				returnMapForList.put("ptPr_file_id", eachPtPrMap.get("file_id"));
				returnMapForList.put("ptPr_extr_id", eachPtPrMap.get("extr_id"));
				returnMapForList.put("ptPr_extr_lob", eachPtPrMap.get("extr_lob"));
			}
		}

		returnMapForList.put("match", String.format("%.1f%%", score));
		returnList.add(returnMapForList);

//		logger.debug("Final Single Record Base value: " + finalCTString);
//		logger.debug("Final Single Record Against value: " + finalPtPrString);

//		System.err.println("Final Single Record Base value: " + finalCTString);
//		System.err.println("Final Single Record Against value: " + finalPtPrString);

		return returnList;
	}

	/**
	 * Method to get matching score for multiple records
	 * 
	 * @param ctRecordsList   List of current term records
	 * @param ptPrRecordsList List of prior term records
	 * @param qs_Key          Checklist question
	 * @param policy_Lob      Acronym of applicable LOB for the pack being processed
	 * @param isMarketed      Marketed or Automatic Renewal (for listing control)
	 * @return List of Maps with comparison
	 * @throws ExtractHelperException Extract Helper Exception
	 */

	/*
	 * CR Leela Mail Call - NFP dated Tuesday, 27 November 2018 at 4:09 AM:
	 * Observation & Page Number changes
	 */
	public static ArrayList<LinkedHashMap<String, String>> getMlRecordMatch(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key,
			HashMap<String, String> metaDataMap, boolean isMarketed) throws ExtractHelperException {

		boolean isBinaryMatch = Boolean.parseBoolean(ConfigLoader.binaryMatchProps.getProperty(qs_Key));
		double qsThreshold = Double.parseDouble((String) ConfigLoader.finalCompareProps.getProperty(qs_Key));
		isMarketedPolicy = isMarketed;

		ArrayList<LinkedHashMap<String, String>> returnList = new ArrayList<LinkedHashMap<String, String>>();
		ArrayList<Integer> ctRmvIdxNew = new ArrayList<Integer>();
		ArrayList<Integer> pPrRmvIdxNew = new ArrayList<Integer>();

		// NEW CODE TO ADDRESS NOISE (OCR)

//		ArrayList<LinkedHashMap<String, String>> ctRecordsListTemp = ctRecordsList;
//		ArrayList<LinkedHashMap<String, String>> ptPrRecordsListTemp = ptPrRecordsList;

		if (ArrayUtils.contains(formKeys, qs_Key)) {
			ctRecordsList = OCRHandler.sanitizeRecords(ctRecordsList, OCRHandler.ALPHANUMERIC);
			ptPrRecordsList = OCRHandler.sanitizeRecords(ptPrRecordsList, OCRHandler.ALPHANUMERIC);
		}

		// END OF NEW CODE TO ADDRESS NOISE (OCR)

		List<LinkedHashMap<String, Integer>> matchedList = ExtractHelper.getScoredIndexedList(ctRecordsList,
				ptPrRecordsList, qs_Key);

//		if(qs_Key.equalsIgnoreCase("LS1")) {
//			System.err.println("ExtractHelper matchedList:"+matchedList);
//		}

		for (int k = 0; k < matchedList.size(); k++) {
			int ctIdx = matchedList.get(k).get("ctIdx");
			int ptPrIdx = matchedList.get(k).get("ptPrIdx");
			LinkedHashMap<String, String> tempCtMap = ctRecordsList.get(ctIdx);
			LinkedHashMap<String, String> tempPtPrMap = ptPrRecordsList.get(ptPrIdx);

//			logger.debug("Multiple Record Base value: " + tempCtMap.get("qs_value"));
//			logger.debug("Multiple Record Against value: " + tempPtPrMap.get("qs_value"));

			String ctValue = StringUtil.getNormalizedUpCaseString(tempCtMap.get("qs_value_std"));
			String ptPrValue = StringUtil.getNormalizedUpCaseString(tempPtPrMap.get("qs_value_std"));
			double match = getMatch(ctValue, ptPrValue, false);

			// NEW CODE TO ADDRESS NOISE (OCR)
			if (ArrayUtils.contains(formKeys, qs_Key)) {
				ctValue = OCRHandler.sanitizeOcrString(ctValue, OCRHandler.ALPHANUMERIC);
				ptPrValue = OCRHandler.sanitizeOcrString(ptPrValue, OCRHandler.ALPHANUMERIC);
				match = OCRHandler.getOcrStringMatch(ctValue, ptPrValue);
			}
			// END OF NEW CODE TO ADDRESS NOISE (OCR)

			/*
			 * Bug TP1: if condition added for TP1 where values are different and being a
			 * numeric match it was returning zero thereby resulting in 2 records: 6625.04
			 * and 6244 So qsThreshold check when it defaults to string match in getMatch
			 */
			if (NumberUtils.isParsable(ctValue) && NumberUtils.isParsable(ptPrValue)) {
				ctRmvIdxNew.add(ctIdx);
				pPrRmvIdxNew.add(ptPrIdx);
				LinkedHashMap<String, String> returnMapForList = getMatchedResultMap(tempCtMap, tempPtPrMap);
				if (isBinaryMatch) {
					match = match != 100.0 ? 0.0 : match;
				}

				returnMapForList.put("match", String.format("%.1f%%", match));
				returnList.add(returnMapForList);

			} else {
				if (match >= qsThreshold) {
					ctRmvIdxNew.add(ctIdx);
					pPrRmvIdxNew.add(ptPrIdx);
					LinkedHashMap<String, String> returnMapForList = getMatchedResultMap(tempCtMap, tempPtPrMap);
					if (isBinaryMatch) {
						match = match != 100.0 ? 0.0 : match;
					}

					returnMapForList.put("match", String.format("%.1f%%", match));
					returnList.add(returnMapForList);
				}
			}
		}

		ctRecordsList = EpodUtil.getDedupedRecords(ctRecordsList, ctRmvIdxNew);
		ctRmvIdxNew.clear();// Clear cache
		ptPrRecordsList = EpodUtil.getDedupedRecords(ptPrRecordsList, pPrRmvIdxNew);
		pPrRmvIdxNew.clear();

		for (LinkedHashMap<String, String> eachCtDescRecords : ctRecordsList) {
			// Always a new HashMap
			LinkedHashMap<String, String> returnMapForList = getUnMatchedCTResultMap(eachCtDescRecords);
			returnMapForList.put("match", String.format("%.1f%%", 0.0));
			returnList.add(returnMapForList);
		}
		// CRR 9 add None PtPr records
		/*
		 * Change: Include PR Listed as part of PT1. Dated: 11 April 2019. Since PR1
		 * will not be treated as a separate question, combine listed forms as part of
		 * PT1. Telephonic with Leela. Mail: PT1 Listed comparison to be modified the
		 * Comparison on PR(Proposal) forms will be only against the forms listed in
		 * proposal vs CT and not CT vs Proposal.
		 * 
		 * FOR DISCREPANCY RELATED TO QUESTIONS CA1 AND CL1, THE FOLLOWING SECTION DOES
		 * NOT APPLY. IT IS HANDLED WITHOUT THIS.
		 */
		if (qs_Key.equalsIgnoreCase("PT1")) {
			if (!isMarketed) {
				for (LinkedHashMap<String, String> eachPtPrDescRecords : ptPrRecordsList) {
					// Always a new HashMap
					LinkedHashMap<String, String> returnMapForList = getUnMatchedPtPrResultMap(eachPtPrDescRecords);
					returnMapForList.put("match", String.format("%.1f%%", 0.0));
					returnList.add(returnMapForList);
				}
			}
		} else if (!qs_Key.equalsIgnoreCase("CL1") || !qs_Key.equalsIgnoreCase("CA1")) {
			/*
			 * No need to show NO RECORDS in case of CL1 and CA1 on PtPr side. For other
			 * questions have NO RECORDS scenarioThe NO RECORDS will be added only and only
			 * if the records pertain to the policy LOB or "main".Change recorded per
			 * Leela's mail "Updated open items for discussion" dated 26 August 2019.
			 */

//			Policy LOB which was the file_lob is not considered any more, the applicable LOBs are now taken from metadata
			List<String> applicableLobs = Arrays.asList(metaDataMap.get("Extracts CT LOB(s)").split(","));
			for (LinkedHashMap<String, String> eachPtPrDescRecords : ptPrRecordsList) {

				if (applicableLobs.stream().anyMatch(eachPtPrDescRecords.get("extr_lob").trim()::equalsIgnoreCase)
						|| eachPtPrDescRecords.get("extr_lob").equalsIgnoreCase("main")) {
					// Always a new HashMap
					LinkedHashMap<String, String> returnMapForList = getUnMatchedPtPrResultMap(eachPtPrDescRecords);
					returnMapForList.put("match", String.format("%.1f%%", 0.0));
					returnList.add(returnMapForList);
				}
			}
		}
		return returnList;
	}

	public static ArrayList<LinkedHashMap<String, String>> new_getMlRecordMatch(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key,
			HashMap<String, String> metaDataMap, boolean isMarketed) throws ExtractHelperException {

		ArrayList<LinkedHashMap<String, String>> returnList = new ArrayList<LinkedHashMap<String, String>>();
		ArrayList<Integer> ctRmvIdxNew = new ArrayList<Integer>();
		ArrayList<Integer> pPrRmvIdxNew = new ArrayList<Integer>();

		boolean isBinaryMatch = Boolean.parseBoolean(ConfigLoader.slMlMetaConfigMaps.get(qs_Key).get("binarymatch"));
		double finalCompareThreshold = Double
				.parseDouble(ConfigLoader.slMlMetaConfigMaps.get(qs_Key).get("finalCompareThreshold"));

		System.err.println(qs_Key);
		// List of comparison params sorted based on priority (fail safe although it is
		// stored as per the config JSON).
		List<Map<String, String>> compareParamsList = CollectionsUtil
				.sortListofMaps(ConfigLoader.slMlCompareConfigMaps.get(qs_Key), "priority");

		List<LinkedHashMap<String, Integer>> matchedList = ExtractHelper.getScoredIndexedList(ctRecordsList,
				ptPrRecordsList, qs_Key);

		for (int k = 0; k < matchedList.size(); k++) {
			int ctIdx = matchedList.get(k).get("ctIdx");
			int ptPrIdx = matchedList.get(k).get("ptPrIdx");
			LinkedHashMap<String, String> tempCtMap = ctRecordsList.get(ctIdx);
			LinkedHashMap<String, String> tempPtPrMap = ptPrRecordsList.get(ptPrIdx);
			double match = 0.0;

			for (Map<String, String> eachCompareParamMap : compareParamsList) {
				String currentParamName = eachCompareParamMap.get("id");
				boolean isCheckGate = Boolean.parseBoolean(eachCompareParamMap.get("checkgate"));
				boolean continueCheckFurther = Boolean.parseBoolean(eachCompareParamMap.get("continuecheckfurther"));
				double compareThreshold = Double.parseDouble(eachCompareParamMap.get("compareThreshold"));

				String ctValue = StringUtil.getNormalizedUpCaseString(tempCtMap.get(currentParamName));
				String ptPrValue = StringUtil.getNormalizedUpCaseString(tempPtPrMap.get(currentParamName));

				double currentParamMatch = getMatch(ctValue, ptPrValue, false);

				if (isCheckGate) {
					if (currentParamMatch < compareThreshold) {
						if (!continueCheckFurther) {
							break;
						}
					}
				} else {
					if (NumberUtils.isParsable(ctValue) && NumberUtils.isParsable(ptPrValue)) {
						ctRmvIdxNew.add(ctIdx);
						pPrRmvIdxNew.add(ptPrIdx);
						LinkedHashMap<String, String> returnMapForList = getMatchedResultMap(tempCtMap, tempPtPrMap);
						if (isBinaryMatch) {
							match = match != 100.0 ? 0.0 : match;
						}

						returnMapForList.put("match", String.format("%.1f%%", match));
						returnList.add(returnMapForList);

					}
					// *************************************

					match = currentParamMatch;
					if (currentParamMatch < compareThreshold) {
						// what with match?
						if (!continueCheckFurther) {
							break;
						}
					} else {

					}
				}

			}

		}

		return returnList;
	}

	/**
	 * 
	 * Method to get matching score for multiple records
	 * 
	 * @param ctRecordsList   List of current term records
	 * @param ptPrRecordsList List of prior term records
	 * @param qs_Key          Checklist question
	 * @param policy_Lob      Acronym of applicable LOB for the pack being processed
	 * @param isMarketed      Marketed or Automatic Renewal (for listing control)
	 * @return List of Maps with comparison
	 * @throws ExtractHelperException Extract Helper Exception
	 */
	@SuppressWarnings("unchecked")
	public static ArrayList<LinkedHashMap<String, String>> getBinRecordMatch(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key,
			HashMap<String, String> metaDataMap, boolean isMarketed) throws ExtractHelperException {

		/*
		 * Block introduced to check for empty binning string. If empty removed from CT
		 * records and PT records Lists before proceeding further. The passed CT records
		 * list and PtPr records list will have records that have valid binning strings
		 * and the rest will be used to populate errorCtRecsList and errorPtPrRecsList
		 * which will be dealt at the end with "Empty Record" scenarios. Use reverse for
		 * loop for removal from original list to overcome index error.
		 */
		ArrayList<LinkedHashMap<String, String>> errorCtRecsList = new ArrayList<LinkedHashMap<String, String>>();
		ArrayList<LinkedHashMap<String, String>> errorPtPrRecsList = new ArrayList<LinkedHashMap<String, String>>();

//		logger.debug("Initial Base Records List Size:" + ctRecordsList.size());
		for (int i = ctRecordsList.size() - 1; i >= 0; i--) {
			String binning_str = ctRecordsList.get(i).get("binning_str");
			if (!isJsonValid(binning_str)) {
				/*
				 * First add then remove
				 */
				errorCtRecsList.add(ctRecordsList.get(i));
				ctRecordsList.remove(i);
			}
		}
//		logger.debug("Final Base Records List Size:" + ctRecordsList.size());
//
//		logger.debug("Initial Against Records List Size:" + ptPrRecordsList.size());

		for (int i = ptPrRecordsList.size() - 1; i >= 0; i--) {
			String binning_str = ptPrRecordsList.get(i).get("binning_str");
			if (!isJsonValid(binning_str)) {
				/*
				 * First add then remove
				 */
				errorPtPrRecsList.add(ptPrRecordsList.get(i));
				ptPrRecordsList.remove(i);
			}
		}
//		logger.debug("Final Against Records List Size:" + ptPrRecordsList.size());

//		logger.debug("Base Records with Binning String errors List Size:" + errorCtRecsList.size());
//		logger.debug("Against Records with Binning String errors List Size:" + errorCtRecsList.size());

		if (errorCtRecsList.size() > 0)
			logger.debug("Base records with empty/errored Binning String:" + errorCtRecsList);
		if (errorPtPrRecsList.size() > 0)
			logger.debug("Against records with empty/errored Binning String:" + errorPtPrRecsList);

		/*
		 * End of new introduced block Now we are left with CT and PtPr lists that have
		 * valid binning JSON Strings
		 */

		ArrayList<LinkedHashMap<String, String>> returnList = new ArrayList<LinkedHashMap<String, String>>();

		ArrayList<Integer> ctRmvIdxNew = new ArrayList<Integer>();
		ArrayList<Integer> pPrRmvIdxNew = new ArrayList<Integer>();

		try {
			// Use BinningUtil to get all Objects from checklist template
			BinningUtil binUtil = new BinningUtil(ConfigLoader.getChkLstJsonStr(), "binningrecord", qs_Key);
			MetaDataParams metaDataParamsObj = binUtil.getMetaDataParamsObj();
			// 6.x change
//			double avgCompareThreshold = Double
//					.parseDouble(metaDataParamsObj.getMetaDataParams().get("avgCompareThreshold"));
			double avgCompareThreshold = Double
					.parseDouble(metaDataParamsObj.getMetaDataParams().get("finalCompareThreshold"));
			// Get Scoring and Comparsion Parameters
			ScoringParams scoreParamsObj = binUtil.getScoreParamsObj();
			ComparisonParams compParamsObj = binUtil.getCompParamsObj();

			// START OF SCORING
			/*
			 * Get the scoring keys using BinningUtil. Pre-process the keys and then arrange
			 * the parameter maps for scoring keys as per priority. The CT records are taken
			 * and maps generated with parameters required for scoring and comparison. Hence
			 * newly formed allBinCtRecsMapsList and allBinPtPrRecsMapsList. Each map will
			 * have data like extraction id and value standard derived from binning string
			 * for each of the scoring or comparison keys. The record indices are maintained
			 * so that current term index and prior term index match with the original
			 * Records list.
			 */
			List<String> scoreTemplateParamKeys = GsonUtils.getBinJsonTemplateKeys(scoreParamsObj);
			ArrayList<LinkedHashMap<String, String>> allBinCtRecsMapsList = getPreProcessedScoringMap(ctRecordsList,
					scoreTemplateParamKeys);
			ArrayList<LinkedHashMap<String, String>> allBinPtPrRecsMapsList = getPreProcessedScoringMap(ptPrRecordsList,
					scoreTemplateParamKeys);
			List<LinkedHashMap<String, Integer>> matchedList = ScoringUtil.getScoreHashBin(allBinCtRecsMapsList,
					allBinPtPrRecsMapsList, binUtil);

			// END OF SCORING

			// START OF COMPARISON

			/*
			 * Get the list of all comparison parameters. Bifurcate the comparison
			 * parameters as checkGate and checkValue Map of Maps. checkGate and checkValue
			 * will have maps as per priority. The key in each of these maps are the
			 * sequence numbers like 1, 2, 3 so Integer is taken. This is not the priority
			 * number from checklist JSON. The priority number of JSON drives the sequence
			 * number in the maps. These maps will be used for comparison. First the
			 * checkGates will be compared and if it passes chceckGate the values will be
			 * compared from checkValues.
			 */
			/*
			 * Processing steps for comparison. Get the current term and prior term record
			 * indices from Scoring.
			 * 
			 */
			List<Map<String, String>> allCompTemplParamMapsList = compParamsObj.getComparisonParams();
			LinkedHashMap<Integer, LinkedHashMap<String, String>> checkGateMaps = getPreProcessedSequencedComparisonMaps(
					allCompTemplParamMapsList, true);
			LinkedHashMap<Integer, LinkedHashMap<String, String>> checkValueMaps = getPreProcessedSequencedComparisonMaps(
					allCompTemplParamMapsList, false);
			for (int k = 0; k < matchedList.size(); k++) {
				int ctIdx = matchedList.get(k).get("ctIdx");
				int ptPrIdx = matchedList.get(k).get("ptPrIdx");

				ctRmvIdxNew.add(ctIdx);
				pPrRmvIdxNew.add(ptPrIdx);

				// Get Binning String
				LinkedHashMap<String, String> eachCtRcdMap = ctRecordsList.get(ctIdx);
				LinkedHashMap<String, String> eachPtPrRcdMap = ptPrRecordsList.get(ptPrIdx);
				String ctBinningStr = eachCtRcdMap.get("binning_str");
				String ptPrBinningStr = eachPtPrRcdMap.get("binning_str");
//				logger.debug("Binning JSON Base values: " + ctBinningStr);
//				logger.debug("Binning JSON Against values: " + ptPrBinningStr);

				// Get the template key values from Binning JSON
				HashMap<String, Object> dbCtBinDataMap = JsonUtil.getMixedDBJsonMap(ctBinningStr);
				HashMap<String, Object> dbPtPrBinDataMap = JsonUtil.getMixedDBJsonMap(ptPrBinningStr);
//				logger.debug("All Binning Comparison Map Base values : " + dbCtBinDataMap);
//				logger.debug("All Binning Comparison Map  Against values: " + dbPtPrBinDataMap);

				/*
				 * First get the return map with other values of current term and prior terms
				 * records. Other values like match result or binning related data will be added
				 * later to this.
				 */
				LinkedHashMap<String, String> returnMapForList = getMatchedResultMap(eachCtRcdMap, eachPtPrRcdMap);

				// START OF CHECK GATE COMPARISON
				/*
				 * CHECK GATE COMPARISON. First sequenced parameter is taken, value std
				 * compared. If passed or failed the value of continue further is taken. If
				 * continue further is true then the next sequenced parameter is picked up and
				 * the value std compared. The process is continued based on the continue
				 * further determination. The outcome is stored in passedCheckGate flag. If
				 * check gate is passed based on param threshold, then check value is initiated
				 * for match. Else the records are added to returnMapForList with match as 0.
				 */
				boolean passedCheckGate = true;
				for (Integer eachChkGateKey : checkGateMaps.keySet()) {
					String currentChkGateKeyId = checkGateMaps.get(eachChkGateKey).get("id");
					boolean continueFurther = Boolean
							.parseBoolean(checkGateMaps.get(eachChkGateKey).get("continuecheckfurther"));
					double paramThreshold = Double
							.parseDouble(checkGateMaps.get(eachChkGateKey).get("threshold").trim());
					String cmptype = checkGateMaps.get(eachChkGateKey).get("cmptype").trim();

					logger.debug("Comparison CHECKGATE Key being considered: :" + currentChkGateKeyId);

					HashMap<String, String> ctCompData = (HashMap<String, String>) dbCtBinDataMap
							.get(currentChkGateKeyId);
					HashMap<String, String> ptPrCompData = (HashMap<String, String>) dbPtPrBinDataMap
							.get(currentChkGateKeyId);

//					logger.debug(
//							"Binning Check Gate Comparison Base Map for " + currentChkGateKeyId + ":" + ctCompData);
//					logger.debug(
//							"Binning Check Gate Comparison Against Map for" + currentChkGateKeyId + ":" + ptPrCompData);

					String ctValue = StringUtil.getNormalizedUpCaseString(ctCompData.get("value_std"));
					String ptPrValue = StringUtil.getNormalizedUpCaseString(ptPrCompData.get("value_std"));

//					logger.debug("Current Checkgate Key:" + currentChkGateKeyId + ", BaseValueStd=" + ctValue
//							+ ", Against Value Std=" + ptPrValue + ", comparison type=" + cmptype
//							+ ", comparison threshold=" + paramThreshold + ". Continue post failure? "
//							+ continueFurther);

					if (hasPassedCheckGate(ctValue, ptPrValue, paramThreshold, cmptype)) {
						logger.debug("Checkgate Key: " + currentChkGateKeyId + " cleared");
						passedCheckGate = true;
						if (continueFurther) {
							continue;
						} else {
							break;
						}
					} else {
						logger.debug("Checkgate Key: " + currentChkGateKeyId + " not cleared.");
						passedCheckGate = false;
						if (continueFurther) {
							continue;
						} else {
							break;
						}

					}
				}
				// END OF CHECK GATE COMPARISON

				// START OF CHECK VALUE COMPARISON

				/*
				 * CHECK VALUE COMPARISON. First sequenced parameter is taken, value std
				 * compared. If passed or failed the value of continue further is taken. If
				 * continue further is true then the next sequenced parameter is picked up and
				 * the value std compared. The process is continued based on the continue
				 * further determination. matches are added and the average match is found out
				 * as final match. Based on the outcome of the match ctIdx and ptPrIdx is added
				 * for removal from NO RECORDS scenario.
				 */

				if (passedCheckGate) {

					logger.debug("Final Checkgate clearence status: PASSED.");
					double match = 0.0;
					int numMatches = 0;// number of matches to be used for average.
					for (Integer eachChkValueKey : checkValueMaps.keySet()) {
						String currentChkValueKeyId = checkValueMaps.get(eachChkValueKey).get("id");
//						boolean continueFurther = Boolean
//								.parseBoolean(checkValueMaps.get(eachChkValueKey).get("continuecheckfurther"));
						double paramThreshold = Double
								.parseDouble(checkValueMaps.get(eachChkValueKey).get("threshold").trim());
						String cmptype = checkValueMaps.get(eachChkValueKey).get("cmptype").trim();

						logger.debug("Comparison CHECKVALUE Key being considered: :" + currentChkValueKeyId);

						HashMap<String, String> ctCompData = (HashMap<String, String>) dbCtBinDataMap
								.get(currentChkValueKeyId);
						HashMap<String, String> ptPrCompData = (HashMap<String, String>) dbPtPrBinDataMap
								.get(currentChkValueKeyId);

//						logger.debug("Binning Check Value Comparison Base Map for " + currentChkValueKeyId + ":"
//								+ ctCompData);
//						logger.debug("Binning Check Value Comparison Against Map for" + currentChkValueKeyId + ":"
//								+ ptPrCompData);

						String ctValue = StringUtil.getNormalizedUpCaseString(ctCompData.get("value_std"));
						String ptPrValue = StringUtil.getNormalizedUpCaseString(ptPrCompData.get("value_std"));

//						logger.debug("Current Comparison Key:" + currentChkValueKeyId + ", BaseValueStd=" + ctValue
//								+ ", Against Value Std=" + ptPrValue + ", comparison type=" + cmptype
//								+ ", comparison threshold=" + paramThreshold + ". Continue post failure? "
//								+ continueFurther);

						double tempMatch = getMatch(ctValue, ptPrValue, false);// Not a date check
						if (cmptype.equalsIgnoreCase("boolean")) {
							tempMatch = tempMatch != 100.0 ? 0.0 : tempMatch;
						}
						logger.debug(
								"Value match for Check Value Comparison Key " + currentChkValueKeyId + ":" + tempMatch);
						if (tempMatch >= paramThreshold) {
							match = match + tempMatch;
							numMatches++;
						}
					}
					// Takes care of divide by zero if there are no matches
					match = numMatches > 0 ? match / numMatches : match;
					logger.debug("Final Binning match =" + match);

					if (match >= avgCompareThreshold) {
						LinkedHashMap<Integer, String> displayParams = getSequencedDisplayParamsMap(compParamsObj);

						List<String> ctDisplayStrList = new ArrayList<String>();
						List<String> ptPrDisplayStrList = new ArrayList<String>();
						String ctBinDisplayValue = "";
						String ptPrBinDisplayValue = "";
						for (Integer eachSeqParam : displayParams.keySet()) {
							String displayParam = displayParams.get(eachSeqParam);
							Map<String, String> displayParamMnemonicMap = metaDataParamsObj.getMetaDataParams();
							String displayParamMnemonic = displayParamMnemonicMap.get(displayParam);
							HashMap<String, String> ctCompData = (HashMap<String, String>) dbCtBinDataMap
									.get(displayParam);
							HashMap<String, String> ptPrCompData = (HashMap<String, String>) dbPtPrBinDataMap
									.get(displayParam);
							ctDisplayStrList.add(displayParamMnemonic + ":" + ctCompData.get("value"));
							ptPrDisplayStrList.add(displayParamMnemonic + ":" + ptPrCompData.get("value"));
						}

						ctBinDisplayValue = String.join(",\n", ctDisplayStrList);
						ptPrBinDisplayValue = String.join(",\n", ptPrDisplayStrList);

						returnMapForList.put("ct_qs_value", ctBinDisplayValue);
						returnMapForList.put("ptPr_qs_value", ptPrBinDisplayValue);
						returnMapForList.put("match", String.format("%.1f%%", match));
						returnList.add(returnMapForList);
//						System.err.println("Added 1");
					}

				} else {
					logger.debug("Final Checkgate clearence status: FAILED.");
					break;
				}
				// END OF CHECK VALUE COMPARISON

			}

			ctRecordsList = EpodUtil.getDedupedRecords(ctRecordsList, ctRmvIdxNew);
			ctRmvIdxNew.clear();// Clear cache
			ptPrRecordsList = EpodUtil.getDedupedRecords(ptPrRecordsList, pPrRmvIdxNew);
			pPrRmvIdxNew.clear();

			/*
			 * Need to populate the display string with compare params like class codes,
			 * desc etc only from CT side. PTPR side no records for each compare param
			 */

			for (LinkedHashMap<String, String> eachCtDescRecords : ctRecordsList) {
				// Always a new HashMap
				LinkedHashMap<String, String> returnMapForList = getUnMatchedCTResultMap(eachCtDescRecords);

				String ctBinningStr = eachCtDescRecords.get("binning_str");
				HashMap<String, Object> dbCtBinDataMap = JsonUtil.getMixedDBJsonMap(ctBinningStr);

				LinkedHashMap<Integer, String> displayParams = getSequencedDisplayParamsMap(compParamsObj);
				List<String> ctDisplayStrList = new ArrayList<String>();
				List<String> ptPrDisplayStrList = new ArrayList<String>();
				String ctBinDisplayValue = "";
				String ptPrBinDisplayValue = "";
				for (Integer eachSeqParam : displayParams.keySet()) {
					String displayParam = displayParams.get(eachSeqParam);
					Map<String, String> displayParamMnemonicMap = metaDataParamsObj.getMetaDataParams();
					String displayParamMnemonic = displayParamMnemonicMap.get(displayParam);
					HashMap<String, String> ctCompData = (HashMap<String, String>) dbCtBinDataMap.get(displayParam);
					ctDisplayStrList.add(displayParamMnemonic + ":" + ctCompData.get("value"));
					ptPrDisplayStrList.add(displayParamMnemonic + ":" + "NO RECORDS");
				}

				ctBinDisplayValue = String.join(",\n", ctDisplayStrList);
				ptPrBinDisplayValue = String.join(",\n", ptPrDisplayStrList);

				returnMapForList.put("ct_qs_value", ctBinDisplayValue);
				returnMapForList.put("ptPr_qs_value", ptPrBinDisplayValue);

				returnMapForList.put("match", String.format("%.1f%%", 0.0));
				returnList.add(returnMapForList);
			}

			// CHANGE due to Group LOB, Policy LOB is not used any more, applicable CT lobs
			// are taken from Meta Data

			List<String> applicableLobs = Arrays.asList(metaDataMap.get("Extracts CT LOB(s)").split(","));

			for (LinkedHashMap<String, String> eachPtPrDescRecords : ptPrRecordsList) {

				if (applicableLobs.stream().anyMatch(eachPtPrDescRecords.get("extr_lob").trim()::equalsIgnoreCase)
						|| eachPtPrDescRecords.get("extr_lob").trim().equalsIgnoreCase("main")) {
					// Always a new HashMap
					LinkedHashMap<String, String> returnMapForList = getUnMatchedPtPrResultMap(eachPtPrDescRecords);

					String ptPrBinningStr = eachPtPrDescRecords.get("binning_str");
					HashMap<String, Object> dbPtPrBinDataMap = JsonUtil.getMixedDBJsonMap(ptPrBinningStr);
					LinkedHashMap<Integer, String> displayParams = getSequencedDisplayParamsMap(compParamsObj);

					List<String> ctDisplayStrList = new ArrayList<String>();
					List<String> ptPrDisplayStrList = new ArrayList<String>();
					String ctBinDisplayValue = "";
					String ptPrBinDisplayValue = "";
					for (Integer eachSeqParam : displayParams.keySet()) {
						String displayParam = displayParams.get(eachSeqParam);
						Map<String, String> displayParamMnemonicMap = metaDataParamsObj.getMetaDataParams();
						String displayParamMnemonic = displayParamMnemonicMap.get(displayParam);
						HashMap<String, String> ptPrCompData = (HashMap<String, String>) dbPtPrBinDataMap
								.get(displayParam);
						ctDisplayStrList.add(displayParamMnemonic + ":" + "NO RECORDS");
						ptPrDisplayStrList.add(displayParamMnemonic + ":" + ptPrCompData.get("value"));
					}

					ctBinDisplayValue = String.join(",\n", ctDisplayStrList);
					ptPrBinDisplayValue = String.join(",\n", ptPrDisplayStrList);

					returnMapForList.put("ct_qs_value", ctBinDisplayValue);
					returnMapForList.put("ptPr_qs_value", ptPrBinDisplayValue);

					returnMapForList.put("match", String.format("%.1f%%", 0.0));
					returnList.add(returnMapForList);
				}
			}

			// No need to show no data scenarios in binning records

//			/*
//			 * Section to deal with leftover records that had empty binning string
//			 */
//
//			for (LinkedHashMap<String, String> eachCtDescRecords : errorCtRecsList) {
//				// Always a new HashMap
//				LinkedHashMap<String, String> returnMapForList = getUnMatchedCTResultMap(eachCtDescRecords);
//
//				LinkedHashMap<Integer, String> displayParams = getSequencedDisplayParamsMap(compParamsObj);
//				List<String> ctDisplayStrList = new ArrayList<String>();
//				List<String> ptPrDisplayStrList = new ArrayList<String>();
//				String ctBinDisplayValue = "";
//				String ptPrBinDisplayValue = "";
//				for (Integer eachSeqParam : displayParams.keySet()) {
//					String displayParam = displayParams.get(eachSeqParam);
//					Map<String, String> displayParamMnemonicMap = metaDataParamsObj.getMetaDataParams();
//					String displayParamMnemonic = displayParamMnemonicMap.get(displayParam);
//					ctDisplayStrList.add(displayParamMnemonic + ":" + "EMPTY RECORD");
//					ptPrDisplayStrList.add(displayParamMnemonic + ":" + "NO RECORDS");
//				}
//
//				ctBinDisplayValue = String.join(",\n", ctDisplayStrList);
//				ptPrBinDisplayValue = String.join(",\n", ptPrDisplayStrList);
//
//				returnMapForList.put("ct_qs_value", ctBinDisplayValue);
//				returnMapForList.put("ptPr_qs_value", ptPrBinDisplayValue);
//
//				returnMapForList.put("match", String.format("%.1f%%", 0.0));
//				returnList.add(returnMapForList);
//			}
//
//			for (LinkedHashMap<String, String> eachPtPrDescRecords : errorPtPrRecsList) {
//
//				if (applicableLobs.stream().anyMatch(eachPtPrDescRecords.get("extr_lob").trim()::equalsIgnoreCase)
//						|| eachPtPrDescRecords.get("extr_lob").trim().equalsIgnoreCase("main")) {
//					// Always a new HashMap
//					LinkedHashMap<String, String> returnMapForList = getUnMatchedPtPrResultMap(eachPtPrDescRecords);
//
//					LinkedHashMap<Integer, String> displayParams = getSequencedDisplayParamsMap(compParamsObj);
//
//					List<String> ctDisplayStrList = new ArrayList<String>();
//					List<String> ptPrDisplayStrList = new ArrayList<String>();
//					String ctBinDisplayValue = "";
//					String ptPrBinDisplayValue = "";
//					for (Integer eachSeqParam : displayParams.keySet()) {
//						String displayParam = displayParams.get(eachSeqParam);
//						Map<String, String> displayParamMnemonicMap = metaDataParamsObj.getMetaDataParams();
//						String displayParamMnemonic = displayParamMnemonicMap.get(displayParam);
//						ctDisplayStrList.add(displayParamMnemonic + ":" + "NO RECORDS");
//						ptPrDisplayStrList.add(displayParamMnemonic + ":" + "EMPTY RECORD");
//					}
//
//					ctBinDisplayValue = String.join(",\n", ctDisplayStrList);
//					ptPrBinDisplayValue = String.join(",\n", ptPrDisplayStrList);
//
//					returnMapForList.put("ct_qs_value", ctBinDisplayValue);
//					returnMapForList.put("ptPr_qs_value", ptPrBinDisplayValue);
//
//					returnMapForList.put("match", String.format("%.1f%%", 0.0));
//					returnList.add(returnMapForList);
//				}
//			}
//			/*
//			 * End of dealing with error records.
//			 */

		} catch (org.json.simple.parser.ParseException e) {
			throw new ExtractHelperException("Parse Exception in getBinRecordMatch():" + e.getMessage(), e);
		}

		return returnList;
	}

	/**
	 * Method to get the comparison of Check Gate standard values
	 * 
	 * @param ctValue        Current term value
	 * @param ptPrValue      Prior term value
	 * @param paramThreshold Threshold for the check gate parameter
	 * @param cmptype        Comparison type
	 * @return boolean passed/failed
	 * @throws ExtractHelperException
	 */
	public static boolean hasPassedCheckGate(String ctValue, String ptPrValue, double paramThreshold, String cmptype)
			throws ExtractHelperException {
		double tempMatch = 0.0;
		if (ctValue.trim().equalsIgnoreCase(EpodUtil.NO_RECORDS)
				|| ptPrValue.trim().equalsIgnoreCase(EpodUtil.NO_RECORDS)) {
			tempMatch = 0.0;
		} else {
			tempMatch = getMatch(ctValue, ptPrValue, false);// Not a date check
			if (cmptype.equalsIgnoreCase("boolean")) {
				if (NumberUtils.isParsable(ctValue) && NumberUtils.isParsable(ptPrValue)) {
					tempMatch = tempMatch != 100.0 ? 0.0 : tempMatch;
				}
			} else {
				if (tempMatch >= paramThreshold) {
					if (cmptype.equalsIgnoreCase("boolean")) {
						tempMatch = tempMatch != 100.0 ? 0.0 : tempMatch;
					}
				}
			}

		}
		if (tempMatch > 0)
			return true;
		else
			return false;
	}

	/**
	 * Method to pre-process extract records to enable scoring
	 * 
	 * @param recordsList            Extracts
	 * @param scoreTemplateParamKeys from checklist JSON
	 * @return List of Maps with required data
	 * @throws org.json.simple.parser.ParseException
	 */
	@SuppressWarnings("unchecked")
	private static ArrayList<LinkedHashMap<String, String>> getPreProcessedScoringMap(
			ArrayList<LinkedHashMap<String, String>> recordsList, List<String> scoreTemplateParamKeys)
			throws org.json.simple.parser.ParseException {
		ArrayList<LinkedHashMap<String, String>> allBinRecsMapsList = new ArrayList<LinkedHashMap<String, String>>();
		/*
		 * While preparing for comparison also add the extr_id so that we can get the
		 * corresponding LinkedHashMap from ctRecordsList
		 * 
		 */

		for (LinkedHashMap<String, String> eachRcd : recordsList) {
			LinkedHashMap<String, String> eachRcdMap = new LinkedHashMap<String, String>();
			eachRcdMap.put("extr_id", eachRcd.get("extr_id"));// IS THIS NECESSARY?
			String binningStr = eachRcd.get("binning_str");
			HashMap<String, Object> dbBinDataMap = JsonUtil.getMixedDBJsonMap(binningStr);

			for (String eachKey : scoreTemplateParamKeys) {
				HashMap<String, String> tempMap = (HashMap<String, String>) dbBinDataMap.get(eachKey);
				eachRcdMap.put(eachKey, tempMap.get("value_std"));
			}

			allBinRecsMapsList.add(eachRcdMap);
		}
		return allBinRecsMapsList;
	}

	/**
	 * Method to pre-process, sequence and get Check Gate or Check Value Maps
	 * 
	 * @param allCompTemplParamMapsList Comparison Parameters List
	 * @param isCheckGateRequest        whether CheckGate or CheckValue
	 * @return Sequenced Map of Maps
	 */
	private static LinkedHashMap<Integer, LinkedHashMap<String, String>> getPreProcessedSequencedComparisonMaps(
			List<Map<String, String>> allCompTemplParamMapsList, boolean isCheckGateRequest) {
		LinkedHashMap<Integer, LinkedHashMap<String, String>> checkGateMaps = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();
		LinkedHashMap<Integer, LinkedHashMap<String, String>> checkValueMaps = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();

		for (Map<String, String> eachCompParamMap : allCompTemplParamMapsList) {
			LinkedHashMap<String, String> currentMap = new LinkedHashMap<String, String>();
			currentMap.put("id", eachCompParamMap.get("id"));
			currentMap.put("continuecheckfurther", eachCompParamMap.get("continuecheckfurther"));
			currentMap.put("threshold", eachCompParamMap.get("threshold"));
			currentMap.put("cmptype", eachCompParamMap.get("cmptype"));
			boolean isCheckGateKey = Boolean.parseBoolean(eachCompParamMap.get("checkgate"));
			int sequence = Integer.parseInt(eachCompParamMap.get("priority"));
			if (isCheckGateKey) {
				checkGateMaps.put(sequence, currentMap);
			} else {
				checkValueMaps.put(sequence, currentMap);
			}
		}
		// Sequence the maps
		TreeSet<Integer> checkGateKeysSet = new TreeSet<Integer>(checkGateMaps.keySet());
		TreeSet<Integer> checkValueKeysSet = new TreeSet<Integer>(checkValueMaps.keySet());
		LinkedHashMap<Integer, LinkedHashMap<String, String>> sequencedCheckGateMaps = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();
		LinkedHashMap<Integer, LinkedHashMap<String, String>> sequencedValueGateMaps = new LinkedHashMap<Integer, LinkedHashMap<String, String>>();

		int counter = 1;
		for (Integer eachKey : checkGateKeysSet) {
			sequencedCheckGateMaps.put(counter++, checkGateMaps.get(eachKey));
		}
		counter = 1;// reset to 1 before sequencing
		for (Integer eachKey : checkValueKeysSet) {
			sequencedValueGateMaps.put(counter++, checkValueMaps.get(eachKey));
		}

		if (isCheckGateRequest) {
			return sequencedCheckGateMaps;
		} else {
			return sequencedValueGateMaps;
		}
	}

	/**
	 * For binning questions, get the display parameters and the sequence
	 * 
	 * @param compParamsObj POJO for comparison objects from JSON
	 * @return Map of sequence and the parameter to be displayed
	 * @throws org.json.simple.parser.ParseException
	 */
	public static LinkedHashMap<Integer, String> getSequencedDisplayParamsMap(ComparisonParams compParamsObj)
			throws org.json.simple.parser.ParseException {
		List<Map<String, String>> compParamsMapsList = compParamsObj.getComparisonParams();
		LinkedHashMap<Integer, String> displayParamsConsidered = new LinkedHashMap<Integer, String>();
		for (Map<String, String> eachParamMap : compParamsMapsList) {
			String id = eachParamMap.get("id");
			int displaySeq = Integer.parseInt(eachParamMap.get("displayseq"));
			if (displaySeq > -1) {
				displayParamsConsidered.put(displaySeq, id);
			}
		}

		LinkedHashMap<Integer, String> seqDisplayParams = new LinkedHashMap<Integer, String>();
		TreeSet<Integer> displayParamsKeysSet = new TreeSet<Integer>(displayParamsConsidered.keySet());
		for (Integer eachDisplayParamKey : displayParamsKeysSet) {
			seqDisplayParams.put(eachDisplayParamKey, displayParamsConsidered.get(eachDisplayParamKey));
		}
		return seqDisplayParams;
	}

	/**
	 * Method that takes the raw Current Term and Prior Term Maps and provides a
	 * unified map with corresponding Current Term and Prior Term values
	 * 
	 * @param ctMap   Map with Current Term Values
	 * @param ptPrMap Map with Prior Term Values
	 * @return Map with current term and prior term values as current term and prior
	 *         term prefixes
	 */
	private static LinkedHashMap<String, String> getMatchedResultMap(LinkedHashMap<String, String> ctMap,
			LinkedHashMap<String, String> ptPrMap) {
		LinkedHashMap<String, String> transformedMap = new LinkedHashMap<String, String>();
		transformedMap.put("ct_qs_desc", ctMap.get("qs_desc"));
		transformedMap.put("ct_qs_desc_std", ctMap.get("qs_desc_std"));
		if (ctMap.get("qs_value").trim().length() == 0) {
			transformedMap.put("ct_qs_value", EpodUtil.NO_RECORDS);
		} else {
			transformedMap.put("ct_qs_value", ctMap.get("qs_value"));
		}
		transformedMap.put("ct_qs_valstd", ctMap.get("qs_value_std"));
		transformedMap.put("ct_qs_valtype", ctMap.get("val_type"));
		transformedMap.put("ct_file_path", ctMap.get("file_path"));
		transformedMap.put("ct_page_num", ctMap.get("page_num"));
		transformedMap.put("ct_line_num", ctMap.get("line_num"));
//		transformedMap.put("ct_file_lob", ctMap.get("file_lob"));
		transformedMap.put("ct_file_type", ctMap.get("file_type"));
		transformedMap.put("ct_file_id", ctMap.get("file_id"));
		transformedMap.put("ct_extr_id", ctMap.get("extr_id"));
		transformedMap.put("ct_extr_lob", ctMap.get("extr_lob"));

		transformedMap.put("ptPr_qs_desc", ptPrMap.get("qs_desc"));
		transformedMap.put("ptPr_qs_desc_std", ptPrMap.get("qs_desc_std"));
		if (ptPrMap.get("qs_value").trim().length() == 0) {
			transformedMap.put("ptPr_qs_value", EpodUtil.NO_RECORDS);
		} else {
			transformedMap.put("ptPr_qs_value", ptPrMap.get("qs_value"));
		}
		transformedMap.put("ptPr_qs_valstd", ptPrMap.get("qs_value_std"));
		transformedMap.put("ptPr_qs_valtype", ptPrMap.get("val_type"));
		transformedMap.put("ptPr_file_path", ptPrMap.get("file_path"));
		transformedMap.put("ptPr_page_num", ptPrMap.get("page_num"));
		transformedMap.put("ptPr_line_num", ptPrMap.get("line_num"));
//		transformedMap.put("ptPr_file_lob", ptPrMap.get("file_lob"));
		transformedMap.put("ptPr_file_type", ptPrMap.get("file_type"));
		transformedMap.put("ptPr_file_id", ptPrMap.get("file_id"));
		transformedMap.put("ptPr_extr_id", ptPrMap.get("extr_id"));
		transformedMap.put("ptPr_extr_lob", ptPrMap.get("extr_lob"));

		return transformedMap;
	}

	/**
	 * Method that takes the raw Current Term Map and without any Prior Term Map and
	 * provides a unified map with corresponding Current Term values and Prior Term
	 * "NO RECORDS"
	 * 
	 * @param ctMap Map with Current Term Values
	 * @return Map with current term and prior term values as current term and prior
	 *         term prefixes where prior term values are NO RECORDS
	 */
	private static LinkedHashMap<String, String> getUnMatchedCTResultMap(LinkedHashMap<String, String> ctMap) {
		LinkedHashMap<String, String> transformedMap = new LinkedHashMap<String, String>();
		transformedMap.put("ct_qs_desc", ctMap.get("qs_desc"));
		transformedMap.put("ct_qs_desc_std", ctMap.get("qs_desc_std"));
		if (ctMap.get("qs_value").trim().length() == 0) {
			transformedMap.put("ct_qs_value", EpodUtil.NO_RECORDS);
		} else {
			transformedMap.put("ct_qs_value", ctMap.get("qs_value"));
		}
		transformedMap.put("ct_qs_valstd", ctMap.get("qs_value_std"));
		transformedMap.put("ct_qs_valtype", ctMap.get("val_type"));
		transformedMap.put("ct_file_path", ctMap.get("file_path"));
		transformedMap.put("ct_page_num", ctMap.get("page_num"));
		transformedMap.put("ct_line_num", ctMap.get("line_num"));
//		transformedMap.put("ct_file_lob", ctMap.get("file_lob"));
		transformedMap.put("ct_file_type", ctMap.get("file_type"));
		transformedMap.put("ct_file_id", ctMap.get("file_id"));
		transformedMap.put("ct_extr_id", ctMap.get("extr_id"));

//		transformedMap.put("ct_extr_lob", ctMap.get("extr_lob"));
		if (ctMap.get("extr_lob").trim().length() == 0) {
			transformedMap.put("ct_extr_lob", EpodUtil.NO_RECORDS);
		} else {
			transformedMap.put("ct_extr_lob", ctMap.get("extr_lob"));
		}

		transformedMap.put("ptPr_qs_desc", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_qs_desc_std", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_qs_value", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_qs_valstd", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_qs_valtype", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_file_path", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_page_num", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_line_num", EpodUtil.NO_RECORDS);
//		transformedMap.put("ptPr_file_lob", SDSLUtil.NO_RECORDS);
		transformedMap.put("ptPr_file_type", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_file_id", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_extr_id", EpodUtil.NO_RECORDS);
		transformedMap.put("ptPr_extr_lob", EpodUtil.NO_RECORDS);

		return transformedMap;
	}

	/**
	 * Method that takes the raw Prior Term Map and without any Current Term Map and
	 * provides a unified map with corresponding Prior Term values and Current Term
	 * "NO RECORDS"
	 * 
	 * @param ptPrMap Map with Prior Term Values
	 * @return Map with current term and prior term values as current term and prior
	 *         term prefixes where current term values are NO RECORDS
	 */
	private static LinkedHashMap<String, String> getUnMatchedPtPrResultMap(LinkedHashMap<String, String> ptPrMap) {
		LinkedHashMap<String, String> transformedMap = new LinkedHashMap<String, String>();

		transformedMap.put("ptPr_qs_desc", ptPrMap.get("qs_desc"));
		transformedMap.put("ptPr_qs_desc_std", ptPrMap.get("qs_desc_std"));
		if (ptPrMap.get("qs_value").trim().length() == 0) {
			transformedMap.put("ptPr_qs_value", EpodUtil.NO_RECORDS);
		} else {
			transformedMap.put("ptPr_qs_value", ptPrMap.get("qs_value"));
		}
		transformedMap.put("ptPr_qs_valstd", ptPrMap.get("qs_value_std"));
		transformedMap.put("ptPr_qs_valtype", ptPrMap.get("val_type"));
		transformedMap.put("ptPr_file_path", ptPrMap.get("file_path"));
		transformedMap.put("ptPr_page_num", ptPrMap.get("page_num"));
		transformedMap.put("ptPr_line_num", ptPrMap.get("line_num"));
//		transformedMap.put("ptPr_file_lob", ptPrMap.get("file_lob"));
		transformedMap.put("ptPr_file_type", ptPrMap.get("file_type"));
		transformedMap.put("ptPr_file_id", ptPrMap.get("file_id"));
		transformedMap.put("ptPr_extr_id", ptPrMap.get("extr_id"));

//		transformedMap.put("ptPr_extr_lob", ptPrMap.get("extr_lob"));
		if (ptPrMap.get("extr_lob").trim().length() == 0) {
			transformedMap.put("ptPr_extr_lob", EpodUtil.NO_RECORDS);
		} else {
			transformedMap.put("ptPr_extr_lob", ptPrMap.get("extr_lob"));
		}

		transformedMap.put("ct_qs_desc", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_qs_desc_std", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_qs_value", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_qs_valstd", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_qs_valtype", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_file_path", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_page_num", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_line_num", EpodUtil.NO_RECORDS);
//		transformedMap.put("ct_file_lob", SDSLUtil.NO_RECORDS);
		transformedMap.put("ct_file_type", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_file_id", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_extr_id", EpodUtil.NO_RECORDS);
		transformedMap.put("ct_extr_lob", EpodUtil.NO_RECORDS);

		return transformedMap;
	}

	/**
	 * Get match value for two strings. Checks for date range match, numeric match
	 * and String match. Handles strings that has one numeric and the other String.
	 * Numeric matches are binary value match, String matches are based on text
	 * distance. Returns a zero score if one string is numeric and the other String.
	 * 
	 * @param val1        Value 1
	 * @param val2        Value 2
	 * @param isDateRange Date range comparison
	 * @return Match score as a double value
	 * @throws ExtractHelperException Extract Helper Exception
	 */
	private static double getMatch(String val1, String val2, boolean isDateRange) throws ExtractHelperException {
		double match = 0.0;
		if (isDateRange) {
			match = getDateRangeMatch(val1, val2);
		} else {
			if (NumberUtils.isParsable(val1) && NumberUtils.isParsable(val2)) {
				match = Double.parseDouble(val1) == Double.parseDouble(val2) ? 100.0 : 0.0;
			}
			// Removed because of ME1 issue:25% $1561 and $1561
//			else if (NumberUtils.isParsable(val1) && !NumberUtils.isParsable(val2)) {
//				match = 0.0;
//			} else if (NumberUtils.isParsable(val2) && !NumberUtils.isParsable(val1)) {
//				match = 0.0;
//			} 
			else {
				match = EpodUtil.getStringMatch(val1, val2);
			}
		}
		return match;
	}

	/**
	 * Compares the Dates Range and returns a match score. This is a Binary match
	 * 
	 * @param ctValue   Current Term Date Value
	 * @param ptPrValue Prior Term Date Value
	 * @return Match score
	 * @throws ExtractHelperException Extract Helper Exception
	 */
	private static double getDateRangeMatch(String ctValue, String ptPrValue) throws ExtractHelperException {
		double matchScore = 0.0;
		// Split by space
		String[] ctDates = ctValue.split("\\s+");
		String[] ptDates = ptPrValue.split("\\s+");

		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");

		// To handle cases where only one date is available and cannot be split
		if (ctDates.length != 2 || ptDates.length != 2) {
			try {
				Date date0 = df.parse(ptPrValue.trim());
				Date date1 = df.parse(ctValue.trim());
				long timeDifference = Math.abs(date1.getTime() - date0.getTime());
				if (isMarketedPolicy) {
					// No difference
					matchScore = timeDifference == 0L ? 100 : 0;
				} else {
					// One year difference
					matchScore = timeDifference == 86400000L ? 100 : 0;
				}
			} catch (ParseException e) {
				throw new ExtractHelperException("Parse Exception in single date in getDateMatch():" + e.getMessage(),
						e);
			}
			return matchScore;
		}

		/*
		 * When to date and from date is available with both CT and PTPR and can be
		 * split
		 */
		try {
			Date date0 = df.parse(ctDates[0].trim());
			Date date1 = df.parse(ctDates[1].trim());

			if (date0.getTime() < date1.getTime()) {
				String tempDateArrayStr = ctDates[0].trim();
				ctDates[0] = ctDates[1].trim();
				ctDates[1] = tempDateArrayStr;
				Date tempDate = date0;
				date0 = date1;
				date1 = tempDate;
			}
			long ctDifference = date0.getTime() - date1.getTime();

			date0 = df.parse(ptDates[0].trim());
			date1 = df.parse(ptDates[1].trim());

			if (date0.getTime() < date1.getTime()) {
				String tempDateArrayStr = ptDates[0].trim();
				ptDates[0] = ptDates[1];
				ptDates[1] = tempDateArrayStr;
				Date tempDate = date0;
				date0 = date1;
				date1 = tempDate;
			}
			long ptDifference = date0.getTime() - date1.getTime();

			if (ctDifference == ptDifference
					|| Math.abs(ctDifference - ptDifference) == 86400000L && ctDates[1].equals(ptDates[0])) {
				matchScore = 100.0;
			}
		} catch (ParseException e) {
			throw new ExtractHelperException("Parse Exception in getDateMatch():" + e.getMessage(), e);
		}
		return matchScore;
	}

	/**
	 * Method to flip the CT and PT/PR values in case of marketed
	 * 
	 * @param arrayMatchMapList List of maps with score
	 * @return List of flipped maps with score
	 */
	public static ArrayList<LinkedHashMap<String, String>> flipMarketed(
			ArrayList<LinkedHashMap<String, String>> arrayMatchMapList) {
		ArrayList<LinkedHashMap<String, String>> flippedList = new ArrayList<LinkedHashMap<String, String>>();
		for (LinkedHashMap<String, String> eachMap : arrayMatchMapList) {
			LinkedHashMap<String, String> tempMap = new LinkedHashMap<String, String>();// Every time new Map

			tempMap.put("ct_qs_desc", eachMap.get("ptPr_qs_desc"));
			tempMap.put("ct_qs_desc_std", eachMap.get("ptPr_qs_desc_std"));
			tempMap.put("ct_qs_value", eachMap.get("ptPr_qs_value"));
			tempMap.put("ct_qs_valstd", eachMap.get("ptPr_qs_valstd"));
			tempMap.put("ct_qs_valtype", eachMap.get("ptPr_qs_valtype"));
			tempMap.put("ct_file_path", eachMap.get("ptPr_file_path"));
			tempMap.put("ct_page_num", eachMap.get("ptPr_page_num"));
			tempMap.put("ct_line_num", eachMap.get("ptPr_line_num"));
//			tempMap.put("ct_file_lob", eachMap.get("ptPr_file_lob"));
			tempMap.put("ct_file_type", eachMap.get("ptPr_file_type"));
			tempMap.put("ct_file_id", eachMap.get("ptPr_file_id"));
			tempMap.put("ct_extr_id", eachMap.get("ptPr_extr_id"));
			tempMap.put("ct_extr_lob", eachMap.get("ptPr_extr_lob"));

			tempMap.put("ptPr_qs_desc", eachMap.get("ct_qs_desc"));
			tempMap.put("ptPr_qs_desc_std", eachMap.get("ct_qs_desc_std"));
			tempMap.put("ptPr_qs_value", eachMap.get("ct_qs_value"));
			tempMap.put("ptPr_qs_valstd", eachMap.get("ct_qs_valstd"));
			tempMap.put("ptPr_qs_valtype", eachMap.get("ct_qs_valtype"));
			tempMap.put("ptPr_file_path", eachMap.get("ct_file_path"));
			tempMap.put("ptPr_page_num", eachMap.get("ct_page_num"));
			tempMap.put("ptPr_line_num", eachMap.get("ct_line_num"));
//			tempMap.put("ptPr_file_lob", eachMap.get("ct_file_lob"));
			tempMap.put("ptPr_file_type", eachMap.get("ct_file_type"));
			tempMap.put("ptPr_file_id", eachMap.get("ct_file_id"));
			tempMap.put("ptPr_extr_id", eachMap.get("ct_extr_id"));
			tempMap.put("ptPr_extr_lob", eachMap.get("ct_extr_lob"));

			tempMap.put("match", eachMap.get("match"));
			flippedList.add(tempMap);
		}
		return flippedList;
	}

	/**
	 * Returns relevant/applicable checklist questions for single line and multiple
	 * lines
	 * 
	 * @param isSingleRecord Single Record or Multiple Record Type
	 * @return List with question keys
	 */
	public static ArrayList<String> getConfigQsList(boolean isSingleRecord) {
		String qsStr = isSingleRecord ? (String) ConfigLoader.slMlRcdQsProps.getProperty("SINGLERECORD")
				: (String) ConfigLoader.slMlRcdQsProps.getProperty("MULTIRECORD");
		return new ArrayList<String>(Arrays.asList(qsStr.split(",")));
	}

//	/**
//	 * Returns relevant/applicable checklist questions for single line and multiple
//	 * lines
//	 * 
//	 * @param isSingleRecord Single Record or Multiple Record Type
//	 * @return List with question keys
//	 */
//	public static ArrayList<String> testGetConfigQsList(String recordType) {
//		return ConfigLoader.testAllQs.get(recordType);
//	}

	/**
	 * Returns relevant/applicable checklist binning questions
	 * 
	 * @return List with question keys
	 */
	public static ArrayList<String> getBinningConfigQsList() {
		String qsStr = (String) ConfigLoader.binRcdQsProps.getProperty("BINNINGRECORD");
		return new ArrayList<String>(Arrays.asList(qsStr.split(",")));
	}

	/**
	 * Checks for validity of a checklist question for a LOB
	 * 
	 * @param qsKey Checklist question
	 * @param lob   LOB value
	 * @return true if question key part of LOB
	 * @throws ExtractHelperException Extract Helper Exception
	 */
	public static boolean isInCurrentLob(String qsKey, String lob) throws ExtractHelperException {
		try {
			int _IntIdx = qsKey.indexOf("_INT_");
			if (_IntIdx > -1) {
				qsKey = qsKey.substring(0, _IntIdx);
			}
			String lobsStr = (String) ConfigLoader.lobQsMappingProps.getProperty(qsKey);
			if (lobsStr == null) {// If a particular question is not mapped to this lob
//				logger.debug(qsKey + " from DB is ignored as not applicable to lob:" + lob);
				return false;
			} else if (lobsStr.equals("all")) {
				return true;
			} else if (lobsStr.indexOf(",") > 1) {
				String[] qsLobArray = lobsStr.split(",");
				return Arrays.asList(qsLobArray).contains(lob);
			} else if (lobsStr.trim().length() > 0) {// Takes care of situations where only one LOB is applicable for
														// example SC1 and LOB is woc
				String[] qsLobArray = { lobsStr };
				return Arrays.asList(qsLobArray).contains(lob);
			}
			return false;
		} catch (Exception e) {
			logger.debug("Exception in isInCurrentLob()");
			throw new ExtractHelperException("Exception in isInCurrentLob():" + e.getMessage(), e);
		}

	}

	/**
	 * Scoring records connector for a question. Score is built with weighted
	 * average.
	 * 
	 * @param ctRecordsList   Current Term records
	 * @param ptPrRecordsList Prior Term records
	 * @param qs_Key          Checklist Question to be used in scoring
	 * @return A List with Maps of Current Term and Prior Term indices with score
	 *         based pairing of the indices.
	 */
	private static List<LinkedHashMap<String, Integer>> getScoredIndexedList(
			ArrayList<LinkedHashMap<String, String>> ctRecordsList,
			ArrayList<LinkedHashMap<String, String>> ptPrRecordsList, String qs_Key) {

		boolean isLobDependent = Boolean.parseBoolean(ConfigLoader.lobDependencyProps.getProperty(qs_Key));
		boolean isDescMatch = Boolean.parseBoolean(ConfigLoader.descMatchProps.getProperty(qs_Key));
		double qsThreshold = Double.parseDouble((String) ConfigLoader.finalCompareProps.getProperty(qs_Key));
		double descThreshold = Double.parseDouble((String) ConfigLoader.descThresholdsProps.getProperty(qs_Key));

		double descWt = Double.parseDouble((String) ConfigLoader.descWtProps.getProperty(qs_Key));
		double lobWt = Double.parseDouble((String) ConfigLoader.lobWtProps.getProperty(qs_Key));
		double stdValWt = Double.parseDouble((String) ConfigLoader.stdValWtProps.getProperty(qs_Key));
		double avgThreshold = Double.parseDouble((String) ConfigLoader.finalScoringThresholdProps.getProperty(qs_Key));// segregating

		// 6x change
//		return ScoringUtil.getScoreHash(ctRecordsList, ptPrRecordsList, qs_Key, isLobDependent, isDescMatch,
//				qsThreshold, descThreshold, descWt, lobWt, stdValWt, avgThreshold);

		return ScoringUtil.getScoredMlRecords(ctRecordsList, ptPrRecordsList, qs_Key,
				ConfigLoader.slMlMetaConfigMaps.get(qs_Key), ConfigLoader.slMlScoreConfigMaps.get(qs_Key),
				ConfigLoader.slMlCompareConfigMaps.get(qs_Key));

	}

	public static boolean isJsonValid(String jsonInString) {
		try {
			if (JsonUtil.isEmptyJSON(jsonInString))
				return false;
			else
				return true;
		} catch (org.json.simple.parser.ParseException e) {
			return false;
		}
	}
}
