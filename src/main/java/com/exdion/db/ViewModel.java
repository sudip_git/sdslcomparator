/**
 * Copyright (C) 2021, Sumyag Data Sciences Pvt.Ltd. - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */

package com.exdion.db;

import java.sql.Timestamp;

/**
 * @author Sudip Das
 *
 */
public class ViewModel {
	// From extracts
	private String extr_id;
	private String file_id;
	private String pack_id;
	private String job_id;
	private String qs_key;
	private String qs_desc;
	private String qs_value;
	private String qs_desc_std;
	private String qs_value_std;
	private String val_type;
	private String lob;
	private int line_num;
	private int page_num;
	private double x0;
	private double x1;
	private double y0;
	private double y1;
	private String qs_section;
	private String qs_sub_section;
	private double disp_y0;
	private double disp_y1;
	private double page_width;
	private double page_height;
	private String extr_json;
	private String child_lob;
	// From Jobs
	private String input_source;
	private String output_dstn;
	private String job_status;
	private Timestamp job_start_time;
	private Timestamp job_end_time;
	// From file:
	private String file_path;
	private String file_type;
	private String file_status;
	// SYG
	private String file_lob;

	private String file_pp;
	private String file_pn;
	private int total_pages;
	// file_metadata:
	private int word_count;
	private String doc_type;
	// derived
	private String pack_key;
	// status:
	private String status_type;
	private String status_desc;

	public String getExtr_id() {
		return extr_id;
	}

	public void setExtr_id(String extr_id) {
		this.extr_id = extr_id;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getPack_id() {
		return pack_id;
	}

	public void setPack_id(String pack_id) {
		this.pack_id = pack_id;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getQs_key() {
		return qs_key;
	}

	public void setQs_key(String qs_key) {
		this.qs_key = qs_key;
	}

	public String getQs_desc() {
		return qs_desc;
	}

	public void setQs_desc(String qs_desc) {
		this.qs_desc = qs_desc;
	}

	public String getQs_value() {
		return qs_value;
	}

	public void setQs_value(String qs_value) {
		this.qs_value = qs_value;
	}

	public String getQs_desc_std() {
		return qs_desc_std;
	}

	public void setQs_desc_std(String qs_desc_std) {
		this.qs_desc_std = qs_desc_std;
	}

	public String getQs_value_std() {
		return qs_value_std;
	}

	public void setQs_value_std(String qs_value_std) {
		this.qs_value_std = qs_value_std;
	}

	public String getVal_type() {
		return val_type;
	}

	public void setVal_type(String val_type) {
		this.val_type = val_type;
	}

	public String getLob() {
		return lob;
	}

	public void setLob(String lob) {
		this.lob = lob;
	}

	public int getLine_num() {
		return line_num;
	}

	public void setLine_num(int line_num) {
		this.line_num = line_num;
	}

	public int getPage_num() {
		return page_num;
	}

	public void setPage_num(int page_num) {
		this.page_num = page_num;
	}

	public double getX0() {
		return x0;
	}

	public void setX0(double x0) {
		this.x0 = x0;
	}

	public double getX1() {
		return x1;
	}

	public void setX1(double x1) {
		this.x1 = x1;
	}

	public double getY0() {
		return y0;
	}

	public void setY0(double y0) {
		this.y0 = y0;
	}

	public double getY1() {
		return y1;
	}

	public void setY1(double y1) {
		this.y1 = y1;
	}

	public String getQs_section() {
		return qs_section;
	}

	public void setQs_section(String qs_section) {
		this.qs_section = qs_section;
	}

	public String getQs_sub_section() {
		return qs_sub_section;
	}

	public void setQs_sub_section(String qs_sub_section) {
		this.qs_sub_section = qs_sub_section;
	}

	public double getDisp_y0() {
		return disp_y0;
	}

	public void setDisp_y0(double disp_y0) {
		this.disp_y0 = disp_y0;
	}

	public double getDisp_y1() {
		return disp_y1;
	}

	public void setDisp_y1(double disp_y1) {
		this.disp_y1 = disp_y1;
	}

	public double getPage_width() {
		return page_width;
	}

	public void setPage_width(double page_width) {
		this.page_width = page_width;
	}

	public double getPage_height() {
		return page_height;
	}

	public void setPage_height(double page_height) {
		this.page_height = page_height;
	}

	public String getExtr_json() {
		return extr_json;
	}

	public void setExtr_json(String extr_json) {
		this.extr_json = extr_json;
	}

	public String getChild_lob() {
		return child_lob;
	}

	public void setChild_lob(String child_lob) {
		this.child_lob = child_lob;
	}

	public String getInput_source() {
		return input_source;
	}

	public void setInput_source(String input_source) {
		this.input_source = input_source;
	}

	public String getOutput_dstn() {
		return output_dstn;
	}

	public void setOutput_dstn(String output_dstn) {
		this.output_dstn = output_dstn;
	}

	public String getJob_status() {
		return job_status;
	}

	public void setJob_status(String job_status) {
		this.job_status = job_status;
	}

	public Timestamp getJob_start_time() {
		return job_start_time;
	}

	public void setJob_start_time(Timestamp job_start_time) {
		this.job_start_time = job_start_time;
	}

	public Timestamp getJob_end_time() {
		return job_end_time;
	}

	public void setJob_end_time(Timestamp job_end_time) {
		this.job_end_time = job_end_time;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getFile_status() {
		return file_status;
	}

	public void setFile_status(String file_status) {
		this.file_status = file_status;
	}

	public String getFile_lob() {
		return file_lob;
	}

	public void setFile_lob(String file_lob) {
		this.file_lob = file_lob;
	}

	public String getFile_pp() {
		return file_pp;
	}

	public void setFile_pp(String file_pp) {
		this.file_pp = file_pp;
	}

	public String getFile_pn() {
		return file_pn;
	}

	public void setFile_pn(String file_pn) {
		this.file_pn = file_pn;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

	public int getWord_count() {
		return word_count;
	}

	public void setWord_count(int word_count) {
		this.word_count = word_count;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public String getPack_key() {
		return pack_key;
	}

	public void setPack_key(String pack_key) {
		this.pack_key = pack_key;
	}

	public String getStatus_type() {
		return status_type;
	}

	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}

	public String getStatus_desc() {
		return status_desc;
	}

	public void setStatus_desc(String status_desc) {
		this.status_desc = status_desc;
	}

}
/*
 * 
 * // Extracts: file_id, pack_id, job_id, qs_key, qs_desc, qs_value,
 * qs_desc_std, // qs_value_std,lob, line_num, page_num, x0,x1,y0,y1, //
 * qs_section,qs_sub_section,disp_y0,disp_y1,page_width,page_height,extr_json,
 * child_lob,
 * 
 * // String, String, String, String, String, String, String, String, String, //
 * Integer, Integer, Double,Double,Double,Double // String, String,
 * Double,Double, Double, Double, String, String
 * 
 * // Jobs: input_source,output_dstn,job_status,job_start_time,job_end_time, //
 * String, String, String, Timestamp, Timestamp
 * 
 * // file:file_path,file_type,file_status,file_pp,file_pn,total_pages, //
 * String, String, String, String, String, Integer
 * 
 * // file_metadata:word_count,doc_type,pack_key, // Integer, String, String
 * 
 * // status: status_type // String
 */