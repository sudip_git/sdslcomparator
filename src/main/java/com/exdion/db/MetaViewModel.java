/**
 * Copyright (C) 2021, Sumyag Data Sciences Pvt.Ltd. - www.sumyag.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
 */
package com.exdion.db;

/**
 * @author Sudip Das
 *
 */
public class MetaViewModel {
	// From file:
	private String file_path;
	private String file_type;
	private String file_status;
	// SYG
	private String file_lob;
	private String file_pp;
	private String file_pn;
	// file_metadata:
	private String file_meta_id;
	private String job_id;
	private String pack_id;
	private String file_id;
	private String doc_type;
	private int total_pages;
	private int native_pages;
	private int focr_pages;
	private int pocr_pages;
	private int blank_pages;
	private int image_count;
	private int total_chars;
	private String doc_lob;
	private int word_count;
	// derived
	private String pack_key;
	// status:
	private String status_type;
	private String status_desc;

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getFile_status() {
		return file_status;
	}

	public void setFile_status(String file_status) {
		this.file_status = file_status;
	}

	public String getFile_lob() {
		return file_lob;
	}

	public void setFile_lob(String file_lob) {
		this.file_lob = file_lob;
	}

	public String getFile_pp() {
		return file_pp;
	}

	public void setFile_pp(String file_pp) {
		this.file_pp = file_pp;
	}

	public String getFile_pn() {
		return file_pn;
	}

	public void setFile_pn(String file_pn) {
		this.file_pn = file_pn;
	}

	public String getFile_meta_id() {
		return file_meta_id;
	}

	public void setFile_meta_id(String file_meta_id) {
		this.file_meta_id = file_meta_id;
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getPack_id() {
		return pack_id;
	}

	public void setPack_id(String pack_id) {
		this.pack_id = pack_id;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

	public String getDoc_type() {
		return doc_type;
	}

	public void setDoc_type(String doc_type) {
		this.doc_type = doc_type;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public void setTotal_pages(int total_pages) {
		this.total_pages = total_pages;
	}

	public int getNative_pages() {
		return native_pages;
	}

	public void setNative_pages(int native_pages) {
		this.native_pages = native_pages;
	}

	public int getFocr_pages() {
		return focr_pages;
	}

	public void setFocr_pages(int focr_pages) {
		this.focr_pages = focr_pages;
	}

	public int getPocr_pages() {
		return pocr_pages;
	}

	public void setPocr_pages(int pocr_pages) {
		this.pocr_pages = pocr_pages;
	}

	public int getBlank_pages() {
		return blank_pages;
	}

	public void setBlank_pages(int blank_pages) {
		this.blank_pages = blank_pages;
	}

	public int getImage_count() {
		return image_count;
	}

	public void setImage_count(int image_count) {
		this.image_count = image_count;
	}

	public int getTotal_chars() {
		return total_chars;
	}

	public void setTotal_chars(int total_chars) {
		this.total_chars = total_chars;
	}

	public String getDoc_lob() {
		return doc_lob;
	}

	public void setDoc_lob(String doc_lob) {
		this.doc_lob = doc_lob;
	}

	public int getWord_count() {
		return word_count;
	}

	public void setWord_count(int word_count) {
		this.word_count = word_count;
	}

	public String getPack_key() {
		return pack_key;
	}

	public void setPack_key(String pack_key) {
		this.pack_key = pack_key;
	}

	public String getStatus_type() {
		return status_type;
	}

	public void setStatus_type(String status_type) {
		this.status_type = status_type;
	}

	public String getStatus_desc() {
		return status_desc;
	}

	public void setStatus_desc(String status_desc) {
		this.status_desc = status_desc;
	}
}
