/**
 * Copyright (C) 2018, Exdion Solutions - www.exdion.com
All rights reserved.

Redistribution, modification or use of source and binary forms are prohibited, without explicit 
permission from the owner and specific license agreements. 
For permitted users, modification is permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. All advertising materials mentioning features or use of this software must
   display the following acknowledgement:
   This product includes software developed by Sumyag Data Sciences Private Limited, 
   associated partners and leverages third party libraries.

4. Neither the name of the copyright holder nor the names of its contributors
   may be used to endorse or promote products derived from this software
   without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

 */

package com.exdion.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.javatuples.Triplet;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.exdion.comparator.ExtractHelper;
import com.exdion.exceptions.ComparatorException;
import com.exdion.exceptions.DBException;
import com.exdion.exceptions.ExtractHelperException;
import com.exdion.helper.ConfigLoader;
import com.exdion.helper.EpodUtil;
import com.exdion.helper.S3Handler;
import com.fasterxml.uuid.Generators;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.sdsl.binning.GsonUtils;
import com.sdsl.db.DbUtil;
import com.sdsl.db.PGresDb;
import com.sdsl.utils.JsonUtil;

/**
 * @author Sudip
 *
 */

public class DBController {
	/**
	 * Default logger
	 */
	final static Logger logger = Logger.getLogger(DBController.class.getName());
	/**
	 * Global Job ID
	 */
	private String jobID = "";
	/**
	 * View Name
	 */
	private String baseViewNameJobID = "";

	/**
	 * Meta View Name for meta data
	 */
	private String metaViewName = "";

	/**
	 * Current view name based on sub packs
	 */
	private String currentExtractsViewName = "";

	/**
	 * Connection
	 */
	private Connection conn = null;

	/**
	 * Prepared Statements
	 */

	private PreparedStatement traceInsertPS = null;
	private PreparedStatement viewLogInsertPS = null;
	private PreparedStatement viewLogUpdatePS = null;
	private PreparedStatement viewLogDeleteRecordPS = null;

	/**
	 * List of extract jsons to be used for Log Trace
	 */
	private ArrayList<JSONObject> extrJsons = new ArrayList<JSONObject>();
	/**
	 * Persistence Beans
	 */
	private ArrayList<ViewModel> viewRecsList = new ArrayList<ViewModel>();
	private ArrayList<MetaViewModel> metaViewRecsList = new ArrayList<MetaViewModel>();
	private ArrayList<String> filesWrongNamesList = new ArrayList<String>();

	private String[] viewJobsObjJSONKeys = { "packs", "input_source", "output_dstn", "job_status", "job_start_time",
			"job_end_time" };
	private String[] viewPacksObjJSONKeys = { "files", "pack_id" };//
	private String[] viewFilesObjJSONKeys = { "extracts", "file_id", "file_path", "file_type", "file_status",
			"file_lob", "file_pp", "file_pn", "total_pages", "word_count", "doc_type", "status_type", "status_desc" };
	private String[] viewExtractsObjJSONKeys = { "extr_id", "qs_key", "qs_desc", "qs_value", "qs_desc_std",
			"qs_value_std", "val_type", "lob", "line_num", "page_num", "x0", "x1", "y0", "y1", "qs_section",
			"qs_sub_section", "disp_y0", "disp_y1", "page_width", "page_height", "extr_json", "child_lob" };

	private String[] metaViewPacksObjJSONKeys = { "files", "pack_id" };
	private String[] metaViewFilesObjJSONKeys = { "file_path", "file_type", "file_status", "file_lob", "file_pp",
			"file_pn", "file_meta_id", "file_id", "doc_type", "total_pages", "native_pages", "focr_pages", "pocr_pages",
			"blank_pages", "image_count", "total_chars", "doc_lob", "word_count", "status_type", "status_desc" };

	/**
	 * Constructor to invoke
	 * 
	 * @param jobID Core engine generated Job ID
	 * @throws ComparatorException Comparator Exception
	 * @throws DBException         DB Exception
	 */
	public DBController(String jobID, String s3Path, String region, boolean isJSONDriven)
			throws ComparatorException, DBException {
		this.jobID = jobID;
		/*
		 * If isJsonDriven is true, need to create the meta view else will be created in
		 * controller_initialize
		 */

		if (isJSONDriven)
			createViewsFromJSON(s3Path, region);
		else
			getConnection();
	}

	/**
	 * Method to get Job ID
	 * 
	 * @return Core engine generated Job ID
	 */
	public String getJobID() {
		return jobID;
	}

	/**
	 * Method to get view name associated with a Job ID
	 * 
	 * @return View Name
	 */
	public String getCurrentExtractsViewName() {
		return currentExtractsViewName;
	}

	/**
	 * Method to get meta view name associated with a Job ID
	 * 
	 * @return Meta View Name
	 */
	public String getMetaViewName() {
		return metaViewName;
	}

	/**
	 * Get Root Directory name to use for Summary File Name
	 * 
	 * @param jobID Core engine generated Job ID
	 * @return Root Directory
	 */
	private String getRootDir(String jobID) {
		String rootDirName = jobID;// If any error it will take Job ID for Summary file name

		/*
		 * Initial file path->s3://exdiopodprod2021/1008csr125112021072541/Input/
		 * 1008csr125112021072541_tru_pt00.pdf.
		 * 
		 * Substring: Begin->third ordinal index of ("/")+1 and End-> fourth ordinal
		 * index of ("/"). Gives-1008csr125112021072541 as the rootDirName. Removes s3
		 * bucket: s3://exdiopodprod2021/
		 */
		String file_path = metaViewRecsList.get(0).getFile_path();

		// if the file_path is null or length zero use default value of
		// rootDirName=jobid
		file_path = file_path == null ? rootDirName : file_path.trim().length() == 0 ? rootDirName : file_path.trim();

		rootDirName = file_path.substring(StringUtils.ordinalIndexOf(file_path, "/", 3) + 1,
				StringUtils.ordinalIndexOf(file_path, "/", 4));

		// If the substring still contains any "/" then revert to jobId
		rootDirName = rootDirName.contains("/") ? jobID : rootDirName;
		if (rootDirName.equalsIgnoreCase(jobID)) {
			logger.error("WARNING: Input folder not stuctured as per convention.");
			logger.info("NOTE: Using Job ID\"" + jobID + "\" for Summary File Name");
		}

		return rootDirName;
	}

	/**
	 * Method to create a connection to DB
	 * 
	 * @throws ComparatorException Comparator Exception
	 * @throws DBException         DB Exception
	 */
	private void getConnection() throws ComparatorException, DBException {
		try {
			logger.info("Establishing DB connection........");

			PGresDb postGresDB = new PGresDb();
			conn = postGresDB.getConnection(ConfigLoader.dbProps);

			logger.info("Fetching data ........");
			controller_initialize();
//			return conn;
		} catch (ClassNotFoundException e) {
			logger.fatal("ERROR: ClassNotFoundException in establising Cloud DB connection");
			throw new ComparatorException("In getConnection()", e);
		} catch (SQLException e) {
			logger.fatal("ERROR: SQLException in establising Cloud DB connection.");
			throw new DBException("In getConnection()", e);
		}
	}

	/**
	 * Method to terminate the old connection and get a new one. The Prepared
	 * Statements are also initialized as the old compiled ones were ties to the old
	 * connection. The new connection will now have handle to new compiled Prepared
	 * Statements
	 * 
	 * @throws ComparatorException
	 * @throws DBException
	 */
	public void refreshConnection() throws ComparatorException, DBException {
		try {
			if (conn != null && !conn.isClosed()) {
				if (!conn.getAutoCommit()) {
					conn.commit();
					conn.setAutoCommit(true);
				}
				conn.close();
				conn = null;
			}
			logger.info("Re-establishing DB connection........");

			PGresDb postGresDB = new PGresDb();
			conn = postGresDB.getConnection(ConfigLoader.dbProps);

			// VERY VERY IMPORTANT STEP. With old connection the PS are terminated
			logger.debug("Initializing PS again........");
			initializePS();

			logger.info("Fetching data ........");
		} catch (ClassNotFoundException e) {
			logger.fatal("ERROR: ClassNotFoundException in establising Cloud DB connection");
			throw new ComparatorException("In getConnection()", e);
		} catch (SQLException e) {
			logger.fatal("ERROR: SQLException in establising Cloud DB connection.");
			throw new DBException("In getConnection()", e);
		}

	}

	/**
	 * Method to initialize the DB controller and create views and Prepared
	 * Statements
	 * 
	 * @throws DBException DB Exception
	 */
	private void initializePS() throws DBException {

		try {

			String sqlQuery = "INSERT INTO trace VALUES(?,?,?,?,?)";
			traceInsertPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "INSERT INTO view_log VALUES(?,?,?)";
			viewLogInsertPS = conn.prepareStatement(sqlQuery);

			sqlQuery = "UPDATE view_log set view_ts=? where job_id=? and view_name=?";
			viewLogUpdatePS = conn.prepareStatement(sqlQuery);

			sqlQuery = "DELETE FROM view_log WHERE view_name=?";
			viewLogDeleteRecordPS = conn.prepareStatement(sqlQuery);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in Prepared Starement initilization.");
			throw new DBException("In initializePS()", e);
		}
	}

	/**
	 * Method to initialize the DB controller and create views and Prepared
	 * Statements
	 * 
	 * @throws DBException DB Exception
	 */
	private void controller_initialize() throws DBException {

		baseViewNameJobID = "VIEW_" + StringUtils.remove(jobID, "-");

		// meta view name
		metaViewName = "META_" + baseViewNameJobID;

		initializePS();

		// Drop all older views, reference from view log table and then create the new
		// views
		dropOldViews();

		// Views are created at the end as viewLogInsertPS will be used to insert into
		// View Log

		// Create only meta view now. Extracts view later
		createMetaView();

	}

	/**
	 * Method to gracefully close connection, Prepared Statements, drop views and
	 * prepare for shutdown
	 * 
	 * @throws DBException DB Exception
	 */
	public void controller_finalize() throws DBException {
		try {
			if (traceInsertPS != null) {
				traceInsertPS.close();
				traceInsertPS = null;
			}
			if (viewLogInsertPS != null) {
				viewLogInsertPS.close();
				viewLogInsertPS = null;
			}
			if (viewLogUpdatePS != null) {
				viewLogUpdatePS.close();
				viewLogUpdatePS = null;
			}
			if (viewLogDeleteRecordPS != null) {
				viewLogDeleteRecordPS.close();
				viewLogDeleteRecordPS = null;
			}

			// extracts view drops are happening from the comparator class since subpacks
			// used
//			dropView(baseViewNameJobID);

			// drop meta view for the jobId
			dropView(metaViewName);

			if (conn != null && !conn.isClosed()) {
				if (!conn.getAutoCommit()) {
					conn.commit();
					conn.setAutoCommit(true);
				}
				conn.close();
				conn = null;
			}

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in controller finalize.");
			throw new DBException("In controller_finalize()", e);
		}
	}

//SYG
	/**
	 * Creates view of file_metadata, files and status at a Job level. Useful for
	 * summary tab data
	 * 
	 * @throws DBException
	 */
	private void createMetaView() throws DBException {
		ArrayList<String> allViews = new ArrayList<String>();
		try {
			DbUtil.dropMaterializedView(conn, metaViewName);
			String sqlQuery = "CREATE MATERIALIZED VIEW  " + metaViewName + "  AS \n" + "SELECT file_metadata.*,  \n"
					+ "files.file_path,files.file_type,files.file_status, files.file_lob, \n"
					+ "files.file_pp, files.file_pn, \n" + "concat (files.file_pn,'_',files.file_lob) AS pack_key, \n"
					+ "status.status_type, status.status_desc \n" + "FROM public.files files \n"
					+ "INNER JOIN public.file_metadata file_metadata ON files.file_id=file_metadata.file_id \n"
					+ "INNER JOIN public.status status ON files.file_status=status.status_name \n"
					+ "WHERE files.job_id='" + jobID + "' WITH DATA";

			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sqlQuery);
			logger.debug("Meta View:" + metaViewName);

			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
			persistViews(metaViewName);
			allViews.add(metaViewName);
			insertViewLog(allViews);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in creating meta view.");
			throw new DBException("In createMetaView()", e);
		}
	}

	/**
	 * Create extract views for a sublist of packs from the JobId
	 * 
	 * @param subPackIdsList
	 * @param viewSeqNum
	 * @throws DBException
	 */
	public void createExtractViewForSubPacks(List<String> subPackIdsList, int viewSeqNum) throws DBException {
		ArrayList<String> allViews = new ArrayList<String>();
		// Seq added to end, view cannot start with number
		currentExtractsViewName = baseViewNameJobID + "_" + viewSeqNum;
		String subPackIdsStr = String.join("', '", subPackIdsList);
		try {
			/*
			 * LEFT Join introduced instead of INNER JOIN for packs not in file_metadata
			 * 202011116: 5.1_Pipeline_Changes: files.word_count replaced with
			 * file_metadata.word_count. New subPackid based view
			 */

			DbUtil.dropMaterializedView(conn, currentExtractsViewName);

			String sqlQuery = "CREATE MATERIALIZED VIEW " + currentExtractsViewName + " AS  \n"
					+ "SELECT extracts.*, jobs.input_source,jobs.output_dstn,\n"
					+ "jobs.job_status,jobs.job_start_time,jobs.job_end_time,\n"
					+ "files.file_path,files.file_type,files.file_status,files.file_lob,  \n"
					+ "files.file_pp, files.file_pn,files.total_pages,\n"
					+ "file_metadata.word_count, file_metadata.doc_type, concat (files.file_pn,'_',files.file_lob) AS pack_key, \n"
					+ "status.status_type, status.status_desc \n" + "FROM public.extracts extracts   \n"
					+ "INNER JOIN public.files files ON extracts.file_id=files.file_id \n"
					+ "INNER JOIN public.jobs jobs ON extracts.job_id=jobs.job_id \n"
					+ "LEFT JOIN public.file_metadata file_metadata ON files.file_id=file_metadata.file_id \n"
					+ "INNER JOIN public.status status ON files.file_status=status.status_name\n"
					+ "WHERE jobs.job_id='" + jobID + "' and extracts.pack_id IN ('" + subPackIdsStr + "') WITH DATA";

			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sqlQuery);
			logger.debug("Extracts View: " + currentExtractsViewName);

			if (stmt != null) {
				stmt.close();
				stmt = null;
			}

			persistViews(currentExtractsViewName);
			allViews.add(currentExtractsViewName);
			insertViewLog(allViews);

//			// GENERATING JSON CODE
//			generateJson(currentExtractsViewName);
//			System.exit(0);

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in creating views.");
			throw new DBException("In createView()", e);
		}
	}

	/**
	 * Creating ViewModels of extracts from JSON and then meta view models
	 * 
	 * @param s3ViewJSON
	 * @throws DBException
	 */
	private void createViewsFromJSON(String s3Path, String region) throws DBException {
		logger.info("Fetching data from AWS ........");
		String s3JSON = "";
		try {
			s3JSON = S3Handler.downloadS3FileAsString(s3Path, region, getJobID() + ".json");
		} catch (IOException ioex) {
			logger.error("ERROR:" + getJobID() + ".json file not available in " + "syg.comparator.jsons");
			throw new DBException("In createExtractsView()", ioex);
		} catch (AmazonS3Exception awsex) {
			logger.error("ERROR:" + awsex.getMessage());
			throw new DBException("In createViewsFromJSON()", awsex);
		}

		ArrayList<LinkedHashMap<Object, Object>> allExtractsRecords = new ArrayList<LinkedHashMap<Object, Object>>();

		JSONParser parser = new JSONParser();
		JSONObject jobObj;
		try {
			jobObj = (JSONObject) parser.parse(s3JSON);
		} catch (ParseException e) {
			logger.error("ERROR: ParseException while parsing JSON from file.");
			throw new DBException("In createViewsFromJSON()", e);
		}

		JSONArray viewRelatedArray = (JSONArray) jobObj.get("view");
		for (int i = 0; i < viewRelatedArray.size(); i++) {
			JSONObject jobsWithPacksObject = (JSONObject) viewRelatedArray.get(i);
			/*
			 * Here only key "packs" is called. The remaining keys like input_source,
			 * output_dstn, job_status,job_start_time, job_end_time are called from the
			 * jobsWithPacksObject in the extracts loop.
			 * 
			 * If pack key is not there raise exception.
			 */

			if (!EpodUtil.checkKeysExist(jobsWithPacksObject, viewJobsObjJSONKeys)) {
				logger.error("ERROR: JSON file does not have all keys as per schema of jobs array.");
				throw new DBException("In createViewsFromJSON()", new Exception("Jobs Array Keys missing"));
			}

			JSONArray packsArray = (JSONArray) jobsWithPacksObject.get("packs");
			for (int j = 0; j < packsArray.size(); j++) {
				JSONObject packsWithFilesObj = (JSONObject) packsArray.get(j);
				/*
				 * Here only key "files" is called. The remaining key like pack_id is called
				 * from the packsWithFilesObj in the extracts loop
				 */
				if (!EpodUtil.checkKeysExist(packsWithFilesObj, viewPacksObjJSONKeys)) {
					logger.error("ERROR: JSON file does not have all keys as per schema of packs array.");
					throw new DBException("In createExtractsView()", new Exception("Packs Array Keys missing"));
				}

				JSONArray filesArray = (JSONArray) packsWithFilesObj.get("files");
				for (int k = 0; k < filesArray.size(); k++) {
					JSONObject filesWithExtractsObj = (JSONObject) filesArray.get(k);
					if (!EpodUtil.checkKeysExist(filesWithExtractsObj, viewFilesObjJSONKeys)) {
						logger.error("ERROR: JSON file does not have all keys as per schema of files array.");
						throw new DBException("In createExtractsView()", new Exception("Files Array Keys missing"));
					}

					JSONArray extractsArray = (JSONArray) filesWithExtractsObj.get("extracts");
					/*
					 * Extracts loop: All key values added from extractObj and then from
					 * filesWithExtractsObj, packsWithFilesObj and finally jobsWithPacksObject
					 */

					for (int m = 0; m < extractsArray.size(); m++) {
						LinkedHashMap<Object, Object> currentExtractRec = new LinkedHashMap<Object, Object>();
						JSONObject extractObj = (JSONObject) extractsArray.get(m);
						if (!EpodUtil.checkKeysExist(extractObj, viewExtractsObjJSONKeys)) {
							logger.error("ERROR: JSON file does not have all keys as per schema of extracts array.");
							throw new DBException("In createViewsFromJSON()",
									new Exception("Extracts Array Keys missing"));
						}
						// Deal with null values in extracts
						currentExtractRec.put("extr_id", extractObj.get("extr_id"));
						currentExtractRec.put("qs_key", EpodUtil.dealWithNull(extractObj.get("qs_key")));
						currentExtractRec.put("qs_desc", EpodUtil.dealWithNull(extractObj.get("qs_desc")));
						currentExtractRec.put("qs_value", EpodUtil.dealWithNull(extractObj.get("qs_value")));
						currentExtractRec.put("qs_desc_std", EpodUtil.dealWithNull(extractObj.get("qs_desc_std")));
						currentExtractRec.put("qs_value_std", EpodUtil.dealWithNull(extractObj.get("qs_value_std")));
						currentExtractRec.put("val_type", EpodUtil.dealWithNull(extractObj.get("val_type")));
						currentExtractRec.put("lob", EpodUtil.dealWithNull(extractObj.get("lob")));
						currentExtractRec.put("line_num", EpodUtil.dealWithNull(extractObj.get("line_num")));
						currentExtractRec.put("page_num", EpodUtil.dealWithNull(extractObj.get("page_num")));
						currentExtractRec.put("x0", EpodUtil.dealWithNull(extractObj.get("x0")));
						currentExtractRec.put("x1", EpodUtil.dealWithNull(extractObj.get("x1")));
						currentExtractRec.put("y0", EpodUtil.dealWithNull(extractObj.get("y0")));
						currentExtractRec.put("y1", EpodUtil.dealWithNull(extractObj.get("y1")));
						currentExtractRec.put("qs_section",
								EpodUtil.dealWithNull(EpodUtil.dealWithNull(extractObj.get("qs_section"))));
						currentExtractRec.put("qs_sub_section",
								EpodUtil.dealWithNull(extractObj.get("qs_sub_section")));
						currentExtractRec.put("disp_y0", EpodUtil.dealWithNull(extractObj.get("disp_y0")));
						currentExtractRec.put("disp_y1", EpodUtil.dealWithNull(extractObj.get("disp_y1")));
						currentExtractRec.put("page_width", EpodUtil.dealWithNull(extractObj.get("page_width")));
						currentExtractRec.put("page_height", EpodUtil.dealWithNull(extractObj.get("page_height")));
						currentExtractRec.put("extr_json", EpodUtil.dealWithNull(extractObj.get("extr_json")));
						currentExtractRec.put("child_lob", EpodUtil.dealWithNull(extractObj.get("child_lob")));
						currentExtractRec.put("file_id", filesWithExtractsObj.get("file_id"));// filesWithExtractsObj
						currentExtractRec.put("pack_id", packsWithFilesObj.get("pack_id"));// packsWithFilesObj
						currentExtractRec.put("input_source", jobsWithPacksObject.get("input_source"));// jobsWithPacksObject
						currentExtractRec.put("output_dstn", jobsWithPacksObject.get("output_dstn"));// jobsWithPacksObject
						currentExtractRec.put("job_status", jobsWithPacksObject.get("job_status"));// jobsWithPacksObject
						currentExtractRec.put("job_id", getJobID());// jobId

						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

							Date parsedDate = dateFormat.parse(jobsWithPacksObject.get("job_start_time").toString());
							Timestamp timeStamp = new Timestamp(parsedDate.getTime());
							currentExtractRec.put("job_start_time", timeStamp);

							parsedDate = dateFormat.parse(jobsWithPacksObject.get("job_end_time").toString());
							timeStamp = new Timestamp(parsedDate.getTime());
							currentExtractRec.put("job_end_time", timeStamp);

						} catch (java.text.ParseException e) {
							logger.error("ERROR: ParseException while parsing Dates.");
							throw new DBException("In createViewsFromJSON()", e);
						}
						currentExtractRec.put("file_path", filesWithExtractsObj.get("file_path"));
						currentExtractRec.put("file_type", filesWithExtractsObj.get("file_type"));
						currentExtractRec.put("file_status", filesWithExtractsObj.get("file_status"));
						currentExtractRec.put("file_lob", filesWithExtractsObj.get("file_lob"));
						currentExtractRec.put("file_pp", filesWithExtractsObj.get("file_pp"));
						currentExtractRec.put("file_pn", filesWithExtractsObj.get("file_pn"));
						currentExtractRec.put("total_pages", filesWithExtractsObj.get("total_pages"));
						currentExtractRec.put("word_count", filesWithExtractsObj.get("word_count"));
						currentExtractRec.put("doc_type", filesWithExtractsObj.get("doc_type"));

						// Generated pack_key
						currentExtractRec.put("pack_key", EpodUtil.dealWithNull(filesWithExtractsObj.get("file_pn"))
								+ '_' + EpodUtil.dealWithNull(filesWithExtractsObj.get("file_lob")));

						currentExtractRec.put("status_type", filesWithExtractsObj.get("status_type"));
						currentExtractRec.put("status_desc", filesWithExtractsObj.get("status_desc"));

						allExtractsRecords.add(currentExtractRec);
					}
				}
			}
		}
		logger.debug("Total Extract Records:" + allExtractsRecords.size());
		persistExtractsView(allExtractsRecords);

		// Create meta view models
		JSONArray metaViewRelatedArray = (JSONArray) jobObj.get("meta");
		createMetaViewFromJSON(metaViewRelatedArray);
	}

	/**
	 * Create Meta View Models of Files Metadata
	 * 
	 * @param metaArray
	 * @throws DBException
	 */
	private void createMetaViewFromJSON(JSONArray metaArray) throws DBException {

		ArrayList<LinkedHashMap<Object, Object>> allFileMetaRecords = new ArrayList<LinkedHashMap<Object, Object>>();

		/*
		 * This provides an array with two Json Objects: Packs Array and Files missed
		 * array. Use the array position to create the meta view objects and also the
		 * files missed data.
		 */
		// First object is packs array
		JSONObject jobsWithPacksObject = (JSONObject) metaArray.get(0);

		JSONArray packsArray = (JSONArray) jobsWithPacksObject.get("packs");
		for (int j = 0; j < packsArray.size(); j++) {
			JSONObject packsWithFilesObj = (JSONObject) packsArray.get(j);
			if (!EpodUtil.checkKeysExist(packsWithFilesObj, metaViewPacksObjJSONKeys)) {
				logger.error("ERROR: JSON file does not have all keys as per schema of meta packs array.");
				throw new DBException("In createMetaViewFromJSON()", new Exception("Meta Packs Array Keys missing"));
			}

			JSONArray filesArray = (JSONArray) packsWithFilesObj.get("files");

			for (int k = 0; k < filesArray.size(); k++) {
				LinkedHashMap<Object, Object> currentFileRec = new LinkedHashMap<Object, Object>();
				JSONObject filesObj = (JSONObject) filesArray.get(k);
				if (!EpodUtil.checkKeysExist(filesObj, metaViewFilesObjJSONKeys)) {
					logger.error("ERROR: JSON file does not have all keys as per schema of files array.");
					throw new DBException("In createMetaViewFromJSON()", new Exception("Files Array Keys missing"));
				}

				currentFileRec.put("file_id", EpodUtil.dealWithNull(filesObj.get("file_id")));
				currentFileRec.put("file_meta_id", EpodUtil.dealWithNull(filesObj.get("file_meta_id")));
				currentFileRec.put("file_path", EpodUtil.dealWithNull(filesObj.get("file_path")));
				currentFileRec.put("file_type", EpodUtil.dealWithNull(filesObj.get("file_type")));
				currentFileRec.put("file_status", EpodUtil.dealWithNull(filesObj.get("file_status")));
				currentFileRec.put("file_lob", EpodUtil.dealWithNull(filesObj.get("file_lob")));
				currentFileRec.put("file_pp", EpodUtil.dealWithNull(filesObj.get("file_pp")));
				currentFileRec.put("file_pn", EpodUtil.dealWithNull(filesObj.get("file_pn")));
				currentFileRec.put("doc_type", EpodUtil.dealWithNull(filesObj.get("doc_type")));
				currentFileRec.put("total_pages", EpodUtil.dealWithNull(filesObj.get("total_pages")));
				currentFileRec.put("native_pages", EpodUtil.dealWithNull(filesObj.get("native_pages")));
				currentFileRec.put("focr_pages", EpodUtil.dealWithNull(filesObj.get("focr_pages")));
				currentFileRec.put("pocr_pages", EpodUtil.dealWithNull(filesObj.get("pocr_pages")));
				currentFileRec.put("blank_pages", EpodUtil.dealWithNull(filesObj.get("blank_pages")));
				currentFileRec.put("image_count", EpodUtil.dealWithNull(filesObj.get("image_count")));
				currentFileRec.put("total_chars", EpodUtil.dealWithNull(filesObj.get("total_chars")));
				currentFileRec.put("doc_lob", EpodUtil.dealWithNull(filesObj.get("doc_lob")));
				currentFileRec.put("word_count", EpodUtil.dealWithNull(filesObj.get("word_count")));
				currentFileRec.put("status_type", EpodUtil.dealWithNull(filesObj.get("status_type")));
				currentFileRec.put("status_desc", EpodUtil.dealWithNull(filesObj.get("status_desc")));
				// Generated pack_key
				currentFileRec.put("pack_key", EpodUtil.dealWithNull(filesObj.get("file_pn")) + '_'
						+ EpodUtil.dealWithNull(filesObj.get("file_lob")));

				currentFileRec.put("pack_id", packsWithFilesObj.get("pack_id"));// packsWithFilesObj
				currentFileRec.put("job_id", getJobID());// jobId

				allFileMetaRecords.add(currentFileRec);
			}
		}
		logger.debug("Total Meta View Records:" + allFileMetaRecords.size());

		jobsWithPacksObject = (JSONObject) metaArray.get(1);

		// Second object is files missed array
		JSONArray files_missedArray = (JSONArray) jobsWithPacksObject.get("files_missed");

		for (int j = 0; j < files_missedArray.size(); j++) {
			JSONObject wrongNamedFile = (JSONObject) files_missedArray.get(j);
			filesWrongNamesList.add(wrongNamedFile.get("file_path").toString());
		}

		persistMetaView(allFileMetaRecords);

	}

	/**
	 * Method to persist view data from JSON as view model (extract view)
	 * 
	 * @param allExtractsRecords
	 */
	@SuppressWarnings("unchecked")
	private void persistExtractsView(ArrayList<LinkedHashMap<Object, Object>> allExtractsRecords) {

		for (LinkedHashMap<Object, Object> eachExtractRec : allExtractsRecords) {

			// Creation of the ViewModel.viewRecsList
			ViewModel viewBean = new ViewModel();

			viewBean.setExtr_id(eachExtractRec.get("extr_id").toString());
			viewBean.setFile_id(eachExtractRec.get("file_id").toString());
			viewBean.setPack_id(eachExtractRec.get("pack_id").toString());
			viewBean.setJob_id(eachExtractRec.get("job_id").toString());
			viewBean.setQs_key(eachExtractRec.get("qs_key").toString());
			viewBean.setQs_desc(eachExtractRec.get("qs_desc").toString());
			viewBean.setQs_value(eachExtractRec.get("qs_value").toString());
			viewBean.setQs_desc_std(eachExtractRec.get("qs_desc_std").toString());
			viewBean.setQs_value_std(eachExtractRec.get("qs_value_std").toString());
			viewBean.setVal_type(eachExtractRec.get("val_type").toString());
			viewBean.setLob(eachExtractRec.get("lob").toString());
			viewBean.setLine_num(Integer.parseInt(eachExtractRec.get("line_num").toString()));
			viewBean.setPage_num(Integer.parseInt(eachExtractRec.get("page_num").toString()));
			viewBean.setX0(Double.parseDouble(eachExtractRec.get("x0").toString()));
			viewBean.setX1(Double.parseDouble(eachExtractRec.get("x1").toString()));
			viewBean.setY0(Double.parseDouble(eachExtractRec.get("y0").toString()));
			viewBean.setY1(Double.parseDouble(eachExtractRec.get("y1").toString()));
			viewBean.setPage_width(Double.parseDouble(eachExtractRec.get("page_width").toString()));
			viewBean.setPage_height(Double.parseDouble(eachExtractRec.get("page_height").toString()));
			viewBean.setExtr_json(eachExtractRec.get("extr_json").toString());
			viewBean.setChild_lob(eachExtractRec.get("child_lob").toString());
			viewBean.setInput_source(eachExtractRec.get("input_source").toString());
			viewBean.setOutput_dstn(eachExtractRec.get("output_dstn").toString());
			viewBean.setJob_status(eachExtractRec.get("job_status").toString());
			viewBean.setJob_start_time((Timestamp) eachExtractRec.get("job_start_time"));
			viewBean.setJob_end_time((Timestamp) eachExtractRec.get("job_end_time"));
			viewBean.setFile_path(eachExtractRec.get("file_path").toString());
			viewBean.setFile_type(eachExtractRec.get("file_type").toString());
			viewBean.setFile_status(eachExtractRec.get("file_status").toString());
			viewBean.setFile_lob(eachExtractRec.get("file_lob").toString());
			viewBean.setFile_pp(eachExtractRec.get("file_pp").toString());
			viewBean.setFile_pn(eachExtractRec.get("file_pn").toString());
			viewBean.setTotal_pages(Integer.parseInt(eachExtractRec.get("total_pages").toString()));
			viewBean.setWord_count(Integer.parseInt(eachExtractRec.get("word_count").toString()));
			viewBean.setDoc_type(eachExtractRec.get("doc_type").toString());
			viewBean.setPack_key(eachExtractRec.get("pack_key").toString());
			viewBean.setStatus_type(eachExtractRec.get("status_type").toString());
			viewBean.setStatus_desc(eachExtractRec.get("status_desc").toString());

			viewRecsList.add(viewBean);

			// Populate the extract Json objects lists to add as Trace record
			JSONObject jsonObj = new JSONObject();
			jsonObj.put(viewBean.getExtr_id(), viewBean.getExtr_json());
			extrJsons.add(jsonObj);
		}
	}

	/**
	 * Method to persist meta view data from JSON as meta view model (f view)
	 * 
	 * @param allFileMetaRecords
	 */
	private void persistMetaView(ArrayList<LinkedHashMap<Object, Object>> allFileMetaRecords) {

		for (LinkedHashMap<Object, Object> eachExtractRec : allFileMetaRecords) {
			MetaViewModel metaViewBean = new MetaViewModel();

			metaViewBean.setFile_path(eachExtractRec.get("file_path").toString());
			metaViewBean.setFile_type(eachExtractRec.get("file_type").toString());
			metaViewBean.setFile_status(eachExtractRec.get("file_status").toString());
			// SYG
			metaViewBean.setFile_lob(eachExtractRec.get("file_lob").toString());

			metaViewBean.setFile_pp(eachExtractRec.get("file_pp").toString());
			metaViewBean.setFile_pn(eachExtractRec.get("file_pn").toString());

			metaViewBean.setFile_meta_id(eachExtractRec.get("file_meta_id").toString());
			metaViewBean.setJob_id(eachExtractRec.get("job_id").toString());
			metaViewBean.setPack_id(eachExtractRec.get("pack_id").toString());
			metaViewBean.setFile_id(eachExtractRec.get("file_id").toString());
			metaViewBean.setDoc_type(eachExtractRec.get("doc_type").toString());
			metaViewBean.setTotal_pages(Integer.parseInt(eachExtractRec.get("total_pages").toString()));
			metaViewBean.setNative_pages(Integer.parseInt(eachExtractRec.get("native_pages").toString()));
			metaViewBean.setFocr_pages(Integer.parseInt(eachExtractRec.get("focr_pages").toString()));
			metaViewBean.setPocr_pages(Integer.parseInt(eachExtractRec.get("pocr_pages").toString()));
			metaViewBean.setBlank_pages(Integer.parseInt(eachExtractRec.get("blank_pages").toString()));
			metaViewBean.setImage_count(Integer.parseInt(eachExtractRec.get("image_count").toString()));
			metaViewBean.setTotal_chars(Integer.parseInt(eachExtractRec.get("total_chars").toString()));
			metaViewBean.setDoc_lob(eachExtractRec.get("doc_lob").toString());
			metaViewBean.setWord_count(Integer.parseInt(eachExtractRec.get("word_count").toString()));
			// SYG
			metaViewBean.setPack_key(eachExtractRec.get("pack_key").toString());
			metaViewBean.setStatus_type(eachExtractRec.get("status_type").toString());
			metaViewBean.setStatus_desc(eachExtractRec.get("status_desc").toString());

			metaViewRecsList.add(metaViewBean);
		}

	}

	/**
	 * Dropping the view created for the current subPacks
	 * 
	 * @param currentViewName
	 */
	public void deleteViewForSubPacks(String currentViewName) {
		dropView(currentViewName);
	}

	/**
	 * Populating the model Beans to persist data based on view name
	 * 
	 * @param currentViewName
	 * @throws DBException
	 */
	@SuppressWarnings("unchecked")
	private void persistViews(String currentViewName) throws DBException {
		String sqlQuery = "";
		Statement stmt = null;
		ResultSet resultSet = null;

		try {
			if (currentViewName.equals(metaViewName)) {
				sqlQuery = "SELECT * from " + metaViewName;
				stmt = conn.createStatement();
				resultSet = stmt.executeQuery(sqlQuery);
				while (resultSet.next()) {
					MetaViewModel metaViewBean = new MetaViewModel();

					metaViewBean.setFile_path(resultSet.getString("file_path"));
					metaViewBean.setFile_type(resultSet.getString("file_type"));
					metaViewBean.setFile_status(resultSet.getString("file_status"));
					// SYG
					metaViewBean.setFile_lob(resultSet.getString("file_lob"));

					metaViewBean.setFile_pp(resultSet.getString("file_pp"));
					metaViewBean.setFile_pn(resultSet.getString("file_pn"));

					metaViewBean.setFile_meta_id(resultSet.getString("file_meta_id"));
					metaViewBean.setJob_id(resultSet.getString("job_id"));
					metaViewBean.setPack_id(resultSet.getString("pack_id"));
					metaViewBean.setFile_id(resultSet.getString("file_id"));
					metaViewBean.setDoc_type(resultSet.getString("doc_type"));
					metaViewBean.setTotal_pages(resultSet.getInt("total_pages"));
					metaViewBean.setNative_pages(resultSet.getInt("native_pages"));
					metaViewBean.setFocr_pages(resultSet.getInt("focr_pages"));
					metaViewBean.setPocr_pages(resultSet.getInt("pocr_pages"));
					metaViewBean.setBlank_pages(resultSet.getInt("blank_pages"));
					metaViewBean.setImage_count(resultSet.getInt("image_count"));
					metaViewBean.setTotal_chars(resultSet.getInt("total_chars"));
					metaViewBean.setDoc_lob(resultSet.getString("doc_lob"));
					metaViewBean.setWord_count(resultSet.getInt("word_count"));
					// SYG
					metaViewBean.setPack_key(resultSet.getString("pack_key"));
					metaViewBean.setStatus_type(resultSet.getString("status_type"));
					metaViewBean.setStatus_desc(resultSet.getString("status_desc"));

					metaViewRecsList.add(metaViewBean);
				}
			} else {

				sqlQuery = "SELECT * from " + currentViewName;
				stmt = conn.createStatement();
				resultSet = stmt.executeQuery(sqlQuery);

				while (resultSet.next()) {

					ViewModel viewBean = new ViewModel();

					viewBean.setExtr_id(resultSet.getString("extr_id"));
					viewBean.setFile_id(resultSet.getString("file_id"));
					viewBean.setPack_id(resultSet.getString("pack_id"));
					viewBean.setJob_id(resultSet.getString("job_id"));
					viewBean.setQs_key(resultSet.getString("qs_key"));
					viewBean.setQs_desc(resultSet.getString("qs_desc"));
					viewBean.setQs_value(resultSet.getString("qs_value"));
					viewBean.setQs_desc_std(resultSet.getString("qs_desc_std"));
					viewBean.setQs_value_std(resultSet.getString("qs_value_std"));
					viewBean.setVal_type(resultSet.getString("val_type"));
					viewBean.setLob(resultSet.getString("lob"));
					viewBean.setLine_num(resultSet.getInt("line_num"));
					viewBean.setPage_num(resultSet.getInt("page_num"));
					viewBean.setX0(resultSet.getDouble("x0"));
					viewBean.setX1(resultSet.getDouble("x1"));
					viewBean.setY0(resultSet.getDouble("y0"));
					viewBean.setY1(resultSet.getDouble("y1"));
					viewBean.setPage_width(resultSet.getDouble("page_width"));
					viewBean.setPage_height(resultSet.getDouble("page_height"));
					viewBean.setExtr_json(resultSet.getString("extr_json"));
					viewBean.setChild_lob(resultSet.getString("child_lob"));
					viewBean.setInput_source(resultSet.getString("input_source"));
					viewBean.setOutput_dstn(resultSet.getString("output_dstn"));
					viewBean.setJob_status(resultSet.getString("job_status"));
					viewBean.setJob_start_time(resultSet.getTimestamp("job_start_time"));
					viewBean.setJob_end_time(resultSet.getTimestamp("job_end_time"));
					viewBean.setFile_path(resultSet.getString("file_path"));
					viewBean.setFile_type(resultSet.getString("file_type"));
					viewBean.setFile_status(resultSet.getString("file_status"));
					viewBean.setFile_lob(resultSet.getString("file_lob"));
					viewBean.setFile_pp(resultSet.getString("file_pp"));
					viewBean.setFile_pn(resultSet.getString("file_pn"));
					viewBean.setTotal_pages(resultSet.getInt("total_pages"));
					viewBean.setWord_count(resultSet.getInt("word_count"));
					viewBean.setDoc_type(resultSet.getString("doc_type"));
					viewBean.setPack_key("pack_key");
					viewBean.setStatus_type(resultSet.getString("status_type"));
					viewBean.setStatus_desc(resultSet.getString("status_desc"));

					viewRecsList.add(viewBean);

					// Populate the extract Json objects lists
					JSONObject jsonObj = new JSONObject();
					jsonObj.put(viewBean.getExtr_id(), viewBean.getExtr_json());
					extrJsons.add(jsonObj);
				}
			}

			logger.debug("Persistence for " + currentViewName + " completed.");

			if (resultSet != null) {
				resultSet.close();
				resultSet = null;
			}
			if (stmt != null) {
				stmt.close();
				stmt = null;
			}
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in persisting Beans for view: " + currentViewName);
			throw new DBException("In persistViews()", e);
		}
	}

	/**
	 * Check if the job_id is available
	 * 
	 * @return
	 */
	public boolean checkJobExists() {
		return metaViewRecsList.size() > 0 ? true : false;
	}

	/**
	 * Method to drop a view
	 * 
	 * @param viewName View Name
	 */
	private void dropView(String viewName) {
		try {
			DbUtil.dropMaterializedView(conn, viewName);
			logger.debug("Dropped View:" + viewName);
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in dropping views." + e.getMessage());
		}
	}

	/**
	 * Method to determine policy type (Automatic Renewal / Marketed)
	 * 
	 * @param packId Pack ID
	 * @return AR or Marketed
	 * 
	 */
	public String getPolicyType(String packId) {

		Set<String> fileTypesSet = new HashSet<String>();

		for (ViewModel eachViewBean : viewRecsList) {
			if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
				fileTypesSet.add(eachViewBean.getFile_type());
			}
		}

		List<String> fileTypesList = new ArrayList<String>(fileTypesSet);

		for (String eachFileType : fileTypesList) {
			if (StringUtils.startsWithIgnoreCase(eachFileType, "PR")) {
				return "Marketed";
			}
		}
		return "Automatic Renewal";
	}

	/**
	 * Gets distinct file paths, policy number and job start date for each pack
	 * 
	 * @param packId Pack ID
	 * @return Map of meta data
	 */
	public HashMap<String, String> getMetaData(String packId) {

		HashMap<String, String> metaDataMap = new HashMap<String, String>();
		Set<String> wipfileNamesSet = new HashSet<String>();
		Set<String> wipFilePnSet = new HashSet<String>();
		Set<String> wipJobStartTimeSet = new HashSet<String>();
		Set<String> wipCtLobSet = new HashSet<String>();

		// get file names from file path
		for (ViewModel eachViewBean : viewRecsList) {
			if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
				String file_path = eachViewBean.getFile_path();
				file_path = file_path == null ? ""
						: file_path.trim().length() == 0 ? ""
								: file_path.trim().substring(file_path.trim().lastIndexOf("/") + 1);
				wipfileNamesSet.add(file_path);
				// File Name from CT
				if (StringUtils.containsIgnoreCase(file_path, "CT00")) {
					metaDataMap.put("Checklist Name", file_path);
				}

				// Distinct File PN
				String file_pn = eachViewBean.getFile_pn();
				file_pn = file_pn == null ? "" : file_pn.trim().length() == 0 ? "" : file_pn;
				wipFilePnSet.add(file_pn);

				// Distinct job Start Time
				String job_start_date = "";
				Timestamp job_start_ts = eachViewBean.getJob_start_time();
				if (job_start_ts != null) {
					DateTime joda_start_datetime = new DateTime(job_start_ts.getTime());
					job_start_date = joda_start_datetime.getDayOfMonth() + "-" + joda_start_datetime.getMonthOfYear()
							+ "-" + joda_start_datetime.getYear();
				}
				wipJobStartTimeSet.add(job_start_date);

				// CT Lobs from extracts
				String lob = eachViewBean.getLob();
				String file_type = eachViewBean.getFile_type();
				file_type = file_type == null ? "" : file_type.trim();
				if (lob != null && lob.trim().length() > 0 && !lob.equalsIgnoreCase("NO LOB")
						&& !lob.equalsIgnoreCase("NO RECORDS") && file_type.startsWith("CT")) {
					wipCtLobSet.add(lob);
				}

			}

		}

		List<String> filesList = new ArrayList<String>(wipfileNamesSet);
		metaDataMap.put("File Name", String.join("\n", filesList));

		// Ideally the Pn list should have only one element as there cannot be two
		// policy numbers. Even if it is more than 1 for some reason, use element at 0
		// which will always be there

		List<String> filesPnList = new ArrayList<String>(wipFilePnSet);
		metaDataMap.put("Policy No", filesPnList.get(0));// Take only the first element

		// Distinct Policy Load date, Should be single but to make it failsame take the
		// first element
		List<String> policyLoadDate = new ArrayList<String>(wipJobStartTimeSet);
		metaDataMap.put("Policy Loaded Date", policyLoadDate.get(0));

		// Distinct CT Lobs from extracts
		List<String> applicableCTLobs = new ArrayList<String>(wipCtLobSet);
		metaDataMap.put("Extracts CT LOB(s)", String.join(",", applicableCTLobs));

		// GROUP LOB Addition
		// Unique List of Extract LOBS
		ArrayList<String> applicableLobsPerConfig = new ArrayList<String>();
		for (String eachLob : getExtractLobsInPack(packId)) {
			// If some unknown LOBs are wrongly entered in DB during extraction
			if (ConfigLoader.lobDictProps.getProperty(eachLob) != null) {
				applicableLobsPerConfig.add(ConfigLoader.lobDictProps.getProperty(eachLob));
			}
		}
		metaDataMap.put("Extracts LOB(s)", String.join(",\n", applicableLobsPerConfig));

		metaDataMap.put("Policy LOB", getPackLob(packId));// File LOB

		List<String> globalParamsQsKeys = new ArrayList<String>(Arrays.asList("UC1", "PP1", "NI1"));
		LinkedHashMap<String, String> globalParamsMap = getGlobalParamsMapForPack(packId, globalParamsQsKeys);

		metaDataMap.put("Ins Carrier Name", globalParamsMap.get("UC1"));
		metaDataMap.put("Policy Term", globalParamsMap.get("PP1"));
		metaDataMap.put("Insured Name", globalParamsMap.get("NI1"));
		return metaDataMap;
	}

	/**
	 * Method to calculate global values (UC, PP and NI)
	 * 
	 * @param packId Pack ID
	 * @param qsKey  Question Key
	 * @return Finalized parameter String
	 */
	/*
	 * Change as per Leela's Mail "Update on NI scenario - Summary" dated Monday, 5
	 * November 2018 at 6:39 PM
	 */
	public LinkedHashMap<String, String> getGlobalParamsMapForPack(String packId, List<String> qsKeys) {

		LinkedHashMap<String, String> globalParamsMap = new LinkedHashMap<String, String>();

		/*
		 * Check availability in CT first. If available move to the next question. If
		 * not move to the PT. If available move to the next question. If not in either
		 * CT or PT mark as No records
		 */
		for (ViewModel eachViewBean : viewRecsList) {
			if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
				for (String eachQsKey : qsKeys) {
					if (eachViewBean.getQs_key().equalsIgnoreCase(eachQsKey)) {
						globalParamsMap.put(eachQsKey, "");
						String file_type = eachViewBean.getFile_type().trim();
						if (file_type.equalsIgnoreCase("CT00")) {
							if (eachViewBean.getQs_value().trim().length() > 0) {
								globalParamsMap.put(eachQsKey, eachViewBean.getQs_value().trim());
								continue;
							}
						} else if (file_type.equalsIgnoreCase("PT00")) {
							if (eachViewBean.getQs_value().trim().length() > 0) {
								globalParamsMap.put(eachQsKey, eachViewBean.getQs_value().trim());
								continue;
							} else {
								globalParamsMap.put(eachQsKey, EpodUtil.NO_RECORDS);
							}
						} else {
							globalParamsMap.put(eachQsKey, EpodUtil.NO_RECORDS);
						}
					}
				}
			}
		}
		return globalParamsMap;
	}

	/**
	 * Method to determine if a pack can be processed based availability of Current
	 * Term file and the status of files within the pack
	 * 
	 * @param packId Pack ID
	 * @return true is pack can be processed
	 */
	private boolean checkPackProcessable(String packId) {

		/*
		 * Steps: Check if file details exist in meta data view. If no files or one file
		 * cannot process. Need at least two files with status as completed and one of
		 * them should be CT00
		 */
		// No files in filemetadata so return false
		if (metaViewRecsList.size() == 0) {
			logger.debug("File IDs not available in file_metadata and possibly no extracts for pack id:" + packId);
			return false;
		} else if (metaViewRecsList.size() == 1) {// Need at least two files to compare
			logger.debug("Only one File ID and possibly extracs for only one file is available for pack id:" + packId);
			return false;
		} else {
			// Get unique list of file types
			Set<String> wipFileTypesSet = new HashSet<String>();

			ArrayList<Triplet<String, String, String>> filesInPack = new ArrayList<Triplet<String, String, String>>();
			for (MetaViewModel eachMetaViewBean : metaViewRecsList) {
				if (eachMetaViewBean.getPack_id().equalsIgnoreCase(packId)) {
					// Check status completed before considering the file as candidate
					String status_type = eachMetaViewBean.getStatus_type().trim();
					if (status_type.equalsIgnoreCase("COMPLETED")) {
						String file_type = eachMetaViewBean.getFile_type().trim();
						String file_id = eachMetaViewBean.getFile_id().trim();
						filesInPack.add(Triplet.with(file_id, file_type, status_type));
						wipFileTypesSet.add(eachMetaViewBean.getFile_type());
					}
				}
			}

			// If CT file is not there, cannot process
			if (!wipFileTypesSet.contains("CT00")) {
				logger.debug("CT not available in file metadata and possible CT extractions not available for pack id:"
						+ packId);
				return false;
			}
			// Now that CT is available and extracted, find if Prior term side is present .
			for (Triplet<String, String, String> eachFile : filesInPack) {

				if (StringUtils.startsWithIgnoreCase(eachFile.getValue1(), "PR")
						&& StringUtils.equalsIgnoreCase(eachFile.getValue2(), "COMPLETED")) {
					return true;
				} else if (StringUtils.startsWithIgnoreCase(eachFile.getValue1(), "PT")
						&& StringUtils.equalsIgnoreCase(eachFile.getValue2(), "COMPLETED")) {
					return true;
				} else if (StringUtils.startsWithIgnoreCase(eachFile.getValue1(), "PE")
						&& StringUtils.equalsIgnoreCase(eachFile.getValue2(), "COMPLETED")) {
					return true;
				}
			}
		}

		// If can be processed true is returned in the for block above
		logger.debug("Except CT no other file extraction avaialable for:" + packId);
		return false;
	}

	/**
	 * Method to return all the Pack Ids for a Job ID
	 * 
	 * @return Map of Pack Ids
	 */
	public HashMap<String, ArrayList<String>> getPackIds() {
		// SYG

		logger.info("Evaluating pack(s) ....");

		HashMap<String, ArrayList<String>> packsSegregationMap = new HashMap<String, ArrayList<String>>();

		Set<String> wipPackIDsIncluded = new HashSet<String>();
		Set<String> wipPackIdsExcluded = new HashSet<String>();

		for (MetaViewModel eachMetaViewBean : metaViewRecsList) {
			String pack_id = eachMetaViewBean.getPack_id().trim();
			if (checkPackProcessable(pack_id)) {
				wipPackIDsIncluded.add(pack_id);
			} else {
				wipPackIdsExcluded.add(pack_id);
			}
		}

		ArrayList<String> packIDsIncluded = new ArrayList<String>(wipPackIDsIncluded);
		ArrayList<String> packIdsExcluded = new ArrayList<String>(wipPackIdsExcluded);

		packsSegregationMap.put("packIDsIncluded", packIDsIncluded);
		packsSegregationMap.put("packIdsExcluded", packIdsExcluded);

		int totalPacksAvailable = packIDsIncluded.size() + packIdsExcluded.size();
		logger.info("Total Packs available:" + totalPacksAvailable);

		if (packIDsIncluded.size() > 0) {
			logger.info("Count of Packs considered for comparison:" + packIDsIncluded.size());
			logger.debug("Pack IDs considered for comparison are:" + String.join(",", packIDsIncluded));
		}

		if (packIdsExcluded.size() > 0) {
			logger.info("Count of Packs excluded from comparison:" + packIdsExcluded.size());
			logger.debug("Pack IDs not considered for comparison are:");
		}

		return packsSegregationMap;
	}

	/*
	 * CHANGE AFTER GROUP LOB Instead of String returning ArrayList
	 */

	/**
	 * Method to return LOB for a pack
	 * 
	 * @param packId Pack ID System generated
	 * @return abbreviated LOB String
	 */
	public String getPackLob(String packId) {
		String packLob = "";
		// Take the first record as this will be repeated for all records
		for (MetaViewModel metaView : metaViewRecsList) {
			if (metaView.getPack_id().equalsIgnoreCase(packId)) {
				packLob = metaView.getFile_lob();
			}
		}
		return packLob;
//		return metaViewRecsList.get(0).getFile_lob();
	}

	/**
	 * Get all lobs from extracts for the pack.
	 * 
	 * @param packId ID of pack
	 * @return List of questions as found in extracts
	 */
	public ArrayList<String> getExtractLobsInPack(String packId) {
		String lobStr = "";
		Set<String> wipLobStrSet = new HashSet<String>();
		for (ViewModel eachViewBean : viewRecsList) {
			if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
				lobStr = eachViewBean.getLob();
				lobStr = lobStr == null ? ""
						: lobStr.equalsIgnoreCase(EpodUtil.NO_RECORDS) ? "" : lobStr.trim().toLowerCase();

				// Add to the list only if lob is of some length and not null based on above
				if (lobStr.length() > 0)
					wipLobStrSet.add(lobStr);
			}
		}
		logger.debug("Extracts LOB(s) in Pack:" + String.join(",", wipLobStrSet));
		return new ArrayList<String>(wipLobStrSet);
	}

	/**
	 * Gets a list of questions pertaining to a pack ID and corresponding to a LOB
	 * from file name
	 * 
	 * @param packId Pack ID
	 * @return List of question keys
	 * @throws ExtractHelperException Extract Helper Exception
	 */
	public ArrayList<String> getQsKeysForLOBs(String packId) throws ExtractHelperException {

		Set<String> wipQsKeysSet = new HashSet<String>();
		Set<String> wipNotApplicableQsKeysSet = new HashSet<String>();
		ArrayList<String> extractLobs = getExtractLobsInPack(packId);
		ArrayList<String> extractLobsNotConfiguredList = new ArrayList<String>();
		ArrayList<String> extractLobsApplicableList = new ArrayList<String>();

		for (String eachExtractLob : extractLobs) {
			if (ConfigLoader.lobDictProps.getProperty(eachExtractLob) == null) {
				extractLobsNotConfiguredList.add(eachExtractLob);
			} else {
				extractLobsApplicableList.add(eachExtractLob);

				for (ViewModel eachViewBean : viewRecsList) {
					if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
						String qs_key = eachViewBean.getQs_key();
						qs_key = qs_key == null ? "" : qs_key.trim();
						if (qs_key.length() > 0 && ExtractHelper.isInCurrentLob(qs_key, eachExtractLob.trim())) {
							wipQsKeysSet.add(qs_key);
						} else {
							wipNotApplicableQsKeysSet.add(qs_key);
						}
					}
				}
			}
		}

		if (extractLobsNotConfiguredList.size() > 0) {
			logger.info("Pack ID: " + packId + ", extract LOB(s) Not configured: "
					+ String.join(", ", extractLobsNotConfiguredList));
		}

		if (extractLobsApplicableList.size() > 0) {
			logger.info("Pack ID: " + packId + ", extract LOB(s) applicable: "
					+ String.join(", ", extractLobsApplicableList));
		} else {
			logger.info("Pack ID: " + packId + ", No applicable extract LOB(s) found ");
		}

		if (wipNotApplicableQsKeysSet.size() > 0) {
			logger.debug("Qs list from DB not applicable to LOB(s):" + String.join(", ", extractLobsApplicableList)
					+ " are: " + String.join(", ", wipNotApplicableQsKeysSet));
		}

		logger.debug("Qs list from DB for LOB(s):" + String.join(", ", extractLobsApplicableList) + " are: "
				+ String.join(", ", wipQsKeysSet));

		return new ArrayList<String>(wipQsKeysSet);
	}

	/**
	 * Extracts for all the questions is retrieved from the DB for a pack id at one
	 * go. The extracts will have question keys that are not applicable to the LOBs.
	 * So the question keys passed by the calling method is used to filter only
	 * those records that are relevant/valid. For a relevant record, a map is
	 * created to store all key, value pairs from the result set (DB record). The
	 * map is then added to a relevant records List which holds all maps for
	 * relevant records with details like file path file_type qs_key qs_value
	 * page_num line num. To note that question keys passed to the function might
	 * have UC1, PP1, AI1. But the DB records might have UC1, PP1, CA1. In this case
	 * the relevant records are only for UC1 and PP1. Hence a key set is used to
	 * store the keys that are applicable to the relevant records. In this case UC1,
	 * PP1. A Set is used because we have multi-records like CA1, CL1 etc and
	 * duplicates have to removed. After this the question set keys are used to
	 * iterate over the List of relevant records HashMaps and multimaps are created
	 * for each question key.Each multimap is then added to a list of multimaps.
	 * This list will now hold all the relevant extraction records for the pack.
	 * This List with multimaps is returned.
	 * 
	 * @param packId    Pack ID
	 * @param old_qsKey Question Key
	 * @return List of maps of extracts from DB
	 * @throws DBException    DB Exception
	 * @throws ParseException
	 * 
	 */
	public ArrayList<Multimap<String, LinkedHashMap<String, String>>> getDBExtracts(String packId,
			ArrayList<String> qsKeys) throws DBException {

		ArrayList<Multimap<String, LinkedHashMap<String, String>>> allRecordsInPack = new ArrayList<Multimap<String, LinkedHashMap<String, String>>>();

		/*
		 * A list that holds the final valid extraction records pertaining to the common
		 * set of question keys that was passed and those that are available in the DB.
		 * This is returned to the calling function.
		 */
		ArrayList<LinkedHashMap<String, String>> allRelevantExtRecsList = new ArrayList<LinkedHashMap<String, String>>();

		/*
		 * Calling function passes a set of question keys pertaining to a LOB, example
		 * UC1, PP1, AI1 but DB might have records for UC1, PP1, CA1. This set stores
		 * all the qustion keys that are common to what is passed and what is avaiable
		 * in the DB. A set is used to remove duplicates since there can be multiple
		 * records for questions example CA1, CL1. This set will be used while creating
		 * the multimaps later.
		 */
		Set<String> finalQsKeys = new HashSet<String>();

		try {

			for (ViewModel eachViewBean : viewRecsList) {
				if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
					String qs_key = eachViewBean.getQs_key();
					qs_key = qs_key == null ? "" : qs_key.trim().length() == 0 ? "" : qs_key.trim();

					// Consider the record only if the qs_key exists in the list of questions passed
					// else discard.
					if (qsKeys.contains(qs_key)) {

						// Create the map only if we cross the qs_key exists loop
						LinkedHashMap<String, String> eachRecordMap = new LinkedHashMap<String, String>();

						String file_path = eachViewBean.getFile_path();
						file_path = file_path == null ? ""
								: file_path.trim().length() == 0 ? ""
										: file_path.trim().substring(file_path.lastIndexOf("/") + 1);

						String file_type = eachViewBean.getFile_type();
						file_type = file_type == null ? "" : file_type.trim().length() == 0 ? "" : file_type.trim();

						String qs_desc = eachViewBean.getQs_desc();
						qs_desc = qs_desc == null ? "" : qs_desc.trim().length() == 0 ? "" : qs_desc.trim();

						String qs_desc_std = eachViewBean.getQs_desc_std();// qs_desc_std
						qs_desc_std = qs_desc_std == null ? ""
								: qs_desc_std.trim().length() == 0 ? "" : qs_desc_std.trim();

						String qs_value = eachViewBean.getQs_value();
						qs_value = qs_value == null ? "" : qs_value.trim().length() == 0 ? "" : qs_value.trim();

						String qs_value_std = eachViewBean.getQs_value_std();
						qs_value_std = qs_value_std == null ? ""
								: qs_value_std.trim().length() == 0 ? "" : qs_value_std.trim();

						String val_type = eachViewBean.getVal_type();
						val_type = val_type == null ? "" : val_type.trim().length() == 0 ? "" : val_type.trim();

						String page_num = String.valueOf(eachViewBean.getPage_num());
						page_num = page_num == null ? "" : page_num.trim().length() == 0 ? "" : page_num.trim();

						String line_num = String.valueOf(eachViewBean.getLine_num());
						line_num = line_num == null ? "" : line_num.trim().length() == 0 ? "" : line_num.trim();

						String file_id = eachViewBean.getFile_id();
						file_id = file_id == null ? "" : file_id.trim().length() == 0 ? "" : file_id.trim();

						String extr_id = eachViewBean.getExtr_id();
						extr_id = extr_id == null ? "" : extr_id.trim().length() == 0 ? "" : extr_id.trim();

						// IMPORTANT (LOWER CASE)
						String extr_lob = eachViewBean.getLob();
						extr_lob = extr_lob == null ? ""
								: extr_lob.trim().length() == 0 ? "" : extr_lob.trim().toLowerCase();

						// Start Binning JSON
						String extr_json = eachViewBean.getExtr_json();
						extr_json = extr_json == null ? "" : extr_json.trim().length() == 0 ? "" : extr_json.trim();

						String binning_str = GsonUtils.getJsonPrimitiveFromJson(extr_json, "binning_json").trim();
						binning_str = JsonUtil.isEmptyJSON(binning_str) ? "" : binning_str;

						// System.err.println("binning_str"+binning_str);

						// End Binning JSON

						eachRecordMap.put("file_path", file_path);
						eachRecordMap.put("file_type", file_type);
						eachRecordMap.put("qs_key", qs_key);
						eachRecordMap.put("qs_desc", qs_desc);
						eachRecordMap.put("qs_desc_std", qs_desc_std);
						eachRecordMap.put("qs_value", qs_value);
						eachRecordMap.put("qs_value_std", qs_value_std);
						eachRecordMap.put("val_type", val_type);
						eachRecordMap.put("page_num", page_num);
						eachRecordMap.put("line_num", line_num);
						eachRecordMap.put("file_id", file_id);
						eachRecordMap.put("extr_id", extr_id);
						eachRecordMap.put("extr_lob", extr_lob);
						eachRecordMap.put("binning_str", binning_str);

						// Add each map to the ArrayList of relevant records from DB
						allRelevantExtRecsList.add(eachRecordMap);
						// Also add the question key to the final qs keys set
						finalQsKeys.add(qs_key);
					}
				}
			}
		} catch (ParseException e) {
			logger.error("ERROR: ParseException in fetching Binning Qs values.");
			throw new DBException("In getDBExtracts", e);
		}

		/*
		 * Take each question from the final question set and iterate through the List
		 * of relevant records (stored as maps). Create a multimap with the final
		 * question key as the key of the multimap and values having all relevant
		 * records from the list of relevant records for that question. On completion of
		 * the iteration add the multimap to the List of all Records in Pack which needs
		 * to be returned.
		 */

		for (String eachFinalQsKey : finalQsKeys) {
			// A new multimap for each question
			Multimap<String, LinkedHashMap<String, String>> multimap = HashMultimap.create();
			for (LinkedHashMap<String, String> eachValidRec : allRelevantExtRecsList) {
				if (eachFinalQsKey.equalsIgnoreCase(eachValidRec.get("qs_key"))) {
					multimap.put(eachFinalQsKey, eachValidRec);
				}
			}
			allRecordsInPack.add(multimap);
		}

		return allRecordsInPack;
	}

	/**
	 * Method to populate data for Summary Tab
	 * 
	 * @return Map of summary tab data
	 */
	/*
	 * Date 18 Apr 2019 Summary Table 2 change additional columns added total pages,
	 * ocr pages, etc
	 */
	public HashMap<String, Object> getFilesProcessedData() {

		/*
		 * Get details from file_metadata view and then from extracts view. Very often
		 * than not, file extracts are available but the same file is not available in
		 * file metadata. So both scenarios have to be looked into to ensure that all
		 * file details are captured that is common files between extracts and file
		 * metadata and extracted files not available in file meta daata
		 */

		HashMap<String, Object> summaryTableMap = new HashMap<String, Object>();

		// List of maps having file details from both file_metadata and extracts
		ArrayList<HashMap<String, Object>> filesDataMapList = new ArrayList<HashMap<String, Object>>();

		/*
		 * Definitions:
		 * 
		 * Total files loaded: count of files in file_metadata view + count of files in
		 * extracts view (but not in file_metadata view). files table has left join with
		 * files_metadata. So practically all files should be there. Files from
		 * extracts_view is just to take care of corner cases where for some reason the
		 * file_metadata view missed some files during creation.
		 * 
		 * Total files processed: All files available in file_metadata and "processed"
		 * are those that have file_status as COMPLETED.
		 * 
		 * Total files unprocessed: Total files loaded-Total files processed
		 * 
		 */
		int totalFilesLoaded = 0;
		int totalFilesProcessed = 0;
		int totalFilesUnProcessed = 0;

		/*
		 * First consider file_metadata side. Then take up extracts file details.
		 * Considers all calculations related to file_metadata, i.e. related to file_ids
		 * from meta view
		 */

		Set<String> wipMetaViewFileIdSet = new HashSet<String>();
		for (MetaViewModel eachMetaViewBean : metaViewRecsList) {
			// Map to hold all the files data from files_metadata
			HashMap<String, Object> filemetaTableFileDataMap = new HashMap<String, Object>();

			String file_id = eachMetaViewBean.getFile_id();
			file_id = file_id == null ? "" : file_id.trim().length() == 0 ? "" : file_id.trim();
			if (file_id.trim().length() > 0)
				wipMetaViewFileIdSet.add(file_id);
			// file_id,file_path,status_type,doc_type,pack_key,total_pages,native_pages,focr_pages,pocr_pages,blank_pages,image_count

			String fileStatus = eachMetaViewBean.getFile_status();
			fileStatus = fileStatus == null ? "" : fileStatus.trim().length() == 0 ? "" : fileStatus.trim();

			String status_type = eachMetaViewBean.getStatus_type();
			status_type = status_type == null ? "" : status_type.trim().length() == 0 ? "" : status_type.trim();

			String reason_StatusDesc = eachMetaViewBean.getStatus_desc();
			reason_StatusDesc = reason_StatusDesc == null ? ""
					: reason_StatusDesc.trim().length() == 0 ? "" : reason_StatusDesc.trim();

//			String reasonStatus = ConfigLoader.statusMsgsProps.getProperty(fileStatus.toUpperCase());
//			reasonStatus = reasonStatus == null ? ConfigLoader.statusMsgsProps.getProperty("NONE")
//					: reasonStatus.length() == 0 ? ConfigLoader.statusMsgsProps.getProperty("NONE") : reasonStatus;

			// status_type shows "Completed. File_status shows PRSNT_COMPLETED
			if (status_type.equalsIgnoreCase("COMPLETED")) {
				filemetaTableFileDataMap.put("docStatus", "Passed");
				filemetaTableFileDataMap.put("reason", "");// no reason for already extracted files
				// Processed are those that are in file meta_data and file_status completed
				totalFilesProcessed++; // Completed to processed
			} else {
				filemetaTableFileDataMap.put("docStatus", "Failed");
				filemetaTableFileDataMap.put("reason", reason_StatusDesc);
			}

			String filePath = eachMetaViewBean.getFile_path().trim();
			filePath = filePath.substring(filePath.lastIndexOf("/") + 1);

			String docType = eachMetaViewBean.getDoc_type();
			docType = docType == null ? "" : docType.trim().length() == 0 ? "" : docType.trim();

			String packKey = eachMetaViewBean.getPack_key().trim();

			int totalPages = eachMetaViewBean.getTotal_pages();
			int nativePages = eachMetaViewBean.getNative_pages();
			int scannedPages = eachMetaViewBean.getFocr_pages() + eachMetaViewBean.getPocr_pages();
			int blankPages = eachMetaViewBean.getBlank_pages();
			int imageCount = eachMetaViewBean.getImage_count();

			filemetaTableFileDataMap.put("portalID", packKey);
			filemetaTableFileDataMap.put("docName", filePath);
			filemetaTableFileDataMap.put("docType", docType);
			filemetaTableFileDataMap.put("totalPages", totalPages);
			filemetaTableFileDataMap.put("nativePages", nativePages);
			filemetaTableFileDataMap.put("scannedPages", scannedPages);
			filemetaTableFileDataMap.put("blankPages", blankPages);
			filemetaTableFileDataMap.put("imageCount", imageCount);

			// Data about files present in filesmetadaata table stored in list
			filesDataMapList.add(filemetaTableFileDataMap);
		}

		/*
		 * FIRST Step: Adding count of all files that are there in the file_metadata
		 */
		totalFilesLoaded = wipMetaViewFileIdSet.size();

		// Total files processed using File ids from extracts view
		Set<String> wipViewFileIdSet = new HashSet<String>();
		for (ViewModel eachViewBean : viewRecsList) {
			String file_id = eachViewBean.getFile_id();
			file_id = file_id == null ? "" : file_id.trim().length() == 0 ? "" : file_id.trim();
			if (file_id.trim().length() > 0)
				wipViewFileIdSet.add(file_id);
		}

		/*
		 * The files in the file metadata has been account for. Only the additional
		 * files in extracts are required. So we remove those that are common. LHS(set
		 * from which to be removed), in this case extracts view is LHS.
		 */
		wipViewFileIdSet.removeAll(wipMetaViewFileIdSet);

		/*
		 * SECOND Step: Adding count of all files that are there in the extracts but not
		 * in the file_metadata
		 */
		totalFilesLoaded = totalFilesLoaded + wipViewFileIdSet.size();

		// wipViewFileIdSet now has only files that are notavailable in file_metadata
		logger.debug("Count of file Ids not available in file_metadata:" + wipViewFileIdSet.size());
		logger.debug("File Ids not available in file_metadata:" + String.join(", ", wipViewFileIdSet));

		// PENDING ALL FILE DETAILS FROM EXTRACTS SIDE to be added to filesMetaData

		for (ViewModel eachViewBean : viewRecsList) {
			// Map to hold all the files data from extracts
			HashMap<String, Object> extractsTableFileDataMap = new HashMap<String, Object>();

			String file_id = eachViewBean.getFile_id();
			file_id = file_id == null ? "" : file_id.trim().length() == 0 ? "" : file_id.trim();
			if (wipViewFileIdSet.contains(file_id)) {
				String packKey = eachViewBean.getPack_key().trim();
				String filePath = eachViewBean.getFile_path().trim();
				filePath = filePath.substring(filePath.lastIndexOf("/") + 1);
				extractsTableFileDataMap.put("portalID", packKey);
				extractsTableFileDataMap.put("docName", filePath);
				extractsTableFileDataMap.put("docType", "");
				extractsTableFileDataMap.put("docStatus", "Failed");// All failed unprocessed files only
				extractsTableFileDataMap.put("reason", ConfigLoader.statusMsgsProps.getProperty("DOC_FORMAT_ERROR"));// Standard
				extractsTableFileDataMap.put("totalPages", "");
				extractsTableFileDataMap.put("nativePages", "");
				extractsTableFileDataMap.put("scannedPages", "");
				extractsTableFileDataMap.put("blankPages", "");
				extractsTableFileDataMap.put("imageCount", "");

				// Data about files present in extracts table stored in list
				filesDataMapList.add(extractsTableFileDataMap);
			}
		}

		totalFilesUnProcessed = totalFilesLoaded - totalFilesProcessed;

		summaryTableMap.put("totalFilesLoaded", String.valueOf(totalFilesLoaded));
		summaryTableMap.put("totalFilesProcessed", String.valueOf(totalFilesProcessed));
		summaryTableMap.put("totalFilesUnProcessed", String.valueOf(totalFilesUnProcessed));

		/*
		 * Change Leela Mail dated: Summary Tab change Monday, 29 October 2018 at 12:28
		 * PM
		 */
		summaryTableMap.put("filesList", filesDataMapList);

		// Added to name the summary file
		summaryTableMap.put("summaryFileName", getRootDir(getJobID()));

		return summaryTableMap;

	}

	// CCR 8 all extracts successful and failed
	/**
	 * Method to get extracts of files in packs
	 * 
	 * @param packIdsList List of pack Ids
	 * @return Map of extracts
	 */
	public LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> getExtractsForOutput(
			ArrayList<String> packIdsList) {

		LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> allExtractRecords = new LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>>();

		for (String packId : packIdsList) {

			ArrayList<LinkedHashMap<String, String>> allRecordsInPack = new ArrayList<LinkedHashMap<String, String>>();

			for (ViewModel eachViewBean : viewRecsList) {
				if (eachViewBean.getPack_id().equalsIgnoreCase(packId)) {
					LinkedHashMap<String, String> eachRecordMap = new LinkedHashMap<String, String>();

					String file_path = eachViewBean.getFile_path();
					file_path = file_path == null ? "" : file_path.trim().length() == 0 ? "" : file_path.trim();
					String qs_key = eachViewBean.getQs_key();
					qs_key = qs_key == null ? "" : qs_key.trim().length() == 0 ? "" : qs_key.trim();
					String qs_desc = eachViewBean.getQs_desc();
					qs_desc = qs_desc == null ? "" : qs_desc.trim().length() == 0 ? "" : qs_desc.trim();
					String qs_desc_std = eachViewBean.getQs_desc_std();
					qs_desc_std = qs_desc_std == null ? "" : qs_desc_std.trim().length() == 0 ? "" : qs_desc_std.trim();
					String qs_value = eachViewBean.getQs_value();
					qs_value = qs_value == null ? "" : qs_value.trim().length() == 0 ? "" : qs_value.trim();
					String page_num = String.valueOf(eachViewBean.getPage_num());
					String extr_lob = eachViewBean.getLob();// Always lower case
					extr_lob = extr_lob == null ? ""
							: extr_lob.trim().length() == 0 ? "" : extr_lob.trim().toLowerCase();

					eachRecordMap.put("file_path", file_path);
					eachRecordMap.put("qs_key", qs_key);
					eachRecordMap.put("qs_desc", qs_desc);
					eachRecordMap.put("qs_desc_std", qs_desc_std);
					eachRecordMap.put("qs_value", qs_value);
					eachRecordMap.put("page_num", page_num);
					eachRecordMap.put("extr_lob", extr_lob);
					allRecordsInPack.add(eachRecordMap);
				}
			}
			allExtractRecords.put(packId, allRecordsInPack);
		}
		return allExtractRecords;
	}

	@SuppressWarnings("unchecked")
	public void uploadTraceLogJSON(Timestamp loggingTS,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discrepancyTabDataMap, String s3Path,
			String currentReg) throws DBException {

		JSONObject traceObj = new JSONObject();
		traceObj.put("UUID", Generators.timeBasedGenerator().generate().toString());
		traceObj.put("job_id", getJobID());
		traceObj.put("log_timestamp", loggingTS.toString());
		JSONArray extractsJSON = createExtractJSON();
		traceObj.put("all_extracts", extractsJSON);
		JSONArray packsWithDiscrepanciesArray = createDiscrepancyArray(discrepancyTabDataMap);
		traceObj.put("pack_discrepancies", packsWithDiscrepanciesArray);

		String traceFileName = getJobID() + "_Trace.json";

		try {
			S3Handler.writeFileToS3(s3Path, currentReg, "application/json", traceObj.toString(), traceFileName);
		} catch (IOException e) {
			logger.error("ERROR: Writing " + traceFileName + " to " + s3Path);
			throw new DBException("In uploadTraceLogJSON", e);
		}
	}

	/**
	 * Creates JSON and stores in Trace table
	 * 
	 * @param loggingTS             TimeStamp
	 * @param discrepancyTabDataMap
	 */
	public void insertTraceLog(Timestamp loggingTS,
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discrepancyTabDataMap) {

		final int batchSize = 1000;
		int count = 0;
		try {
			conn.setAutoCommit(false);

//				System.err.println("Start extractsJSON: "+new Timestamp(System.currentTimeMillis()));
			JSONArray extractsJSON = createExtractJSON();
//				System.err.println("End extractsJSON Start discrepancyJSON: "+new Timestamp(System.currentTimeMillis()));

			JSONObject discrepancyJSON = createDiscrepancyJSON(discrepancyTabDataMap);
//				System.err.println("End discrepancyJSON: "+new Timestamp(System.currentTimeMillis()));

			traceInsertPS.setObject(1, Generators.timeBasedGenerator().generate(), java.sql.Types.OTHER);
			traceInsertPS.setString(2, jobID);
			traceInsertPS.setTimestamp(3, loggingTS);
			traceInsertPS.setObject(4, extractsJSON.toJSONString());
			traceInsertPS.setObject(5, discrepancyJSON.toJSONString());

//				traceInsertPS.executeUpdate();

			traceInsertPS.addBatch();

			if (++count % batchSize == 0) {
				traceInsertPS.executeBatch();
				conn.commit();
			}
			traceInsertPS.executeBatch();// Remaining records
			conn.commit();
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in updating Trace Log Table");
			logger.error(e.getMessage());
		}
	}

	/**
	 * Method to maintain a repository of views which can be used for clean up later
	 * (dropping old views)
	 * 
	 * @param allViews List of All Views created
	 * @throws DBException DB Exception
	 */
	public void insertViewLog(ArrayList<String> allViews) throws DBException {
		Timestamp viewTs = new Timestamp(System.currentTimeMillis());
		try {
			conn.setAutoCommit(false);

			for (String eachView : allViews) {
				String sqlQuery = "SELECT * from view_log where job_id='" + getJobID() + "' and view_name='" + eachView
						+ "'";
				Statement stmt = conn.createStatement();
				ResultSet resultSet = stmt.executeQuery(sqlQuery);
				if (!resultSet.next()) {
					viewLogInsertPS.setString(1, getJobID());
					viewLogInsertPS.setString(2, eachView);
					viewLogInsertPS.setTimestamp(3, viewTs);
					viewLogInsertPS.addBatch();
				} else {
					viewLogUpdatePS.setTimestamp(1, viewTs);
					viewLogUpdatePS.setString(2, getJobID());
					viewLogUpdatePS.setString(3, eachView);
					viewLogUpdatePS.addBatch();
				}
				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}
				if (stmt != null) {
					stmt.close();
					stmt = null;
				}
			}
			viewLogInsertPS.executeBatch();
			viewLogUpdatePS.executeBatch();
			conn.commit();
		} catch (SQLException e) {
			logger.error("ERROR: SQLException in inserting into View Log Table");
			throw new DBException("In insertViewLog()", e);
		}
	}

	/**
	 * Method to drop all old/existing views that were possibly not deleted in
	 * earlier runs and are more than 24 hours old
	 */
	private void dropOldViews() {

		ArrayList<String> oldViews = new ArrayList<String>();
		try {
			DateTimeFormatter format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");

			String currentTsStr = new Timestamp(System.currentTimeMillis()).toString();
			DateTime currentDateTime = format.parseDateTime(currentTsStr);

			String sqlQuery = "SELECT * FROM view_log";
			Statement stmt = conn.createStatement();
			ResultSet resultSet = stmt.executeQuery(sqlQuery);
			while (resultSet.next()) {
				String dbTsStr = resultSet.getTimestamp("view_ts").toString();
				DateTime dbDateTime = format.parseDateTime(dbTsStr);

				Period period = new Period(dbDateTime, currentDateTime);
				if (period.getHours() > 24) {
					oldViews.add(resultSet.getString("view_name"));
				}
			}
			if (resultSet != null) {
				resultSet.close();
				resultSet = null;
			}

			if (stmt != null) {
				stmt.close();
				stmt = null;
			}

			// Drop existing views and clear the view log table
			for (String eachView : oldViews) {
				dropView(eachView);
				// Code to delete from view log
				viewLogDeleteRecordPS.setString(1, eachView);
				viewLogDeleteRecordPS.executeUpdate();
				logger.debug("Dropped record from view log for view:" + eachView);
			}

		} catch (SQLException e) {
			logger.error("ERROR: SQLException in clearing old views");
		} catch (Exception e) {
			logger.error("ERROR: Exception in clearing old views");
		}

	}

	/**
	 * Required for generating individual files discrepancy and extracts, taken from
	 * meta_view records
	 * 
	 * @param packID pack ID
	 * @return File Name
	 */
	public String getOutputFileName(String packID) {
		String fileName = "";
		// Take the first record as this will be repeated for all records
		for (MetaViewModel metaView : metaViewRecsList) {
			if (metaView.getPack_id().equalsIgnoreCase(packID)) {
				fileName = metaView.getPack_key();
			}
		}
		return fileName;
	}

	/**
	 * Required for generating the third table with file names of excluded/non
	 * discrepancy packs
	 * 
	 * @param packIds List of Pack Ids
	 * @return List of files
	 */
	public ArrayList<String> getFileNamesExcludedPacks(ArrayList<String> packIds) {

		Set<String> wipExcludedFiles = new HashSet<String>();

		for (MetaViewModel eachMetaViewBean : metaViewRecsList) {
			if (packIds.contains(eachMetaViewBean.getPack_id())) {
				String file_path = eachMetaViewBean.getFile_path();
				file_path = file_path == null ? ""
						: file_path.trim().length() == 0 ? ""
								: file_path.trim().substring(file_path.trim().lastIndexOf("/") + 1);
				wipExcludedFiles.add(file_path);
			}
		}

		return new ArrayList<String>(wipExcludedFiles);
	}

	/**
	 * Required for generating the fourth table with wrong file names. DB based
	 * implementation. JSON based is dealt with in creating meta data
	 * 
	 * @param isJSONDriven
	 * @return
	 */
	public ArrayList<String> getWrongFileNamesinJob(boolean isJSONDriven) {
		if (!isJSONDriven) {
			String sqlQuery = null;
			try {
				Statement stmt = conn.createStatement();
				ResultSet resultSet = null;

				sqlQuery = "SELECT * FROM files_missed WHERE job_id='" + getJobID() + "'";
				resultSet = stmt.executeQuery(sqlQuery);
				while (resultSet.next()) {
					String file_path = resultSet.getString("file_path").trim();
					file_path = file_path == null ? ""
							: file_path.trim().length() == 0 ? ""
									: file_path.trim().substring(file_path.trim().lastIndexOf("/") + 1);
					filesWrongNamesList.add(file_path);
				}

				if (resultSet != null) {
					resultSet.close();
					resultSet = null;
				}

				if (stmt != null) {
					stmt.close();
					stmt = null;
				}

			} catch (SQLException e) {
				logger.error("ERROR: SQLException in getting Files for excluded packs");
			}
		}
		return filesWrongNamesList;
	}

	/**
	 * Method to create Create extracts JSON String
	 * 
	 * @return JSONObject representation of extracts
	 */
	@SuppressWarnings("unchecked")
	private JSONArray createExtractJSON() {
		JSONArray allExtractsArray = new JSONArray();
		for (JSONObject eachSubViewExtJSON : extrJsons) {
			allExtractsArray.add(eachSubViewExtJSON);
		}
		return allExtractsArray;
	}

	/**
	 * Method to create the Discrepancy Json. This has data for entire Job with
	 * multiple pack_ids.
	 * 
	 * @param discrepancyTabDataMap
	 * @return JSONObject representation of discrepancies
	 */
	@SuppressWarnings("unchecked")
	private JSONObject createDiscrepancyJSON(
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discrepancyTabDataMap) {
		/*
		 * discrepancyTabDataMap has data across all the packs for the job id. pack_id
		 * is the key. First get the List of discrepancies for a pack_id (use map key
		 * set, each key is pack_id).
		 * 
		 */

		JSONObject discrepancyJsonObj = new JSONObject();

		/*
		 * Pack Id is the root element that will have a list/array of discrepancies
		 * represented by key value pairs. Multiple pack_ids will hold discrepancies
		 * array related to individual pac_id
		 */
		// All pack_ids. Pack id is the root element
		Set<String> disPackIds = discrepancyTabDataMap.keySet();
		for (String eachPackId : disPackIds) {
			ArrayList<LinkedHashMap<String, String>> currentDiscrpMapsList = discrepancyTabDataMap.get(eachPackId);
			// List to hold the key-value pairs
			JSONArray list = new JSONArray();
			for (LinkedHashMap<String, String> eachDiscpMap : currentDiscrpMapsList) {
				list.add(eachDiscpMap);
			}
			// Add the array to return Json Object with pack_id as key
			discrepancyJsonObj.put(eachPackId, list);
		}

		return discrepancyJsonObj;
	}

	/**
	 * Method to create the Discrepancy Json. This has data for entire Job with
	 * multiple pack_ids.
	 * 
	 * @param discrepancyTabDataMap
	 * @return JSONObject representation of discrepancies
	 */
	@SuppressWarnings("unchecked")
	private JSONArray createDiscrepancyArray(
			LinkedHashMap<String, ArrayList<LinkedHashMap<String, String>>> discrepancyTabDataMap) {
		/*
		 * Create an array that holds 2 arrays, one for discrepancies and another for
		 * extracts
		 * 
		 */

		JSONArray packsArray = new JSONArray();

		Set<String> disPackIds = discrepancyTabDataMap.keySet();
		for (String eachPackId : disPackIds) {
			JSONObject packObj = new JSONObject();
			packObj.put("pack_id", eachPackId);
			ArrayList<LinkedHashMap<String, String>> currentDiscrpMapsList = discrepancyTabDataMap.get(eachPackId);
			// List to hold the key-value pairs
			JSONArray discrepanciesArray = new JSONArray();
			for (LinkedHashMap<String, String> eachDiscpMap : currentDiscrpMapsList) {
				discrepanciesArray.add(eachDiscpMap);
			}
			// Add the array to return Json Object with pack_id as key
			packObj.put("discrepancies", discrepanciesArray);
			packsArray.add(packObj);
		}
		return packsArray;
	}

	/**
	 * Generating JSON from view for use in createExtractsView()
	 * 
	 * @param currentExtractsViewName
	 * @return
	 */
	@SuppressWarnings({ "unchecked" })
	public JSONObject generateJson(String currentExtractsViewName) {

		JSONObject jsonReturnObj = new JSONObject();

		JSONObject viewObj = new JSONObject();
		JSONArray viewArray = new JSONArray();

		Set<String> packIdSet = new HashSet<String>();
		Set<String> fileIdSet = new HashSet<String>();
		Set<String> qsKeySet = new HashSet<String>();

		String sqlString = "select job_id,pack_id, file_id, job_status, input_source, output_dstn, qs_key, job_start_time, job_end_time from "
				+ currentExtractsViewName;
		Statement stmt;
		ResultSet resultSet;
		try {
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sqlString);
			while (resultSet.next()) {
				viewObj.put("job_status", resultSet.getString("job_status"));
				viewObj.put("input_source", resultSet.getString("input_source"));
				viewObj.put("output_dstn", resultSet.getString("output_dstn"));
				viewObj.put("job_start_time", resultSet.getString("job_start_time"));
				viewObj.put("job_end_time", resultSet.getString("job_end_time"));

				packIdSet.add(resultSet.getString("pack_id"));
				fileIdSet.add(resultSet.getString("file_id"));
				qsKeySet.add(resultSet.getString("qs_key"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			String sqlQuery = "Select * from " + currentExtractsViewName + " where pack_id=? and file_id=?";
			PreparedStatement jsonPS = conn.prepareStatement(sqlQuery);

//			JSONObject jobsObj = new JSONObject();

			JSONArray packsArray = new JSONArray();
			for (String eachPackId : packIdSet) {
				JSONObject currentPacksObj = new JSONObject();
				currentPacksObj.put("pack_id", eachPackId);

				JSONArray currentFilesArray = new JSONArray();
				for (String eachFileId : fileIdSet) {
					JSONObject currentFileObj = new JSONObject();
					currentFileObj.put("file_id", eachFileId);

					jsonPS.setString(1, eachPackId);
					jsonPS.setString(2, eachFileId);
					resultSet = jsonPS.executeQuery();

					JSONArray currentExtractsArray = new JSONArray();
					while (resultSet.next()) {
						String file_lob = resultSet.getString("file_lob") == null ? ""
								: resultSet.getString("file_lob").trim().length() == 0 ? ""
										: resultSet.getString("file_lob").trim();
						currentFileObj.put("file_lob", file_lob);
						currentFileObj.put("file_path", resultSet.getString("file_path"));
						currentFileObj.put("file_type", resultSet.getString("file_type"));
						currentFileObj.put("file_status", resultSet.getString("file_status"));
						currentFileObj.put("file_pn", resultSet.getString("file_pn"));

						String file_pp = resultSet.getString("file_pp");
						file_pp = file_pp == null ? "" : file_pp.trim();
						currentFileObj.put("file_pp", file_pp);

						currentFileObj.put("doc_type", resultSet.getString("doc_type"));
						currentFileObj.put("total_pages", resultSet.getInt("total_pages"));
						currentFileObj.put("word_count", resultSet.getInt("word_count"));
						currentFileObj.put("status_type", resultSet.getString("status_type"));
						currentFileObj.put("status_desc", resultSet.getString("status_desc"));
						currentFileObj.put("pack_key", currentFileObj.get("file_pn").toString() + '_'
								+ currentFileObj.get("file_lob").toString());

						JSONObject currentExtractObj = new JSONObject();
						currentExtractObj.put("extr_id", resultSet.getString("extr_id"));
						currentExtractObj.put("qs_key", resultSet.getString("qs_key"));
						currentExtractObj.put("qs_section", resultSet.getString("qs_section"));
						currentExtractObj.put("qs_sub_section", resultSet.getString("qs_sub_section"));
						currentExtractObj.put("qs_desc", resultSet.getString("qs_desc"));
						currentExtractObj.put("qs_desc_std", resultSet.getString("qs_desc_std"));
						currentExtractObj.put("qs_value", resultSet.getString("qs_value"));
						currentExtractObj.put("qs_value_std", resultSet.getString("qs_value_std"));
						currentExtractObj.put("val_type", resultSet.getString("val_type"));
						currentExtractObj.put("lob", resultSet.getString("lob"));
						currentExtractObj.put("child_lob", resultSet.getString("child_lob"));
						currentExtractObj.put("line_num", resultSet.getInt("line_num"));
						currentExtractObj.put("page_num", resultSet.getInt("page_num"));
						currentExtractObj.put("x0", resultSet.getDouble("x0"));
						currentExtractObj.put("x1", resultSet.getDouble("x1"));
						currentExtractObj.put("y0", resultSet.getDouble("y0"));
						currentExtractObj.put("y1", resultSet.getDouble("y1"));
						currentExtractObj.put("disp_y0", resultSet.getDouble("disp_y0"));
						currentExtractObj.put("disp_y1", resultSet.getDouble("disp_y1"));
						currentExtractObj.put("page_height", resultSet.getString("page_height"));
						currentExtractObj.put("page_width", resultSet.getString("page_width"));
						currentExtractObj.put("extr_json", resultSet.getString("extr_json"));

						currentExtractsArray.add(currentExtractObj);
						currentFileObj.put("extracts", currentExtractsArray);
					}

					currentFilesArray.add(currentFileObj);
					currentPacksObj.put("files", currentFilesArray);
				}
				packsArray.add(currentPacksObj);
			}

			viewObj.put("packs", packsArray);
			viewArray.add(viewObj);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		jsonReturnObj.put("view", viewArray);
		jsonReturnObj.put("meta", generateMetaViewJSON());

		logger.info("JSON STRING:" + jsonReturnObj);
//		System.err.println(jsonReturnObj);
		return jsonReturnObj;
	}

	/**
	 * Generating JSON from metaView for use in createExtractsView()
	 * 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public JSONArray generateMetaViewJSON() {

		Set<String> packIdSet = new HashSet<String>();
		Set<String> fileIdSet = new HashSet<String>();
		JSONArray metaArray = new JSONArray();
		JSONObject packMetaDataObj = new JSONObject();

		String sqlString = "select pack_id,pack_key,file_id,file_path,file_lob,file_type,file_pn,file_pp,status_desc, status_type,doc_type,total_pages,word_count from "
				+ metaViewName;
		Statement stmt;
		ResultSet resultSet;
		try {
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sqlString);
			while (resultSet.next()) {
				packMetaDataObj.put("pack_id", resultSet.getString("pack_id"));

				packIdSet.add(resultSet.getString("pack_id"));
				fileIdSet.add(resultSet.getString("file_id"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			sqlString = "Select * from " + metaViewName + " where pack_id=? and file_id=?";
			PreparedStatement jsonPS = conn.prepareStatement(sqlString);

			JSONObject packsObj = new JSONObject();

			JSONArray packsArray = new JSONArray();
			for (String eachPackId : packIdSet) {
				JSONObject currentPacksObj = new JSONObject();
				currentPacksObj.put("pack_id", eachPackId);

				JSONArray currentFilesArray = new JSONArray();
				for (String eachFileId : fileIdSet) {
					JSONObject currentFileObj = new JSONObject();
					currentFileObj.put("file_id", eachFileId);

					jsonPS.setString(1, eachPackId);
					jsonPS.setString(2, eachFileId);
					resultSet = jsonPS.executeQuery();

					while (resultSet.next()) {
						String file_lob = resultSet.getString("file_lob") == null ? ""
								: resultSet.getString("file_lob").trim().length() == 0 ? ""
										: resultSet.getString("file_lob").trim();
						currentFileObj.put("file_lob", file_lob);
						currentFileObj.put("file_path", resultSet.getString("file_path"));
						currentFileObj.put("file_type", resultSet.getString("file_type"));
						currentFileObj.put("file_status", resultSet.getString("file_status"));
						currentFileObj.put("file_pn", resultSet.getString("file_pn"));

						String file_pp = resultSet.getString("file_pp");
						file_pp = file_pp == null ? "" : file_pp.trim();
						currentFileObj.put("file_pp", file_pp);
						currentFileObj.put("file_meta_id", resultSet.getString("file_meta_id"));

						currentFileObj.put("doc_type", resultSet.getString("doc_type"));
						currentFileObj.put("total_pages", resultSet.getInt("total_pages"));
						currentFileObj.put("word_count", resultSet.getInt("word_count"));
						currentFileObj.put("status_type", resultSet.getString("status_type"));
						currentFileObj.put("status_desc", resultSet.getString("status_desc"));
						currentFileObj.put("native_pages", resultSet.getInt("native_pages"));
						currentFileObj.put("focr_pages", resultSet.getInt("focr_pages"));
						currentFileObj.put("pocr_pages", resultSet.getInt("pocr_pages"));
						currentFileObj.put("blank_pages", resultSet.getInt("blank_pages"));
						currentFileObj.put("image_count", resultSet.getInt("image_count"));
						currentFileObj.put("total_chars", resultSet.getInt("total_chars"));

						String doc_lob = resultSet.getString("doc_lob") == null ? ""
								: resultSet.getString("doc_lob").trim().length() == 0 ? ""
										: resultSet.getString("doc_lob").trim();
						currentFileObj.put("doc_lob", doc_lob);

						currentFilesArray.add(currentFileObj);
					}

					currentPacksObj.put("files", currentFilesArray);

				}
				packsArray.add(currentPacksObj);
				packsObj.put("packs", packsArray);
			}

			metaArray.add(packsObj);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		sqlString = "select file_path from files_missed where job_id='" + getJobID() + "'";
		JSONObject fielMissedObj = new JSONObject();
		JSONArray filesMissedArray = new JSONArray();
		try {
			stmt = conn.createStatement();
			resultSet = stmt.executeQuery(sqlString);
			while (resultSet.next()) {
				JSONObject currentFilesMissedObj = new JSONObject();
				currentFilesMissedObj.put("file_path", resultSet.getString("file_path"));
				filesMissedArray.add(currentFilesMissedObj);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		fielMissedObj.put("files_missed", filesMissedArray);
		metaArray.add(fielMissedObj);
//		System.err.println(metaArray);
		return metaArray;
	}
}
